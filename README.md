<!--
Copyright ETSI

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions and
limitations under the License
-->

# OSM test automation project - osm/tests

This repository contains tools and configuration files for testing and automation needs of OSM project.

## Prerequisites

- OSM running
- VIM already registered in OSM
- K8s cluster already registered in OSM (for tests involving a K8s cluster)

## Quickstart. How to run tests using OSM docker images

### Configure the environment file

Create a file `envconfig.rc` copying from `envconfig-local.rc` and set the required variables.

```
OSM_HOSTNAME=<OSM_IP_ADDRESS>
VIM_TARGET=<VIM_REGISTERED_AT_OSM>
VIM_MGMT_NET=<NAME_OF_THE_MGMT_NETWORK_IN_THE_VIM>
OS_CLOUD=<OPENSTACK_CLOUD>    # OpenStack Cloud defined in $HOME/.config/openstack/clouds.yaml or in /etc/openstack/clouds.yaml
```

### Run the tests

```bash
# Make sure you are using the latest testing-daily image
docker pull opensourcemano/tests:testing-daily
export CLOUDS_YAML_FILE="${HOME}/.config/openstack/clouds.yaml"
export OSM_TESTS_REPO_FOLDER="${HOME}/tests"
docker run --rm=true --name tests -t --env-file envconfig.rc \
           -v "${CLOUDS_YAML_FILE}":/etc/openstack/clouds.yaml \
           -v "${OSM_TESTS_REPO_FOLDER}/reports":/robot-systest/reports \
           opensourcemano/tests:testing-daily \
           -t sanity
```

You can use a different robot tag instead of `sanity`. The whole list of tags are gathered below in this README.

## How to build docker container for tests and run tests from there

### Create the docker container

```bash
docker build -f docker/Dockerfile -t osmtests .
```

### Run the tests

```bash
export CLOUDS_YAML_FILE="${HOME}/.config/openstack/clouds.yaml"
export OSM_TESTS_REPO_FOLDER="${HOME}/tests"
docker run --rm=true --name tests -t --env-file envconfig.rc \
           -v "${CLOUDS_YAML_FILE}":/etc/openstack/clouds.yaml \
           -v "${OSM_TESTS_REPO_FOLDER}/reports":/robot-systest/reports \
           osmtests \
           -t sol003_01
```

## How to mount local tests folder for developing purposes

The following line will mount the folder `robot-systest`, including all libraries and testuites, and will execute the testsuite `sol003_01`:

```bash
# Make sure you are using the latest testing-daily image as base
docker pull opensourcemano/tests:testing-daily
export CLOUDS_YAML_FILE="${HOME}/.config/openstack/clouds.yaml"
export OSM_TESTS_REPO_FOLDER="${HOME}/tests"
export OSM_PACKAGES_FOLDER="${HOME}/osm-packages"
docker run --rm=true --name tests -t --env-file envconfig.rc \
           -v "${CLOUDS_YAML_FILE}":/etc/openstack/clouds.yaml \
           -v "${OSM_TESTS_REPO_FOLDER}/robot-systest":/robot-systest \
           -v "${OSM_TESTS_REPO_FOLDER}/reports":/robot-systest/reports \
           -v "${OSM_PACKAGES_FOLDER}":/robot-systest/osm-packages \
           opensourcemano/tests:testing-daily \
           -t sol003_01
```

Relevant volumes to be mounted are:

- <path_to_reports> [OPTIONAL]: the absolute path to reports location in the host
- <path_to_clouds.yaml> [OPTIONAL]: the absolute path to the clouds.yaml file in the host
- <path_to_sdncs.yaml> [OPTIONAL]: the absolute path to the sdncs.yaml file in the host
- <path_to_kubeconfig> [OPTIONAL]: the kubeconfig file to be used for k8s clusters

Other relevant options to run tests are:

- `--env-file`: It is the environmental file where is described the OSM target and VIM
- `-o <osmclient_version>` [OPTIONAL]: It is used to specify a particular osmclient version. Default: latest
- `-p <package_branch>` [OPTIONAL]: OSM packages repository branch. Default: master
- `-t <testing_tags>` [OPTIONAL]: Robot tests tags. [sanity, regression, particular_test]. Default: sanity

## How to run tests from a host

In general, testing from docker images is the best way if you want to develop for OSM. However, sometimes it could be useful to run tests directly from the host.

### Install dependencies

This bash script can be used to setup your environment to execute the tests.

```bash
sudo apt-get update
sudo apt-get install ssh ping yq git
# Python packages used for the tests
python3 -m pip install -r requirements.txt
python3 -m pip install -r requirements-dev.txt
# Download community packages
git clone https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git
```

### Configure the environment

Create a file `envconfig.rc` copying from `envconfig-local.rc` and set the required variables (in this case, the use of `export` is mandatory).

```
export OSM_HOSTNAME=<OSM_IP_ADDRESS>
export VIM_TARGET=<VIM_REGISTERED_AT_OSM>
export VIM_MGMT_NET=<NAME_OF_THE_MGMT_NETWORK_IN_THE_VIM>
export OS_CLOUD=<OPENSTACK_CLOUD>    # OpenStack Cloud defined in $HOME/.config/openstack/clouds.yaml or in /etc/openstack/clouds.yaml
export K8S_CREDENTIALS= # path to the kubeconfig file of the K8s cluster to be tested
export OCI_REGISTRY_URL= # URL of the OCI registry where helm charts used in test packages are stored. 
export OCI_REGISTRY_USER= # User of the OCI registry
export OCI_REGISTRY_PASSWORD= # Password of the OCI registry
```

### Running the tests

```bash
source envconfig.rc
mkdir reports
robot -d reports -i <testing_tags> testsuite/
```

## How to run tests from an environment identical to OSM CICD

```bash
git clone https://osm.etsi.org/gerrit/osm/devops
git clone https://osm.etsi.org/gerrit/osm/IM
git clone https://osm.etsi.org/gerrit/osm/osmclient
git clone https://osm.etsi.org/gerrit/osm/tests
# run HTTP server to server artifacts
devops/tools/local-build.sh --install-qhttpd
# generate debian packages locally that will be served by the HTTP server
devops/tools/local-build.sh --module IM,osmclient,tests stage-2
# create docker image and store it locally as opensourcemano/tests:devel
devops/tools/local-build.sh --module tests
```

Then, run the tests:

```bash
docker run --rm=true -t osmtests --env-file <env_file> \
       -v <path_to_reports>:/reports osmtests \
       -v <path_to_clouds.yaml>:/robot-systest/clouds.yaml \
       -v <path_to_sdncs.yaml>:/robot-systest/sdncs.yaml \
       -v <path_to_kubeconfig>:/root/.kube/config \
       -o <osmclient_version> \
       -p <package_branch> \
       -t <testing_tags>
```

## Test tags

All tests in the testsuites have tags. Tags allow to run only a set of tests identified by a tag. Several tags can be specified when running robot in the following way:

```bash
robot -i <tag_01> -i <tag_02> testsuite/
```

The following tags exist for each testsuite:

- A tag per testsuite using its mnemonic (e.g. `basic_01`)
- Cluster tag for each of the statistically similar tests:
  - `cluster_main`: `basic_01`, `basic_05`, `basic_08`, `basic_09`, `basic_15`,
    `basic_16`, `basic_17`, `hackfest_basic`, `hackfest_multivdu`,
    `hackfest_cloudinit`, `quotas_01`
  - `cluster_ee_config`: `basic_06`, `basic_07`, `basic_11`, `basic_12`,
    `basic_13`, `basic_14`, `k8s_05`, `k8s_06`
  - `cluster_relations`: `basic_11`, `basic_13`, `basic_14`
  - `cluster_epa`: `epa_01`, `epa_02`, `epa_03`, `epa_04`, `epa_05`
  - `cluster_k8s`: `k8s_01`, `k8s_02`, `k8s_03`, `k8s_04`, `k8s_05`, `k8s06`,
    `k8s_07`, `k8s_08`, `k8s_09`, `k8s_10`, `k8s_11`, `k8s_12`, `k8s_13`, `k8s_14`, `sa_08`
  - `cluster_k8s_charms`: `k8s_05`, `k8s_06`
  - `cluster_sa`: `sa_01`, `sa_02`, `sa_07`
  - `cluster_slices`: `slice_01`, `slice_02`
  - `cluster_heal`: `heal_01`, `heal_02`, `heal_03`, `heal_04`
  - `cluster_osm_rest`: `sol003_01`
  - `cluster_gitops`: `gitops_01`
- daily: for all testsuites that will run in the daily job
- regression: for all testsuites that should pass in the current stable branch
- sanity: for all testsuites that should be passed by each commit in the
  stage3 to be successfully verified by Jenkins, currently `k8s_04`,
  `sa_02`, `hackfest_basic`, `hackfest_cloudinit`

In addition, the tag "cleanup" exists in those tests that perform
any deletion. In that way, it can be invoked to retry the deletion if
the tests were forcefully stopped.

- For helping in the migration tests and other scenarios in which you don't want
to destroy the deployments immediately, the following tags are used:
  - `prepare`: for the tests that are used to deploy the network
  services under test
  - `verify`: for the tests that perform the actual testing, or changes for
  additional verifications (e.g. scaling).
  - `cleanup`: already described above.

  So, for instance, you could first deploy a number of network services executing
  the tests with "prepare" tag, migrate to another OSM version, and then
  check the behavior executing with the "verify" tag. Finally, use the "cleanup"
  tag. 

## Post-processing Robot output files

The output files of Robot include tyipically three files:

- `report.html`: overview of the test execution results in HTML format
- `log.html`: details about the executed test cases in HTML format
- `output.xml`: all the test execution results in machine readable XML format

More information about these files [here](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#output-file).

It is possible to use the tool `rebot`, included as part of the Robot Framework, to post-process the output file `output.xml`.

```bash
# To re-generate log and report from output.xml:
rebot [-d <output_folder>] output.xml

# To re-generate log and report (and optionally new output.xml) to include only certain tags:
rebot [-d <output_folder>] -i <tag1> -i <tag2> ... -i <tagN> [-o <new_output_xml>] output.xml

# To re-generate log and report (and optionally new output.xml) excluding certain tags:
rebot [-d <output_folder>] -e <tag1> -e <tag2> ... -e <tagN> [-o <new_output_xml>] output.xml

# To merge several test executions:
rebot [-d <output_folder>] --merge output1.xml output2.xml ... outputN.xml
```

More information about post-processing Robot output files [here](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#post-processing-outputs)

## Autogeneration of tests

There is a tool `robot-systest/autogeneration/generate_osm_test.py` that allows generating a Robot test from a YAML configuration:

```bash
$ ./generate_osm_test.py -h
usage: generate_osm_test.py [-h] [-v] --config CONFIG [--template TEMPLATE] [--output OUTPUT]

Generate OSM tests from YAML configuration file.

options:
  -h, --help           show this help message and exit
  -v, --verbose        increase output verbosity
  --config CONFIG      yaml configuration file to create the test
  --template TEMPLATE  template file for rendering the test (default: test_template.j2)
  --output OUTPUT      output file (default: standard output)
```

A YAML configuration file provides the input parameters to generate the test, such as NF and NS packages, NS instances to be created and some operations to be executed once the NS instances are created. An example can be found in `robot-systest/autogeneration/test_config.yaml`:

```yaml
documentation: "[BASIC-40] Auto-generated test"
name: basic_40
tags:
  - basic_40
  - cluster_main
  - daily
nfpkg:
  - package: hackfest_basic_vnf
    name: hackfest_basic-vnf
  - package: hackfest_basic2_vnf
    name: hackfest_basic2-vnf
nspkg:
  - package: hackfest_basic_ns
    name: hackfest_basic-ns
  - package: hackfest_basic2_ns
    name: hackfest_basic2-ns
ns:
  - name: basic_40
    nspkg: hackfest_basic-ns
    config: "{vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }"
vnf:
  - ns: basic_40
    vnf_member_index: vnf
    tests:
      - type: ping
      - type: ssh
```

To generate the Robot, you can run:

```bash
cd robot-systest/autogeneration/
./generate_osm_test.py --config test_config.yaml > ../testsuite/mynewtest.robot
```

## Built With

* [Python](www.python.org/) - The language used
* [Robot Framework](robotframework.org) - The testing framework

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://osm.etsi.org/gitweb/?p=osm/tests.git;a=tags).

## License

This project is licensed under the Apache2 License - see the [LICENSE](LICENSE) file for details

