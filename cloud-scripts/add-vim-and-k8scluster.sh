#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

# Add VIM to OSM
if [ "${CLOUD_TYPE}" == "azure" ]; then
    echo "Adding VIM to OSM"
    set +x
    osm vim-create --name "$VIM_TARGET" --account_type "$CLOUD_TYPE" --auth_url http://www.azure.com \
        --user "$AZURE_CLIENT_ID" --password "$AZURE_SECRET" --tenant "$AZURE_TENANT" --description "None" \
        --config "{region_name: '$AZURE_REGION', resource_group: '$RESOURCE_GROUP',
                subscription_id: '$AZURE_SUBSCRIPTION_ID', vnet_name: '$VNET_NAME',
                flavors_pattern: '$AZURE_FLAVORS_PATTERN'}"
    sleep 10
    osm vim-show "$VIM_TARGET"
elif [ "${CLOUD_TYPE}" == "gcp" ]; then
    echo "CLOUD_TYPE '${CLOUD_TYPE}' still not supported"
    exit
else
    echo "CLOUD_TYPE '${CLOUD_TYPE}' not valid"
    exit
fi

# Add SDN controller to OSM
if [ -n "$SDNC_TYPE" ] && [ -n "$SDNC_USER" ]  && [ -n "$SDNC_URL" ]; then
    echo "Adding $SDNC_TYPE SDN controller to OSM"
    set +x
    osm sdnc-create --name sdnc --user "$SDNC_USER" --password "$SDNC_PASSWORD" --url "$SDNC_URL" --type "$SDNC_TYPE"
    sleep 3
    osm sdnc-list
    # TO-DO: add port-mapping file to VIM
    # osm vim-update "$VIM_TARGET" --sdn_controller sdnc --sdn_port_mapping port-mapping-etsi-vim.yaml
fi

# Add K8s cluster to OSM
if [ -n "$K8S_CREDENTIALS" ] && [ -n "$VIM_TARGET" ]  && [ -n "$K8S_IMAGE_NAME" ]; then
    echo "Adding K8s cluster to OSM"
    osm k8scluster-add --creds $K8S_CREDENTIALS --version v1 --vim $VIM_TARGET \
        --k8s-nets "{'net1': '$VIM_MGMT_NET'}" $K8S_IMAGE_NAME --description "Robot cluster"
    sleep 20
    osm k8scluster-show $K8S_IMAGE_NAME
fi
