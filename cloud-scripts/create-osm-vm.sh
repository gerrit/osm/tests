#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

# Create a new VM for installing OSM and its NSG on Azure. SSH key pair files ~/.ssh/id_rsa
# and ~/.ssh/id_rsa.pub must exist. The NEW_OSM_IP variable is set with IP address.
# To do this it reads the following environment variables:
# - OSM_IMAGE_NAME: name of the new VM
# - RESOURCE_GROUP: name of the resource-group where the VM will be created
# - VNET_NAME: name of the virtual network when creating a new one or referencing an existing one
# - VIM_MGMT_NET: name or ID of the subnet to which the VM will be connected
# - SOURCE_IMAGE_NAME: name of operating system image used (e.g. "Canonical:0001-com-ubuntu-server-jammy:22_04-lts:latest")
# - FLAVOR_NAME: the VM size to be created (e.g. "Standard_D4as_v4")
# - PRIORITY: "Low", "Regular" or Spot"
function create_azure_vm {
    # Create the VM in resource-group
    set -eux
    az vm create --resource-group "${RESOURCE_GROUP}" --name "${OSM_IMAGE_NAME}" --image "${SOURCE_IMAGE_NAME}" --size "${FLAVOR_NAME}" --vnet-name "${VNET_NAME}" --subnet "${VIM_MGMT_NET}" --public-ip-address "" --admin-username ubuntu --priority "${PRIORITY}" --os-disk-size-gb 64
    export NEW_OSM_IP=$(az vm show -d -g "${RESOURCE_GROUP}" -n "${OSM_IMAGE_NAME}" --query privateIps | tr -d \")

    # Add a security group rule
    INTERFACE_ID=$(az vm show --resource-group ${RESOURCE_GROUP} --name ${OSM_IMAGE_NAME} --query networkProfile.networkInterfaces[0].id)
    INTERFACE_ID=${INTERFACE_ID:1:-1}
    SECURITY_GROUP_ID=$(az network nic show --id ${INTERFACE_ID} --query networkSecurityGroup.id)
    SECURITY_GROUP_ID=${SECURITY_GROUP_ID:1:-1}
    SECURITY_GROUP_NAME=$(az resource show --ids ${SECURITY_GROUP_ID} --query name)
    SECURITY_GROUP_NAME=${SECURITY_GROUP_NAME:1:-1}
    az network nsg rule create -n osm --nsg-name ${SECURITY_GROUP_NAME} --priority 2000 -g ${RESOURCE_GROUP}  --description "NBI and Prometheus ports" --protocol TCP --destination-port-ranges 9999 9091
}

if [ -n "${1:-}" ]; then  # If there is an argument, it must be the hostname
    export OSM_IMAGE_NAME=$1
else
    export OSM_IMAGE_NAME=osmtest$(date '+%Y%m%d%H%M')
fi

# Branch by CLOUD_TYPE value ("azure", "gcp")
if [ "${CLOUD_TYPE}" == "azure" ]; then
    # Azure VIM
    create_azure_vm
elif [ "${CLOUD_TYPE}" == "gcp" ]; then
    # Google Cloud VIM
    export OSM_IMAGE_NAME=$(echo $OSM_IMAGE_NAME | sed "s/_/-/g")
    echo "CLOUD_TYPE '${CLOUD_TYPE}' still not supported"
    exit
else
    echo "Invalid cloud type: ${CLOUD_TYPE}"
fi

# Log new environment variables
mkdir -p ${ROBOT_REPORT_FOLDER}
cat <<EOF > ${ROBOT_REPORT_FOLDER}/osm_environment.rc
export CLOUD_TYPE="${CLOUD_TYPE}"
export OSM_IP_ADDRESS="${NEW_OSM_IP}"
export OSM_HOSTNAME="nbi.${NEW_OSM_IP}.nip.io"
export OSM_IMAGE_NAME="${OSM_IMAGE_NAME}"
EOF
echo File with new environment was created at ${ROBOT_REPORT_FOLDER}/osm_environment.rc
