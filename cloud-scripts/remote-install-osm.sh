#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@"${OSM_IP_ADDRESS}" \
'sudo apt-get update -y && sudo apt-get upgrade -y && sudo reboot'

sleep 90

# OSM installation
INSTALLER_PARAMETERS="-R ${REPO_BASE} -t ${DOCKER_TAG} -r ${REPO_NAME} -y"
echo "INSTALLER_URL: $INSTALLER_URL"
echo "INSTALLER_PARAMETERS: $INSTALLER_PARAMETERS"
ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${OSM_IP_ADDRESS} << EOF 2>&1
set -eux
wget "${INSTALLER_URL}"
chmod +x install_osm.sh
./install_osm.sh  ${INSTALLER_PARAMETERS} 2>&1 | tee osm_install_log.txt
set +eux
EOF

# # Installs additional tools for telemetry
# set +eux
# ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${OSM_IP_ADDRESS} 'sudo apt-get install -y sysstat'
# set -eux

# Gets Juju password from LCM env file
set +eux
JUJU_PWD=$(ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${OSM_IP_ADDRESS} 'kubectl -n osm exec -ti deploy/lcm -- env | grep OSMLCM_VCA_SECRET | cut -d = -f 2')
set -eux

# Updates environment variables and logs them
export JUJU_PASSWORD="${JUJU_PWD}"

# Logs new/updated environment variables
cat <<EOF >> ${ROBOT_REPORT_FOLDER}/osm_environment.rc
export JUJU_PASSWORD="${JUJU_PWD}"
EOF

echo Environment was updated at ${ROBOT_REPORT_FOLDER}/osm_environment.rc
