#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

# Delete a VM and its resources (nic, disk, nsg, public IP) on Azure.
# To do this it reads the following environment variables:
# - VM_NAME: name of the VM
# - RESOURCE_GROUP: name of the resource-group where the VM is
function delete_azure_vm {
    set -eux
    INTERFACE_ID=$(az vm show --resource-group ${RESOURCE_GROUP} --name ${VM_NAME} --query networkProfile.networkInterfaces[0].id)
    INTERFACE_ID=${INTERFACE_ID:1:-1}
    OS_DISK_ID=$(az vm show --resource-group ${RESOURCE_GROUP} --name ${VM_NAME} --query storageProfile.osDisk.managedDisk.id)
    OS_DISK_ID=${OS_DISK_ID:1:-1}
    SECURITY_GROUP_ID=$(az network nic show --id ${INTERFACE_ID} --query networkSecurityGroup.id)
    SECURITY_GROUP_ID=${SECURITY_GROUP_ID:1:-1}
    PUBLIC_IP_ID=$(az network nic show --id ${INTERFACE_ID} --query ipConfigurations[0].publicIpAddress.id)
    PUBLIC_IP_ID=${PUBLIC_IP_ID:1:-1}
    az vm delete --resource-group ${RESOURCE_GROUP} --name ${VM_NAME} --yes
    az network nic delete --id ${INTERFACE_ID}
    az disk delete --id ${OS_DISK_ID} --yes
    az network nsg delete --id ${SECURITY_GROUP_ID}
    if [ -n "${PUBLIC_IP_ID}" ]; then
        az network public-ip delete --id ${PUBLIC_IP_ID}
    fi
}

# Delete a VM on GCP.
# To do this it reads the following environment variables:
# - VM_NAME: name of the VM
# - GCP_PROJECT: name of project where the VM is
# - GCP_ZONE: name of the zone
function delete_gcp_vm {
    gcloud compute instances delete "${VM_NAME}" --project="${GCP_PROJECT}" --zone="${GCP_ZONE}" --delete-disks all -q
}

if [ -n "${1:-}" ]; then  # If there is an argument, it must be the hostname
    VM_NAME=$1
else
    VM_NAME="${OSM_IMAGE_NAME}"
fi

# Branch by CLOUD_TYPE value ("azure", "gcp")
if [ "${CLOUD_TYPE}" == "azure" ]; then
    # Azure VIM
    delete_azure_vm
elif [ "${CLOUD_TYPE}" == "gcp" ]; then
    # GCP VIM
    delete_gcp_vm
else
    echo "Invalid cloud type: ${CLOUD_TYPE}"
fi
