#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

# Delete a VM and its resources (nic, disk, nsg, public IP) on Azure.
# To do this it reads the following environment variables:
# - K8S_NAME: name of the VM
# - RESOURCE_GROUP: name of the resource-group where the VM is
function delete_azure_vm {
    set -eux
    INTERFACE_ID=$(az vm show --resource-group ${RESOURCE_GROUP} --name ${K8S_NAME} --query networkProfile.networkInterfaces[0].id)
    INTERFACE_ID=${INTERFACE_ID:1:-1}
    OS_DISK_ID=$(az vm show --resource-group ${RESOURCE_GROUP} --name ${K8S_NAME} --query storageProfile.osDisk.managedDisk.id)
    OS_DISK_ID=${OS_DISK_ID:1:-1}
    SECURITY_GROUP_ID=$(az network nic show --id ${INTERFACE_ID} --query networkSecurityGroup.id)
    SECURITY_GROUP_ID=${SECURITY_GROUP_ID:1:-1}
    PUBLIC_IP_ID=$(az network nic show --id ${INTERFACE_ID} --query ipConfigurations[0].publicIpAddress.id)
    PUBLIC_IP_ID=${PUBLIC_IP_ID:1:-1}
    az vm delete --resource-group ${RESOURCE_GROUP} --name ${K8S_NAME} --yes
    az network nic delete --id ${INTERFACE_ID}
    az disk delete --id ${OS_DISK_ID} --yes
    az network nsg delete --id ${SECURITY_GROUP_ID}
    if [ -n "${PUBLIC_IP_ID}" ]; then
        az network public-ip delete --id ${PUBLIC_IP_ID}
    fi
}

# Delete an existing AKS cluster and its subnet.
# Required the following environment variables:
# - K8S_NAME: name of the AKS
# - RESOURCE_GROUP: name of the resource-group where the AKS was created
function delete_azure_aks {
    SUBNET_ID=$(az aks show --resource-group "${RESOURCE_GROUP}" --name ${K8S_NAME} --query agentPoolProfiles[].vnetSubnetId -o tsv)
    az aks delete -y --resource-group "${RESOURCE_GROUP}" --name "${K8S_NAME}"
    az network vnet subnet delete --ids "${SUBNET_ID}"
}

# Delete a VM on GCP.
# To do this it reads the following environment variables:
# - K8S_NAME: name of the VM
# - GCP_PROJECT: name of project where the VM is
# - GCP_ZONE: name of the zone
function delete_gcp_vm {
    gcloud compute instances delete "${K8S_NAME}" --project="${GCP_PROJECT}" --zone="${GCP_ZONE}" --delete-disks all -q
}

# Delete an existing GKE cluster.
# Required the following environment variables:
# - K8S_NAME: name of the AKS
# - GCP_PROJECT: name of project where the VM is
# - GCP_ZONE: name of the zone
function delete_gcp_gke {
   gcloud container clusters delete "${K8S_NAME}" --project="${GCP_PROJECT}" --zone="${GCP_ZONE}" -q
}

# If there is an argument, it must be the cluster name
if [ -n "${1:-}" ]; then
    K8S_NAME=$1
else
    K8S_NAME="${K8S_IMAGE_NAME}"
fi
# Default USE_PAAS_K8S is "FALSE"
if [ -z "${USE_PAAS_K8S}" ]; then
    USE_PAAS_K8S="FALSE"
fi
# Branch by USE_PAAS_K8S and CLOUD_TYPE values
if [ "${USE_PAAS_K8S}" == "FALSE" ]; then
    # Deletes k8s cluster in a cloud's VM
    echo "Deleting IaaS k8s cluster in ${CLOUD_TYPE}"
    if [ "${CLOUD_TYPE}" == "azure" ]; then
        # Azure VIM
	delete_azure_vm
    elif [ "${CLOUD_TYPE}" == "gcp" ]; then
        # GCP VIM
	delete_gcp_vm
    else
        echo "Invalid cloud type: ${CLOUD_TYPE}"
    fi
else
    # Deletes k8s cluster as PaaS in cloud
    if [ "${CLOUD_TYPE}" == "azure" ]; then
        echo "Deleting PaaS k8s cluster in Azure"
	delete_azure_aks
    elif [ "${CLOUD_TYPE}" == "gcp" ]; then
        echo "Deleting PaaS k8s cluster in GCP"
	delete_gcp_gke
    else
        echo "Invalid cloud type: ${CLOUD_TYPE}"
    fi

fi

