#!/usr/bin/env bash
#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

set -e

# Extracts logs from "deployments"
for MODULE in 'grafana' 'keystone' 'lcm' 'mon' 'nbi' 'pol' 'ro' 'ngui' 'airflow-scheduler' 'pushgateway-prometheus-pushgateway' 'webhook-translator'
do
    echo Saving ${MODULE} logs...
    ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${OSM_IP_ADDRESS} \
    'kubectl -n osm logs deployment/"'${MODULE}'" --all-containers=true 2>&1 | cat' > "${ROBOT_REPORT_FOLDER}"/"osm-deploy-${MODULE}".log
done

# Extracts logs from "statefulsets"
for MODULE in 'kafka' 'mongo' 'mysql' 'prometheus' 'zookeeper' 'alertmanager'
do
    echo Saving ${MODULE} logs...
    ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@${OSM_IP_ADDRESS} \
    'kubectl -n osm logs statefulset/"'${MODULE}'" --all-containers=true 2>&1 | cat' > "${ROBOT_REPORT_FOLDER}"/"osm-sts-${MODULE}".log
done

echo
echo All logs saved to ${ROBOT_REPORT_FOLDER}/

