#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import os
import yaml
from get_clouds_yaml_info import get_values_from_cloud, get_vim_values


# Variables to be used by the testsuite
VIM_ACCOUNT_TYPE = "openstack"
VIM_NAME = "basic_08_vim_test"
VIM_USER = ""
VIM_PASSWORD = ""
VIM_AUTH_URL = ""
VIM_TENANT = ""
VIM_CONFIG = ""

# VIM configuration
cloud_type = os.environ.get("CLOUD_TYPE", "openstack")
if cloud_type == "openstack":
    # Openstack VIM
    VIM_ACCOUNT_TYPE = "openstack"
    vim_name_prefix = "basic32"
    # Get credentias from Openstack Clouds file
    cloud, os_cloud = get_values_from_cloud()
    (
        VIM_USER,
        VIM_PASSWORD,
        VIM_AUTH_URL,
        VIM_TENANT,
        vim_user_domain_name,
        vim_project_domain_name,
        vim_insecure,
    ) = get_vim_values(cloud, os_cloud)

    # Availability Zone config
    vim_config_dict = {"storage_availability_zone": "storage_az"}
    VIM_CONFIG = "'{}'".format(
        yaml.safe_dump(vim_config_dict, default_flow_style=True, width=10000).rstrip(
            "\r\n"
        )
    )

else:
    raise Exception("VIM type not supported: '" + cloud_type + "'")
