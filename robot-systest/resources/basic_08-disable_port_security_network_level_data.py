#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import os
import yaml
from get_clouds_yaml_info import get_values_from_cloud, get_vim_values


# Variables to be used by the testsuite
VIM_ACCOUNT_TYPE = "openstack"
VIM_NAME = "basic_08_vim_test"
VIM_USER = ""
VIM_PASSWORD = ""
VIM_AUTH_URL = ""
VIM_TENANT = ""
VIM_CONFIG = ""

# Get credentials from Openstack clouds file
cloud, os_cloud = get_values_from_cloud()
(
    VIM_USER,
    VIM_PASSWORD,
    VIM_AUTH_URL,
    VIM_TENANT,
    vim_user_domain_name,
    vim_project_domain_name,
    vim_insecure,
) = get_vim_values(cloud, os_cloud)

# Extra VIM config
vim_config_dict = {}
vim_config_dict["vim_network_name"] = os.environ.get("VIM_MGMT_NET")
if vim_project_domain_name:
    vim_config_dict["project_domain_name"] = vim_project_domain_name
if vim_user_domain_name:
    vim_config_dict["user_domain_name"] = vim_user_domain_name
if vim_insecure:
    vim_config_dict["insecure"] = True
vim_config_dict["disable_network_port_security"] = True
vim_config_dict["management_network_name"] = os.environ.get("VIM_MGMT_NET")
VIM_CONFIG = "'{}'".format(
    yaml.safe_dump(vim_config_dict, default_flow_style=True, width=10000).rstrip("\r\n")
)
