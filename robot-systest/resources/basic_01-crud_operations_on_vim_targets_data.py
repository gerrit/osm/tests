#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import os
import yaml
from get_clouds_yaml_info import get_values_from_cloud, get_vim_values


# Variables to be used by the testsuite
VIM_ACCOUNT_TYPE = ""
VIM_NAME_PREFIX = ""
VIM_USER = ""
VIM_PASSWORD = ""
VIM_AUTH_URL = ""
VIM_TENANT = ""
VIM_CONFIG = ""

# VIM configuration
cloud_type = os.environ.get("CLOUD_TYPE", "openstack")
if cloud_type == "openstack":
    # Openstack VIM
    VIM_ACCOUNT_TYPE = "openstack"
    VIM_NAME_PREFIX = "basic01"
    # Get credentias from Openstack Clouds file
    cloud, os_cloud = get_values_from_cloud()
    (
        VIM_USER,
        VIM_PASSWORD,
        VIM_AUTH_URL,
        VIM_TENANT,
        vim_user_domain_name,
        vim_project_domain_name,
        vim_insecure,
    ) = get_vim_values(cloud, os_cloud)
    # Extra config
    vim_config_dict = {}
    vim_config_dict["vim_network_name"] = os.environ.get("VIM_MGMT_NET")
    if vim_project_domain_name:
        vim_config_dict["project_domain_name"] = vim_project_domain_name
    if vim_user_domain_name:
        vim_config_dict["user_domain_name"] = vim_user_domain_name
    if vim_insecure:
        vim_config_dict["insecure"] = True
    VIM_CONFIG = "'{}'".format(
        yaml.safe_dump(vim_config_dict, default_flow_style=True, width=10000).rstrip(
            "\r\n"
        )
    )

elif cloud_type == "azure":
    # Azure VIM
    VIM_ACCOUNT_TYPE = "azure"
    VIM_NAME_PREFIX = "basic01"
    VIM_AUTH_URL = "http://www.azure.com"
    VIM_USER = os.environ.get("AZURE_CLIENT_ID")
    VIM_PASSWORD = os.environ.get("AZURE_SECRET")
    VIM_TENANT = os.environ.get("AZURE_TENANT")

    # Extra config
    vim_config_dict = {}
    if "RESOURCE_GROUP" in os.environ:
        vim_config_dict["resource_group"] = os.environ.get("RESOURCE_GROUP")
    if "AZURE_REGION" in os.environ:
        vim_config_dict["region_name"] = os.environ.get("AZURE_REGION")
    if "AZURE_SUBSCRIPTION_ID" in os.environ:
        vim_config_dict["subscription_id"] = os.environ.get("AZURE_SUBSCRIPTION_ID")
    if "VNET_NAME" in os.environ:
        vim_config_dict["vnet_name"] = os.environ.get("VNET_NAME")
    if "AZURE_FLAVORS_PATTERN" in os.environ:
        vim_config_dict["flavors_pattern"] = os.environ.get("AZURE_FLAVORS_PATTERN")
    VIM_CONFIG = "'{}'".format(
        yaml.safe_dump(vim_config_dict, default_flow_style=True, width=10000).rstrip(
            "\r\n"
        )
    )

else:
    raise Exception("VIM type not supported: '" + cloud_type + "'")
