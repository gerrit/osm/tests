*** Comments ***
# Copyright 2020 Canonical Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   Library providing keywords for CRUD operations over Kubernetes clusters with OSM client.
Library   OperatingSystem


*** Variables ***
${SUCCESS_RETURN_CODE}   0
${K8SCLUSTER_LAUNCH_MAX_WAIT_TIME}   6min
${K8SCLUSTER_LAUNCH_POL_TIME}   30sec
${K8SCLUSTER_DELETE_MAX_WAIT_TIME}   2min
${K8SCLUSTER_DELETE_POL_TIME}   15sec


*** Keywords ***
Create K8s Cluster
    [Documentation]   Register a Kubernetes cluster in OSM using the name, version and credentials passed as arguments.
    [Arguments]   ${k8scluster_creds}   ${k8scluster_version}   ${k8scluster_vim}   ${k8scluster_net}   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-add --creds ${k8scluster_creds} --version ${k8scluster_version} --vim ${k8scluster_vim} --k8s-nets '{"net1": "${k8scluster_net}"}' ${k8scluster_name} --description "Robot cluster"
    Log   ${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Wait Until Keyword Succeeds   ${K8SCLUSTER_LAUNCH_MAX_WAIT_TIME}   ${K8SCLUSTER_LAUNCH_POL_TIME}   Check For K8s Cluster To Be Ready   ${k8scluster_name}
    RETURN   ${stdout}

Delete K8s Cluster
    [Documentation]   Unregister/delete a Kubernetes cluster from OSM.
    [Arguments]   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-delete ${k8scluster_name}
    Log   ${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Wait Until Keyword Succeeds   ${K8SCLUSTER_DELETE_MAX_WAIT_TIME}   ${K8SCLUSTER_DELETE_POL_TIME}   Check For K8s Cluster To Be Deleted   ${k8scluster_name}

Get K8s Cluster
    [Documentation]   Get the list of Kubernetes clusters in OSM, and return it.
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-list
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Log   ${stdout}
    RETURN   ${stdout}

Check For K8s Cluster
    [Documentation]   Check if a Kubernetes cluster identified by name exists in OSM, and return it.
    [Arguments]   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-list --filter name="${k8scluster_name}"
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    RETURN   ${stdout}

Check For K8s Cluster To Be Deleted
    [Documentation]   Check if a Kubernetes cluster identified by name is not present in OSM.
    [Arguments]   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-list --filter name="${k8scluster_name}" | awk '{print $2}' | grep ${k8scluster_name}
    Log   ${rc},${stdout}
    Should Be Empty   ${stdout}

Check For K8s Cluster To Be Ready
    [Documentation]   Check if a Kubernetes cluster registered in OSM is ready (the state must be ENABLED or DEGRADED).
    [Arguments]   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-list --filter name="${k8scluster_name}" --filter _admin.operationalState="ENABLED,DEGRADED"
    Log   ${rc},${stdout}
    ${rc}   ${stdout}=   Run And Return Rc And Output   echo "${stdout}" | awk '{print $2}' | grep ${k8scluster_name}
    Log   ${rc},${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Should Be Equal As Strings   ${stdout}   ${k8scluster_name}

Check For K8s Cluster To Be Enabled
    [Documentation]   Check if the state of Kubernetes cluster registered in OSM is ENABLED.
    [Arguments]   ${k8scluster_name}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-show ${k8scluster_name}
    Log   ${rc},${stdout}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm k8scluster-list --filter name="${k8scluster_name}" --filter _admin.operationalState="ENABLED"
    Log   ${rc},${stdout}
    ${rc}   ${stdout}=   Run And Return Rc And Output   echo "${stdout}" | awk '{print $2}' | grep ${k8scluster_name}
    Log   ${rc},${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Should Be Equal As Strings   ${stdout}   ${k8scluster_name}
