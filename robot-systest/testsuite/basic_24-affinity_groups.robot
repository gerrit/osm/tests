*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-24] VNF with two VDUs belonging to an Openstack server group

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   basic_24   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   affinity_basic_vnf
${VNFD_NAME}   affinity_basic-vnf
${NSD_PKG}   affinity_basic_ns
${NSD_NAME}   affinity_basic-ns

# NS instance name and configuration
${NS_NAME}   basic_24
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${NS_ID}   ${EMPTY}
${VNF_MEMBER_INDEX}   affinity-basic-1
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Package
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Package
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Check Vnf Affinity Groups
    [Documentation]   Check that all VDUs of the VNF belong to the expected affinity group.
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    ${vnf_id}=   Get Vnf Id   ${NS_ID}   ${VNF_MEMBER_INDEX}
    @{vdur_id_list}=   Get VDU List From VNF   ${vnf_id}
    FOR   ${vdur_id}   IN   @{vdur_id_list}
        ${server_group_name}=   Get VDU Affinity Group Name   ${NS_NAME}   ${vnf_id}   ${vdur_id}
        ${vm_vim_id}=   Get VDU VIM Id   ${vnf_id}   ${vdur_id}
        Check VM In Server Group   ${vm_vim_id}   ${server_group_name}
    END

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
