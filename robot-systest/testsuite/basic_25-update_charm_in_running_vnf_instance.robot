*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-25] Update Charms in Running VNF Instance.

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_25   cluster_ee_config   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   charm-packages/ha_proxy_charm_vnf
${UPDATED_VNFD_PKG}   charm-packages/updated_vnfds/ha_proxy_charm_vnf
${VNFD_NAME}   ha_proxy_charm-vnf
${NSD_PKG}   charm-packages/ha_proxy_charm_ns
${NSD_NAME}   ha_proxy_charm-ns

# NS instance name and configuration
${NS_NAME}   basic_25_charm_update_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${ACTION_NAME}   touch
${NEW_ACTION_NAME}   mkdir
${VNF_MEMBER_INDEX}   vnf1
${DAY_1_FILE_NAME}   /home/ubuntu/first-touch
${DAY_2_FILE_NAME}   /home/ubuntu/mytouch1
${DAY_2_FOLDER_NAME}   /home/ubuntu/myfolder1
${NS_TIMEOUT}   15min

# NS update operation configuration
${UPDATE_TYPE}   CHANGE_VNFPKG
${NS_UPDATE_TIMEOUT}   500


*** Test Cases ***
Create Charm VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    ${id}=   Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'
    Set Suite Variable   ${VNFD_ID}   ${id}

Create Charm NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Charm Network Service
    [Documentation]   Instantiates the NS for the test suite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}

Set NS Update Config
    [Documentation]   Prepare data for NS Update
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    ${id}=   Get Vnf Id   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${id}
    Set Suite Variable   ${VNF_ID}   ${id}
    Set Suite Variable   ${NS_UPDATE_CONFIG}   '{changeVnfPackageData: [{vnfInstanceId: "${VNF_ID}", vnfdId: "${VNFD_ID}"}]}'

Get Management Ip Address
    [Documentation]   Get the mgmt IP address of the VNF.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test SSH Access
    [Documentation]   Check that the VNF is accessible via SSH in its mgmt IP address.
    Variable Should Exist   ${VNF_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX}' is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Check Remote Files Created Via Day 1 Operation
    [Documentation]   The Charm VNF has a Day 1 operation that creates a file named ${day_1_file_name}.
    ...               This test checks whether that files have been created or not.
    Check If Remote File Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_1_FILE_NAME}

Execute Day 2 Operation
    [Documentation]   Performs one Day 2 operation on the VNF that creates a new file.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id_1}=   Execute NS Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX}   filename=${DAY_2_FILE_NAME}
    Log   ${ns_op_id_1}

Check Remote File Created Via Day 2 Operation
    [Documentation]   Check whether the file created in the previous test via Day 2 operations exist or not.
    Check If Remote File Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_2_FILE_NAME}

Update VNFD Package
    [Documentation]   Updates the VNFD by using new VNFD package which includes the Charm
    ...               which has new day2 operation.
    Update VNFD   '%{PACKAGES_FOLDER}/${UPDATED_VNFD_PKG}'   ${VNFD_NAME}

Update Running VNF Instance
    [Documentation]   Updates the running VNF instance by using new VNFD package.
    ...               As the VNFD has an updated charm, this operation will trigger the upgrade of existing charm
    ...               for the specific VNF.
    Update Network Service   ${NS_ID}   ${UPDATE_TYPE}   ${NS_UPDATE_CONFIG}   ${NS_UPDATE_TIMEOUT}

Execute Day 2 Operation on Upgraded Charm
    [Documentation]   Performs one Day 2 operation on the updated VNF that creates a new folder.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id_2}=   Execute NS Action   ${NS_NAME}   ${NEW_ACTION_NAME}   ${VNF_MEMBER_INDEX}   foldername=${DAY_2_FOLDER_NAME}
    Log   ${ns_op_id_2}

Check Remote Folder Created Via Day 2 Operation
    [Documentation]   Check whether the folder created in the previous test via Day 2 operations exist or not.
    Check If Remote Folder Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_2_FOLDER_NAME}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
