*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-26] Secure connection to helm execution environments.

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_26   cluster_ee_config   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   simple_ee_vnf
${VNFD_NAME}   simple_ee-vnf
${NSD_PKG}   simple_ee_ns
${NSD_NAME}   simple_ee-ns

# NS instance name and configuration
${NS_NAME}   basic_26_secure_helm_ee_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   5min

# SSH keys and username to be used to connect to the VNF
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa

# SSH keys and username to be used to connect to OSM VM
${OSM_HOST}   %{OSM_HOSTNAME}
${OSM_USERNAME}   ubuntu
${OSM_PASSWORD}   ${EMPTY}
${OSM_PRIVATEKEY}   %{OSM_RSA_FILE}

# Variables related to day1 and day2
${ACTION_NAME}   touch
${NEW_ACTION_NAME}   mkdir
${VNF_MEMBER_INDEX}   simple
${DAY_1_FILE_NAME}   /home/ubuntu/first-touch


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    ${id}=   Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'
    Set Suite Variable   ${VNFD_ID}   ${id}

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Address
    [Documentation]   Get the mgmt IP address of the VNF of the NS.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test SSH Access
    [Documentation]   Check that the VNF is accessible via SSH in its mgmt IP address.
    Variable Should Exist   ${VNF_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX}' is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Check Remote File Created Via Day 1 Operation
    [Documentation]   Check whether the file created in the previous test via Day 1 operations exist or not.
    Check If Remote File Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_1_FILE_NAME}

Get data from EE pod
    [Documentation]   Retrieve pod name and service IP address of the helm-based EE running in OSM cluster and store as suite variables to be used later on.
    Open Connection   ${OSM_HOST}
    Login With Public Key   ${OSM_USERNAME}   keyfile=${OSM_PRIVATEKEY}
    ${pod}=   Execute Remote Command Check Rc Return Output   ${OSM_HOST}   ${OSM_USERNAME}   ${OSM_PASSWORD}   ${OSM_PRIVATEKEY}   kubectl get pod -n ${NS_ID} --no-headers -o custom-columns=":metadata.name"
    ${svc}=   Execute Remote Command Check Rc Return Output   ${OSM_HOST}   ${OSM_USERNAME}   ${OSM_PASSWORD}   ${OSM_PRIVATEKEY}   kubectl get svc -n ${NS_ID} -l app.kubernetes.io/name=eechart -o jsonpath='{.items[0].spec.clusterIP}'
    Set Suite Variable   ${POD_NAME}   ${pod}
    Set Suite Variable   ${SVC_IP}   ${svc}

Fail when trying to access gRPC server without TLS
    [Documentation]   Assert failure when accessing the EE without TLS.
    ${command}=   Catenate   python3 osm_ee/frontend_client.py ${SVC_IP} 50050 sleep
    ${result}=   Execute Remote Command Check Rc Return Output   ${OSM_HOST}   ${OSM_USERNAME}   ${OSM_PASSWORD}   ${OSM_PRIVATEKEY}   kubectl exec -it -n ${NS_ID} ${POD_NAME} -- ${command}
    Should Contain   ${result}   Connection lost

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
