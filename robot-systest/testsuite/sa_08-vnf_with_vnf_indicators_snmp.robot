*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [SA-08] VNF with VNF-based indicators through SNMP.

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/prometheus_lib.resource

Test Tags   sa_08   cluster_k8s   daily   regression

Suite Setup   Run Keyword And Ignore Error   Suite Preparation
Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS instantiation parameters
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   snmp_ee_vnf
${VNFD_NAME}   snmp_ee-vnf
${NSD_PKG}   snmp_ee_ns
${NSD_NAME}   snmp_ee-ns

# NS instance name
${NS_NAME}   sa_08-vnf_with_vnf_indicators_snmp_test

# Prometheus polling interval and retries
${PROMETHEUS_POLL_RETRIES}   10 times
${PROMETHEUS_POLL_TIMEOUT}   1 minute

# Prometheus metrics to retrieve
${METRIC_1_NAME}   ifInOctets
${METRIC_1_FILTER}   ifIndex=1
${METRIC_2_NAME}   ifMtu
${METRIC_2_FILTER}   ifIndex=2


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate the NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${EMPTY}
    Set Suite Variable   ${NS_ID}   ${id}

Get VNF SNMP Metrics
    [Documentation]   Check that SNMP metrics from the VNF are stored in Prometheus.
    Variable Should Exist   ${PROMETHEUS_URL}   msg=Prometheus URL is not available
    Variable Should Exist   ${METRIC_1_NAME}   msg=Prometheus first metric name is not available
    Variable Should Exist   ${METRIC_2_NAME}   msg=Prometheus second metric name is not available
    ${metric_1_value}=   Wait Until Keyword Succeeds   ${PROMETHEUS_POLL_RETRIES}   ${PROMETHEUS_POLL_TIMEOUT}   Get Metric   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}   ${METRIC_1_NAME}   ${METRIC_1_FILTER}
    IF   ${metric_1_value} <= 0   Fail   msg=The metric '${METRIC_1_NAME}' value is '${metric_1_value}'
    ${metric_2_value}=   Wait Until Keyword Succeeds   ${PROMETHEUS_POLL_RETRIES}   ${PROMETHEUS_POLL_TIMEOUT}   Get Metric   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}   ${METRIC_2_NAME}   ${METRIC_2_FILTER}
    IF   ${metric_2_value} <= 0   Fail   msg=The metric '${METRIC_2_NAME}' value is '${metric_2_value}'

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables

Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
