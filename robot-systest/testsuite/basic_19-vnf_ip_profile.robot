*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-19] NS with a single VNF and two VDU linked by a VLD with ip-profile.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_19   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   ipprofile_2vm_vnf
${VNFD_NAME}   ipprofile_2vm-vnf
${NSD_PKG}   ipprofile_2vm_ns
${NSD_NAME}   ipprofile_2vm-ns

# NS instance name and configuration
${NS_NAME}   basic_19
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${USERNAME}   ubuntu

# VNFs data
${VNF_MEMBER_INDEX_1}   vnf
${INTERNAL_PREFIX}   ^192.168.200.*

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Info
    [Documentation]   Get VNF information, specifically the VNF instance id and the mgmt IP address.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Set Suite Variable   ${VNF_IPMGMT}   ${ip}
    Log   ${VNF_IPMGMT}

Check Vnf IPs
    [Documentation]   Check whether IP addresses are syntactically valid.
    Variable Should Exist   ${VNF_ID}   msg=VNF is not available
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq '.vdur[0].interfaces[] | select(."vnf-vld-id" == "internal")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${INTERNAL_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${INTERNAL_PREFIX}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq '.vdur[1].interfaces[] | select(."vnf-vld-id" == "internal")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${INTERNAL_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${INTERNAL_PREFIX}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
