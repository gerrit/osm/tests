*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-23] Creation and deletion of SOL004 and SOL007 packages.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource

Test Tags   basic_23   cluster_main   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   sol004_hackfest_basic_vnf
${VNFD_NAME}   sol004_hackfest_basic-vnf
${NSD_PKG}   sol007_hackfest_basic_ns
${NSD_NAME}   sol007_hackfest_basic-ns


*** Test Cases ***
Create VNF Package
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Package
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Cannot Delete VNF Package
    [Documentation]   Assert that the VNF package cannot be deleted before the NS package is deleted.
    Assert Failure Delete VNFD   ${VNFD_NAME}

Delete NS Package
    [Documentation]   Delete NS package.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Package
    [Documentation]   Delete VNF package.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting NS and VNF packages
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
