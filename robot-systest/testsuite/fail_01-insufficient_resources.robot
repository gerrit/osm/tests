*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [FAIL-01] A single VNF with 2 VDUs, one of them unallocatable because of resources.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   fail_01   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   simple_2vm_vnf
${VNFD_NAME}   simple_2vm-vnf
${NSD_PKG}   simple_2vm_ns
${NSD_NAME}   simple_2vm-ns

# NS instance name and configuration
${NS_NAME_PREFIX}   fail_01
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub

# Default memory in GB for dataVM VDU (can be overwritten by VM_MEMORY_OVERRIDE environment variable)
# Only used if no quota is defined in Openstack's project (VDU tries to allocate the full memory of the quota)
${DEFAULT_MEMORY}   250

# NS launch timeout and polling time
${NS_LAUNCH_MAX_WAIT_TIME}   5min
${NS_LAUNCH_POL_TIME}   15

# Openstack delete operation timeout and polling time
${OS_DELETE_MAX_WAIT_TIME}   6min
${OS_DELETE_POL_TIME}   20


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    ${ram_quota}=   Get Project Quota   ram
    IF   ${ram_quota} == -1
        ${memory}=   Get Environment Variable   VM_MEMORY_OVERRIDE   default=${DEFAULT_MEMORY}
    ELSE
        ${memory}=   Evaluate   ${ram_quota} / 1000
    END
    Log   ${memory}
    Create VNFD Overriding Fields   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   virtual-compute-desc.1.virtual-memory.size=${memory}

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Cannot Instantiate Network Service
    [Documentation]   Assert failure when trying to instantiate the NS instance and there are not enough resources in the VIM.
    ${rand}=   Generate Random String   4   [NUMBERS]
    ${ns_name}=   Catenate   SEPARATOR=_   ${NS_NAME_PREFIX}   ${rand}
    Set Suite Variable   ${NS_NAME}
    Log   ${NS_NAME}
    ${id}=   Instantiate Network Service   ${NS_NAME}   ${NSD_NAME}   %{VIM_TARGET}   --config '${NS_CONFIG}' --ssh_keys ${PUBLICKEY}
    Log   ${id}
    Set Suite Variable   ${NS_ID}   ${id}
    WAIT UNTIL KEYWORD SUCCEEDS   ${NS_LAUNCH_MAX_WAIT_TIME}   ${NS_LAUNCH_POL_TIME}   Check For Ns Instance To Configured   ${NS_NAME}
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm ns-list --filter id="${NS_ID}" | grep ${NS_ID} | awk '{print $8}'
    Log   ${rc},${stdout}
    Should Contain   ${stdout}   BROKEN
    ${rc}   ${stdout}=   Run And Return Rc And Output   osm ns-show ${NS_ID} --literal | yq .errorDetail
    Log   ${rc},${stdout}
    Should Contain   ${stdout}   Deploying at VIM: Error at create vdu

Delete NS Instance And Check VIM
    [Documentation]   Delete NS instance and check that the VMs have been deleted.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}
    WAIT UNTIL KEYWORD SUCCEEDS   ${OS_DELETE_MAX_WAIT_TIME}   ${OS_DELETE_POL_TIME}   Check NS Servers In VIM   ${NS_NAME}   0

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
