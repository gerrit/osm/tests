*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [GITOPS-01] Advanced Cluster Management.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/advanced_cluster_mgmt_lib.resource

Test Tags   gitops_01   cluster_gitops

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# OKA packages
${OKA_FOLDER}   '%{PACKAGES_FOLDER}/oka'
${OKA1_PKG}   apps/testacme
${OKA1_NAME}   testacme
${OKA1_PROFILE}   app-profile
${OKA2_PKG}   apps/jenkins
${OKA2_NAME}   jenkins
${OKA2_PROFILE}   app-profile
${OKA_NS_PKG}   apps/namespace
${OKA_NS_NAME}   namespace
${OKA_NS_PROFILE}   app-profile

# Cluster
${CLUSTER_NAME}   gitops01
${CLUSTER_DESCRIPTION}   "Cluster for gitops01 Robot test suite"
${CLUSTER_VM_SIZE}   Standard_D2_v2
${CLUSTER_NODES}   2
${REGION_NAME}   northeurope
${RESOURCE_GROUP}   OSM-CTIO
${KUBERNETES_VERSION}   "1.30"

# KSUs
${KSU1_NAME}   testacme
${KSU_NS_NAME}   jenkins-ns
${KSU2_NAME}   jenkins
${KSU3_NAME}   jenkinsbis


*** Test Cases ***
Add OKA Packages For The Testsuite
    [Documentation]   Add OKA packages for the testsuite.
    [Tags]   oka   create
    Add OKA Package   ${OKA_NS_NAME}   '${OKA_FOLDER}/${OKA_NS_PKG}'   ${OKA_NS_PROFILE}
    Add OKA Package   ${OKA1_NAME}   '${OKA_FOLDER}/${OKA1_PKG}'   ${OKA1_PROFILE}
    Add OKA Package   ${OKA2_NAME}   '${OKA_FOLDER}/${OKA2_PKG}'   ${OKA2_PROFILE}

Create Cluster For Testsuite in Azure
    [Documentation]   Create K8s cluster for the testsuite.
    [Tags]   cluster   create
    ${id}=   Create Cluster   ${CLUSTER_NAME}   %{VIM_TARGET}   ${CLUSTER_DESCRIPTION}   ${CLUSTER_VM_SIZE}   ${KUBERNETES_VERSION}   ${CLUSTER_NODES}   ${REGION_NAME}   ${RESOURCE_GROUP}
    Set Suite Variable   ${CLUSTER_ID}   ${id}
    Log   ${CLUSTER_ID}

Get Cluster Kubeconfig
    [Documentation]   Get Kubeconfig for the cluster.
    [Tags]   cluster
    ${rc}   ${stdout}=   Run And Return Rc And Output
    ...    osm cluster-show ${CLUSTER_NAME} -o jsonpath='{.credentials}' | yq -y
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Log   ${stdout}

Create KSU1 Without Params
    [Documentation]   Create KSU based on ${OKA1_NAME} without params.
    [Tags]   ksu   ksu1   create
    ${id}=   Create KSU   ${KSU1_NAME}   ${KSU1_NAME}   ${CLUSTER_NAME}   app-profile   ${OKA1_NAME}
    Set Suite Variable   ${KSU1_ID}   ${id}
    Log   ${KSU1_ID}

Create KSU Namespace For KSU2 and KSU3
    [Documentation]   Create KSU based on ${OKA_NS_NAME} with params.
    [Tags]   ksu   ksu-ns   create
    ${id}=   Create KSU   ${KSU_NS_NAME}   ${KSU_NS_NAME}   ${CLUSTER_NAME}   app-profile   ${OKA_NS_NAME}   ${OKA_FOLDER}/apps/jenkins-ns-params.yaml
    Set Suite Variable   ${KSU_NS_ID}   ${id}
    Log   ${KSU_NS_ID}

Create KSU2 With Params
    [Documentation]   Create KSU based on ${OKA2_NAME} with params.
    [Tags]   ksu   ksu2   create
    ${id}=   Create KSU   ${KSU2_NAME}   ${KSU2_NAME}   ${CLUSTER_NAME}   app-profile   ${OKA2_NAME}   ${OKA_FOLDER}/apps/jenkins-params.yaml
    Set Suite Variable   ${KSU2_ID}   ${id}
    Log   ${KSU2_ID}

Create KSU3 With Params
    [Documentation]   Create KSU based on ${OKA2_NAME} with same params as KSU2.
    [Tags]   ksu   ksu3   create
    ${id}=   Create KSU   ${KSU3_NAME}   ${KSU3_NAME}   ${CLUSTER_NAME}   app-profile   ${OKA2_NAME}   ${OKA_FOLDER}/apps/jenkins-params.yaml
    Set Suite Variable   ${KSU3_ID}   ${id}
    Log   ${KSU3_ID}

Delete KSU3
    [Documentation]   Delete KSU3 based on ${OKA2_NAME}.
    [Tags]   cleanup   ksu   ksu3
    Delete KSU   ${KSU3_ID}

Delete KSU2
    [Documentation]   Delete KSU2 based on ${OKA2_NAME}.
    [Tags]   cleanup   ksu   ksu2
    Delete KSU   ${KSU2_ID}

Delete KSU Namespace
    [Documentation]   Delete KSU based on ${OKA_NS_NAME}.
    [Tags]   cleanup   ksu   ksu-ns
    Delete KSU   ${KSU_NS_ID}

Delete KSU1
    [Documentation]   Delete KSU1 based on ${OKA1_NAME}.
    [Tags]   cleanup   ksu   ksu1
    Delete KSU   ${KSU1_NAME}

Delete Cluster For Testsuite
    [Documentation]   Delete K8s cluster for the testsuite.
    [Tags]   cleanup   cluster
    Delete Cluster   ${CLUSTER_NAME}

Delete OKA Packages For The Testsuite
    [Documentation]   Delete OKA packages for the testsuite.
    [Tags]   cleanup   oka
    Delete OKA Package   ${OKA_NS_NAME}
    Delete OKA Package   ${OKA1_NAME}
    Delete OKA Package   ${OKA2_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting cluster, KSUs, OKAs
    Run Keyword If Any Tests Failed   Delete Cluster   ${CLUSTER_NAME}
    Run Keyword If Any Tests Failed   Delete OKA Package   ${OKA_NS_NAME}
    Run Keyword If Any Tests Failed   Delete OKA Package   ${OKA1_NAME}
    Run Keyword If Any Tests Failed   Delete OKA Package   ${OKA2_NAME}
