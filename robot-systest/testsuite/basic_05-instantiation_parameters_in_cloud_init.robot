*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-05] Instantiation parameters in cloud-init.

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_05   cluster_main   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   ubuntu_cloudinit_vnf
${VNFD_NAME}   ubuntu_cloudinit-vnf
${NSD_PKG}   ubuntu_cloudinit_ns
${NSD_NAME}   ubuntu_cloudinit-ns

# NS instance name and configuration
${NS_NAME}   basic_05_instantiation_params_cloud_init
${VNF_MEMBER_INDEX}   vnf
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ], additionalParamsForVnf: [ { member-vnf-index: "${VNF_MEMBER_INDEX}", additionalParams: { password: "${NEW_PASSWORD}" } } ] }

# SSH user and password
${USERNAME}   ubuntu
${NEW_PASSWORD}   newpassword


*** Test Cases ***
Create Cloudinit VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Cloudinit NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Cloudinit Network Service Using Instantiation Parameters
    [Documentation]   Instantiates the NS using the instantiation parameter 'additionalParamsForVnf' to change the password of the default user.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${EMPTY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Addresses
    [Documentation]   Retrieve VNF mgmt IP address from OSM.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test SSH Access With The New Password
    [Documentation]   Test SSH access with the new password configured via cloud-init.
    Variable Should Exist   ${VNF_IP_ADDR}   msg=IP address of the management VNF is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${NEW_PASSWORD}   ${EMPTY}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
