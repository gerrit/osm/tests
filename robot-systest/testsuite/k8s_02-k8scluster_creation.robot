*** Comments ***
# Copyright 2020 Canonical Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-02] K8s cluster addition.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/k8scluster_lib.resource
Resource   ../lib/vim_lib.resource

Variables   ../resources/basic_01-crud_operations_on_vim_targets_data.py

Test Tags   k8s_02   cluster_k8s   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# K8s cluster name and version
${K8SCLUSTER_NAME}   k8s02
${K8SCLUSTER_VERSION}   v1


*** Test Cases ***
Create VIM Target Basic
    [Documentation]   Creates a VIM for the K8s cluster to be anchored against
    ${rand}=   Generate Random String   6   [NUMBERS]
    ${VIM_NAME}=   Catenate   SEPARATOR=_   ${vim_name_prefix}   ${rand}
    Set Suite Variable   ${VIM_NAME}
    ${CREATED_VIM_ACCOUNT_ID}=   Create VIM Target   ${VIM_NAME}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}

Add K8s Cluster To OSM
    [Documentation]   Creates a VIM for the K8s cluster to be anchored against
    Create K8s Cluster   %{K8S_CREDENTIALS}   ${K8SCLUSTER_VERSION}   ${VIM_NAME}   %{VIM_MGMT_NET}   ${K8SCLUSTER_NAME}
    Check For K8s Cluster To Be Ready   ${K8SCLUSTER_NAME}

Remove K8s Cluster from OSM
    [Documentation]   Delete K8s cluster.
    [Tags]   cleanup
    Delete K8s Cluster   ${K8SCLUSTER_NAME}

Delete VIM Target By ID
    [Documentation]   Delete the VIM Target created in previous test-case by its ID.
    ...               Checks whether the VIM Target was created or not before perform the deletion.
    [Tags]   cleanup

    ${vim_account_id}=   Get VIM Target ID   ${VIM_NAME}
    Should Be Equal As Strings   ${vim_account_id}   ${CREATED_VIM_ACCOUNT_ID}
    Delete VIM Target   ${vim_account_id}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting K8s Cluster

    Run Keyword If Any Tests Failed   Delete K8s Cluster   ${K8SCLUSTER_NAME}
