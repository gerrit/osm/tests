*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-08] Disable port security at network level.

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/vim_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/prometheus_lib.resource

Variables   ../resources/basic_08-disable_port_security_network_level_data.py

Test Tags   basic_08   cluster_main   daily   regression

Suite Setup   Run Keyword And Ignore Error   Suite Preparation
Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_multivdu_vnf
${VNFD_NAME}   hackfest_multivdu-vnf
${NSD_PKG}   hackfest_multivdu_ns
${NSD_NAME}   hackfest_multivdu-ns

# NS instance name and configuration
${NS_NAME}   basic_08_disable_port_security_network_level_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub

# Other variables
${PORT_DISABLED_MSG}   port_security_enabled: false


*** Test Cases ***
Create VIM With Port Security Disabled
    [Documentation]   Register an Opentack VIM with port security disabled, so that all ports created from OSM have port security disabled
    ...               (no firewall).
    ${created_vim_account_id}=   Create VIM Target   ${VIM_NAME}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}   config=${VIM_CONFIG}
    Log   ${created_vim_account_id}
    Check For VIM Target Status   ${VIM_NAME}   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}

Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   ${VIM_NAME}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Check Port Security Is Disabled
    [Documentation]   Check that there are ports/interfaces in the VDU of the VNF of the NS whose port security was disabled.
    ${rc}   ${disabled_ports}=   Run And Return RC And Output   osm ns-show ${NS_NAME} --literal | grep -c '${PORT_DISABLED_MSG}'
    Log   ${rc},${disabled_ports}
    IF   ${disabled_ports} <= 0   Fail   msg=Found '${disabled_ports}' matches for '${PORT_DISABLED_MSG}'

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}

Delete VIM
    [Documentation]   Delete VIM from OSM.
    [Tags]   cleanup
    Delete VIM Target   ${VIM_NAME}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables

Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Delete VIM Target   ${VIM_NAME}
