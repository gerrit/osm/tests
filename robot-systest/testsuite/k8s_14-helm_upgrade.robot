*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-14] Helm upgrade using --reset-values, --reuse-values flags.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   k8s_14   cluster_k8s   daily   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   openldap_knf
${VNFD_NAME}   openldap_knf
${NSD_PKG}   openldap_ns
${NSD_NAME}   openldap_ns

# NS instance name and configuration
${NS_NAME}   ldap
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}}], additionalParamsForVnf: [ {member-vnf-index: openldap, additionalParamsForKdu: [ {kdu_name: ldap, additionalParams: {adminPassword: admin}} ] } ] }

${NS_ID}   ${EMPTY}
${PUBLICKEY}   ${EMPTY}
${VNF_MEMBER_INDEX}   openldap
${KDU_NAME}   ldap
${UPGRADE_ACTION}   upgrade

${INITIAL_PARAM_1_VALUE}   "admin"
${PARAM_1}   adminPassword
${PARAM_2}   appUser
${PARAM_1_REUSE_VALUE}   reusePasswd
${PARAM_2_REUSE_VALUE}   Reuser
${PARAM_1_RESET_VALUE}   resetPasswd
${PARAM_2_RESET_VALUE}   Resetter
${PARAM_1_VALUE_AFTER_RESET_1}   ${EMPTY}
${PARAM_2_VALUE_AFTER_RESET_1}   ${EMPTY}
${PARAM_1_VALUE_AFTER_REUSE_2}   "reusePasswd"
${PARAM_2_VALUE_AFTER_REUSE_2}   "Reuser"
${PARAM_1_VALUE_AFTER_RESET_2}   "resetPasswd"
${PARAM_2_VALUE_AFTER_RESET_2}   "Resetter"


*** Test Cases ***
Create Package For OpenLDAP CNF
    [Documentation]   Upload NF package for the testsuite.
    [Tags]   prepare
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Package For OpenLDAP NS
    [Documentation]   Upload NS package for the testsuite.
    [Tags]   prepare
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Create Network Service Instance
    [Documentation]   Instantiate NS for the testsuite.
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Log   ${id}

Get Ns Id
    [Documentation]   Retrieve NS instance id to be used later on.
    [Tags]   verify
    ${id}=   Get Ns Id   ${NS_NAME}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Retrieve NF instance id to be used later on.
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Execute First NS Upgrade Using KDU_REUSE Flag Set To True
    [Documentation]   Upgrade the NS using values files with reuse-values flag true
    [Tags]   verify
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   kdu_reuse_values=True
    Log   ${ns_op_id}

Get Additional Params And Values From Kdu After First Upgrade with Reuse True
    [Documentation]   Get values from the NS by after upgrading with reuse-values
    [Tags]   verify
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_1}
    Should Be Equal   ${param_value}   ${INITIAL_PARAM_1_VALUE}

Execute Second NS Upgrade Using KDU_REUSE Flag Set To True
    [Documentation]   Upgrade the NS using values files with reuse-values flag true changing existing value and adding a new parameter
    [Tags]   verify
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   ${PARAM_1}=${PARAM_1_REUSE_VALUE}   ${PARAM_2}=${PARAM_2_REUSE_VALUE}
    Log   ${ns_op_id}

Get Additional Params And Values From Kdu After Second Upgrade with Reuse True
    [Documentation]   Get values from the NS by after upgrading with reuse-values
    [Tags]   verify
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_1}
    Should Be Equal   ${param_value}   ${PARAM_1_VALUE_AFTER_REUSE_2}
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_2}
    Should Be Equal   ${param_value}   ${PARAM_2_VALUE_AFTER_REUSE_2}

Execute First NS Upgrade Using KDU_RESET Flag Set To True
    [Documentation]   Upgrade the NS using values files with reset-values flag true
    [Tags]   verify
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   kdu_reset_values=True
    Log   ${ns_op_id}

Get Additional Params And Values From Kdu After Reset
    [Documentation]   Get values from the NS by after upgrading with reset-values
    [Tags]   verify
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_1}
    Should Be Equal   ${param_value}   ${PARAM_1_VALUE_AFTER_RESET_1}
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_2}
    Should Be Equal   ${param_value}   ${PARAM_2_VALUE_AFTER_RESET_1}

Execute Second NS Upgrade Using KDU_RESET Flag Set To True and additional params
    [Documentation]   Upgrade the NS using values files with reset-values flag true and also passing additional params
    [Tags]   verify
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   ${PARAM_1}=${PARAM_1_RESET_VALUE}   ${PARAM_2}=${PARAM_2_RESET_VALUE}
    Log   ${ns_op_id}

Get Additional Params And Values From Kdu After Second Reset
    [Documentation]   Get values from the NS by after upgrading with additional values and reset-values flag set to True
    [Tags]   verify
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_1}
    Should Be Equal   ${param_value}   ${PARAM_1_VALUE_AFTER_RESET_2}
    ${param_value}=   Get Additional Params And Values From Kdu   ${NS_NAME}   ${PARAM_2}
    Should Be Equal   ${param_value}   ${PARAM_2_VALUE_AFTER_RESET_2}

Delete Network Service Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete NF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
