*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [HACKFEST-MULTIVDU] Basic NS with two multi-VDU VNF

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   hackfest_multivdu   cluster_main   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_multivdu_vnf
${VNFD_NAME}   hackfest_multivdu-vnf
${NSD_PKG}   hackfest_multivdu_ns
${NSD_NAME}   hackfest_multivdu-ns

# NS instance name and configuration
${NS_NAME}   hfmultivdu
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${NS_ID}   ${EMPTY}
${VNF_MEMBER_INDEX}   vnf1
${VNF_IP_ADDR}   ${EMPTY}
${WAIT_GUARD_FOR_VM_BOOT}   50s


*** Test Cases ***
Create Hackfest multivdu VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Hackfest Multivdu NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${status}   ${message}=   Run Keyword And Ignore Error   Variable Should Exist   ${PUBLICKEY}
    Log   ${status},${message}
    IF   "${status}" == "FAIL"   Set Global Variable   ${PUBLICKEY}   ${EMPTY}
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}
    Sleep   ${WAIT_GUARD_FOR_VM_BOOT}   Waiting for VM's daemons to be up and running

Get Vnf Ip Address
    [Documentation]   Get the mgmt IP address of the VNF of the NS.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test Ping
    [Documentation]   Test that the mgmt IP address of the VNF is reachable with ping.
    Test Connectivity   ${VNF_IP_ADDR}

Test SSH Access
    [Documentation]   Check that the VNF is accessible via SSH in its mgmt IP address.
    ${status}   ${message}=   Run Keyword And Ignore Error   Variable Should Exist   ${PRIVATEKEY}
    Log   ${status},${message}
    IF   "${status}" == "FAIL"   Set Global Variable   ${PRIVATEKEY}   ${EMPTY}
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
