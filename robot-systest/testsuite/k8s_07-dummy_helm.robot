*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-07] Openldap Helm in isolated cluster with dummy VIM.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vim_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   k8s_07   cluster_k8s   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# VIM Configuration
${VIM_NAME}   k8s07_dummy
${VIM_USER}   user
${VIM_PASSWORD}   pass
${VIM_AUTH_URL}   http://localhost/dummy
${VIM_TENANT}   tenant
${VIM_ACCOUNT_TYPE}   dummy

# K8s cluster data
${K8SCLUSTER_NAME}   k8s07
${K8SCLUSTER_VERSION}   v1
${K8SCLUSTER_NET}   null

# NS and VNF descriptor package files
${VNFD_PKG}   openldap_knf
${NSD_PKG}   openldap_ns
${VNFD_NAME}   openldap_knf
${NSD_NAME}   openldap_ns

# NS instance name
${NS_NAME}   ldap

${NS_ID}   ${EMPTY}
${NS_CONFIG}   ${EMPTY}
${PUBLICKEY}   ${EMPTY}


*** Test Cases ***
Create Package For OpenLDAP CNF
    [Documentation]   Upload NF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Package For OpenLDAP NS
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Create Dummy VIM
    [Documentation]   Register a VIM of type dummy in OSM.
    ${created_vim_account_id}=   Create VIM Target   ${VIM_NAME}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}
    Log   ${created_vim_account_id}

Add K8s Cluster To OSM
    [Documentation]   Register a K8s cluster associated to the dummy VIM.
    Create K8s Cluster   %{K8S_CREDENTIALS}   ${K8SCLUSTER_VERSION}   ${VIM_NAME}   ${K8SCLUSTER_NET}   ${K8SCLUSTER_NAME}
    Check For K8s Cluster To Be Ready   ${K8SCLUSTER_NAME}

Create Network Service Instance
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   ${VIM_NAME}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Delete Network Service Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Remove K8s Cluster from OSM
    [Documentation]   Remove K8s cluster from OSM.
    [Tags]   cleanup
    Delete K8s Cluster   ${K8SCLUSTER_NAME}

Delete VIM
    [Documentation]   Remove VIM from OSM.
    [Tags]   cleanup
    Delete VIM Target   ${VIM_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete NF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance, cluster and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Delete K8s Cluster   ${K8SCLUSTER_NAME}
    Run Keyword If Any Tests Failed   Delete VIM Target   ${VIM_NAME}
