*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-15] RBAC Configurations.

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/user_lib.resource
Resource   ../lib/project_lib.resource
Resource   ../lib/role_lib.resource

Test Tags   basic_15   cluster_main   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# User, project and roles to use
${USERNAME}   basic_15_test_user
${USER_PASSWORD}   Basic_15_user_pass
${USER_NEW_PASSWORD}   Basic_15_new_pass
${USER_ROLE}   project_user
${USER_PROJECT}   admin
${PROJECT_NAME}   basic_15_test_project
${NEW_PROJECT_NAME}   basic_15_project_test
${ROLE_NAME}   test_role
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create And Validate User
    [Documentation]   Create a user in OSM.
    Create User   ${USERNAME}   ${USER_PASSWORD}
    Check If User Exists   ${USERNAME}

Assign Role To User
    [Documentation]   Assign a user to a project with a role
    Update User Role   ${USERNAME}   ${USER_PROJECT}   ${USER_ROLE}
    Check If User Is Assigned To Project   ${USERNAME}   ${USER_PROJECT}
    Check If User Has Role   ${USERNAME}   ${USER_ROLE}   ${USER_PROJECT}

Run Action As User
    [Documentation]   Update the password of the user.
    Update User Password   ${USERNAME}   ${USER_PASSWORD}   ${USER_NEW_PASSWORD}
    ${rc}   ${stdout}=   Run And Return RC And Output   OSM_USER=${USERNAME} OSM_PROJECT=${USER_PROJECT} OSM_PASSWORD=${USER_NEW_PASSWORD} osm ns-list
    Log   ${rc}
    Log   ${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}

Create And Update Project
    [Documentation]   Create a project and update its name.
    Create Project   ${PROJECT_NAME}
    Update Project Name   ${PROJECT_NAME}   ${NEW_PROJECT_NAME}

Create And Validate Role
    [Documentation]   Create a role and check that exists in OSM.
    Create Role   ${ROLE_NAME}
    Check If Role Exists   ${ROLE_NAME}

Update Role Information
    [Documentation]   Update a role with a capability.
    Update Role   ${ROLE_NAME}   add='vims: true'
    Check If Role Exists   ${ROLE_NAME}

Delete Allocated Resources
    [Documentation]   Delete user, project and role.
    [Tags]   cleanup
    Delete User   ${USERNAME}
    Delete Project   ${NEW_PROJECT_NAME}
    Delete Role   ${ROLE_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting users, roles and projects.
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete User   ${USERNAME}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete Role   ${ROLE_NAME}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete Project   ${PROJECT_NAME}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete Project   ${NEW_PROJECT_NAME}
