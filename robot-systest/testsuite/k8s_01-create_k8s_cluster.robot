*** Comments ***
#   Copyright 2020 Canonical Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-01] Create K8s cluster.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary
Library   ../lib/renderTemplate.py

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   k8s_01   cluster_k8s

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package files
${VNFD_PKG1}   k8s_jujucontroller_vnf.tar.gz
${VNFD_PKG2}   k8s_jujumachine_vnf.tar.gz
${NSD_PKG}   k8s_juju_ns.tar.gz
${VNFD_NAME1}   k8s_jujucontroller_vnf
${VNFD_NAME2}   k8s_jujumachine_vnf
${NSD_NAME}   k8s_juju

# VNF Member indexes
${VNF_MEMBER_INDEX_1}   k8s_vnf1
${VNF_MEMBER_INDEX_2}   k8s_vnf2
${VNF_MEMBER_INDEX_3}   k8s_vnf3
${VNF_MEMBER_INDEX_4}   k8s_vnf4
${VNF_MEMBER_INDEX_5}   k8s_juju

# Kubeconfig file
${KUBECONFIG_FILE}   /home/ubuntu/.kube/config

# NS instance name
${NS_NAME}   k8s-cluster

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

# Template and config file to use
${TEMPLATE}   k8s_juju_template.yaml
${CONFIG_FILE}   config.yaml


*** Test Cases ***
Render a template
    [Documentation]   Generate from a template the instantiation parameters that will be used for the NS instance.
    ${stdout}=   Render Template   ../resources/${TEMPLATE}   ../resources/${CONFIG_FILE}   IP_VM1=%{IP_VM1}   IP_VM2=%{IP_VM2}   IP_VM3=%{IP_VM3}   IP_VM4=%{IP_VM4}   IP_JUJU=%{IP_JUJU}   NETWORK=%{VIM_MGMT_NET}
    Log To Console   \n${stdout}

Create Controller VNF Descriptor
    [Documentation]   Upload first VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG1}'

Create Machines VNF Descriptor
    [Documentation]   Upload second VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG2}'

Create K8s Cluster NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate K8s Cluster Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ns_config=${EMPTY}   publickey=${PUBLICKEY}   ns_launch_max_wait_time=70min   config_file=%{ROBOT_DEVOPS_FOLDER}/resources/${CONFIG_FILE}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Addresses
    [Documentation]   Get the mgmt IP addresses of the five VNF of the NS.
    ${ip_addr_1}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Log   ${ip_addr_1}
    Set Suite Variable   ${VNF_1_IP_ADDR}   ${ip_addr_1}
    ${ip_addr_2}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_2}
    Log   ${ip_addr_2}
    Set Suite Variable   ${VNF_2_IP_ADDR}   ${ip_addr_2}
    ${ip_addr_3}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_3}
    Log   ${ip_addr_3}
    Set Suite Variable   ${VNF_3_IP_ADDR}   ${ip_addr_3}
    ${ip_addr_4}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_4}
    Log   ${ip_addr_4}
    Set Suite Variable   ${VNF_4_IP_ADDR}   ${ip_addr_4}
    ${ip_addr_5}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_5}
    Log   ${ip_addr_5}
    Set Suite Variable   ${VNF_5_IP_ADDR}   ${ip_addr_5}

Test SSH Access
    [Documentation]   Check that the five VNF are accessible via SSH in their respective mgmt IP address.
    Variable Should Exist   ${VNF_1_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_1}' is not available
    Variable Should Exist   ${VNF_2_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_2}' is not available
    Variable Should Exist   ${VNF_3_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_3}' is not available
    Variable Should Exist   ${VNF_4_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_4}' is not available
    Variable Should Exist   ${VNF_5_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_5}' is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_1_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}
    Test SSH Connection   ${VNF_2_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}
    Test SSH Connection   ${VNF_3_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}
    Test SSH Connection   ${VNF_4_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}
    Test SSH Connection   ${VNF_5_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Check Kubeconfig File
    [Documentation]   Check that the Kubeconfig file of the provisioned cluster is present in the VNF.
    Check If Remote File Exists   ${VNF_5_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${KUBECONFIG_FILE}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete Controller VNF Descriptor
    [Documentation]   Delete first VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME1}

Delete Machines VNF Descriptor
    [Documentation]   Delete second VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME2}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME1}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME2}
