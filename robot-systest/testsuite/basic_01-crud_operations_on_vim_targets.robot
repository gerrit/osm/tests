*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-01] CRUD operations on VIM targets.
...             All tests will be performed over an Openstack VIM, and the credentials will be loaded from clouds.yaml file.

Resource   ../lib/vim_lib.resource
Resource   ../lib/prometheus_lib.resource

Variables   ../resources/basic_01-crud_operations_on_vim_targets_data.py

Test Tags   basic_01   cluster_main   daily   regression

Suite Setup   Run Keyword And Ignore Error   Suite Preparation


*** Test Cases ***
Create VIM Target Basic
    [Documentation]   Create a VIM Target only with the mandatory parameters.
    ...               Checks the status of the VIM in Prometheus after it creation.
    Pass Execution If   ${VIM_ACCOUNT_TYPE} != 'openstack'   Not applicable for ${VIM_ACCOUNT_TYPE} VIM
    ${rand}=   Generate Random String   6   [NUMBERS]
    ${VIM_NAME}=   Catenate   SEPARATOR=_   ${vim_name_prefix}   ${rand}
    Set Suite Variable   ${VIM_NAME}
    ${CREATED_VIM_ACCOUNT_ID}=   Create VIM Target   ${VIM_NAME}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}
    Check VIM Target Operational State   ${VIM_NAME}
    ${stdout}=   Check For VIM Target Metric   ${VIM_NAME}   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}
    Log   ${stdout}

Delete VIM Target By Name
    [Documentation]   Delete the VIM Target created in previous test-case by its name.
    ...               Checks whether the VIM Target was created or not before perform the deletion.
    [Tags]   cleanup
    Pass Execution If   ${VIM_ACCOUNT_TYPE} != 'openstack'   Not applicable for ${VIM_ACCOUNT_TYPE} VIM
    ${vim_account_id}=   Get VIM Target ID   ${VIM_NAME}
    Should Be Equal As Strings   ${vim_account_id}   ${CREATED_VIM_ACCOUNT_ID}
    Delete VIM Target   ${VIM_NAME}

Create VIM Target With Extra Config
    [Documentation]   Create a VIM Target using the extra parameter 'config'.
    ...               Checks the status of the VIM in Prometheus after it creation.
    ${rand}=   Generate Random String   6   [NUMBERS]
    ${VIM_NAME}=   Catenate   SEPARATOR=_   ${vim_name_prefix}   ${rand}
    Set Suite Variable   ${VIM_NAME}
    ${CREATED_VIM_ACCOUNT_ID}=   Create VIM Target   ${VIM_NAME}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}   config=${VIM_CONFIG}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}
    Check VIM Target Operational State   ${VIM_NAME}
    Check For VIM Target Status   ${VIM_NAME}   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}

Delete VIM Target By ID
    [Documentation]   Delete the VIM Target created in previous test-case by its ID.
    ...               Checks whether the VIM Target was created or not before perform the deletion.
    [Tags]   cleanup
    ${vim_account_id}=   Get VIM Target ID   ${VIM_NAME}
    Should Be Equal As Strings   ${vim_account_id}   ${CREATED_VIM_ACCOUNT_ID}
    Delete VIM Target   ${vim_account_id}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables
