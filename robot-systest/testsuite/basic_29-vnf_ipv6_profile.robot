*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-29] NS with a single VNF and two VDU linked by a VLD with ipv6-profile.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary
Library   JSONLibrary

Resource   ../lib/vnf_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   basic_29   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   ipv6profile_2vm_vnf
${VNFD_NAME}   ipv6profile_2vm-vnf
${NSD_PKG}   ipv6profile_2vm_ns
${NSD_NAME}   ipv6profile_2vm-ns

# NS instance name and configuration
${NS_NAME}   basic_29
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${USERNAME}   ubuntu

# VNFs data
${VNF_MEMBER_INDEX_1}   vnf
${INTERNAL_PREFIX}   ^2001:db8::*

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Info
    [Documentation]   Get VNF information, specifically VNF instance id and mgmt IP address.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Set Suite Variable   ${VNF_IPMGMT}   ${ip}
    Log   ${VNF_IPMGMT}

Check Vnf IPs
    [Documentation]   Check whether IP addresses are syntactically valid.
    Variable Should Exist   ${VNF_ID}   msg=VNF is not available
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq '.vdur[0].interfaces[] | select(."vnf-vld-id" == "internal")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${INTERNAL_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${INTERNAL_PREFIX}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq '.vdur[1].interfaces[] | select(."vnf-vld-id" == "internal")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${INTERNAL_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${INTERNAL_PREFIX}

Check That Ipv6 Address_mode And Ra_mode Are Set
    [Documentation]   Check that IPv6 address mode and RA mode are set and equal to dhcpv6-stateful.
    ${rc}   ${vim_info}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq '.vdur[] | select(."count-index" == 0)' | yq -r '.vim_info[].interfaces[].vim_info'
    Log   ${rc},${vim_info}
    ${subnet_id}=   Get Regexp Matches   ${vim_info}   {ip_address: '2001:db8::.*', subnet_id: ([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})}   1
    ${subnet_info}=   Get Subnet   ${subnet_id}[0]
    ${json_object}=   Convert String To JSON   ${subnet_info}
    ${address_mode}=   Get From Dictionary   ${json_object}   ipv6_address_mode
    ${ra_mode}=   Get From Dictionary   ${json_object}   ipv6_ra_mode
    Should Be Equal   ${address_mode}   dhcpv6-stateful   msg=ipv6_address_mode does not equals to dhcpv6-stateful
    Should Be Equal   ${ra_mode}   dhcpv6-stateful   msg=ipv6_ra_mode does not equals to dhcpv6-stateful

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
