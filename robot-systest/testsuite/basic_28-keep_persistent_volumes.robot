*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-28] NS with one VDU which has 2 persistent volumes with keep flag

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   basic_28   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   keep_persistent_volume_vnf
${VNFD_NAME}   keep_persistent-volumes-vnf
${NSD_PKG}   keep_persistent_volume_ns
${NSD_NAME}   persistent_volumes-ns

# NS instance name and configuration
${NS_NAME}   basic_28
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${VNF_MEMBER_INDEX}   vnf-persistent-volumes
${VDU_ID}   keep-persistent-vol-VM
${ROOT_DISK}   root-volume
${ORDINARY_DISK}   persistent-volume
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Check Persistent Volume Counts
    [Documentation]   Check that the number of persistent volumes attached to VM in the NS meet the expected number (2).
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    ${VOLUME_MATCH}=   Get Persistent Volumes Attached To Vm   ${NS_ID}
    Set Suite Variable   ${VOLUME_MATCH}
    ${VOLUME_COUNTS}=   Get Length   ${VOLUME_MATCH}
    Set Suite Variable   ${VOLUME_COUNTS}
    IF   ${VOLUME_COUNTS} != 2   Fail   msg=There were not 2 volumes attached to VM

Set Persistent Volume Ids
    [Documentation]   Get the identifiers of the persistent volumes attached to VM in the NS and store in suite variables ROOT_VOL_ID and ORDINARY_VOL_ID.
    Variable Should Exist   ${VOLUME_MATCH}   msg=NS is not available
    Set Volume Id   ${VOLUME_MATCH}   0
    Set Volume Id   ${VOLUME_MATCH}   1

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    Delete NS   ${NS_NAME}

Check Persistent Root Volume in VIM after NS Deleted
    [Documentation]   Check that the persistent root volume requested to be kept is present after deleting NS instance.
    Variable Should Exist   ${ROOT_VOL_ID}   msg=Root volume is not available
    ${root_vol_exists}=   Check If Volume Exists   ${ROOT_VOL_ID}
    Log   ${root_vol_exists}
    IF   ${root_vol_exists}
        Log   Persistent root volume still exists
    ELSE
        Fail   msg=Persistent root volume was deleted
        Set Suite Variable   ${ROOT_VOL_ID}   ${EMPTY}
    END

Check Persistent Ordinary Volume in VIM after NS Deleted
    [Documentation]   Check that the persistent ordinary volume requested to be kept is present after deleting NS instance.
    Variable Should Exist   ${ORDINARY_VOL_ID}   msg=Ordinary persistent volume is not available
    ${ordinary_vol_exists}=   Check If Volume Exists   ${ORDINARY_VOL_ID}
    Log   ${ordinary_vol_exists}
    IF   ${ordinary_vol_exists}
        Log   Persistent ordinary volume still exists
    ELSE
        Fail   msg=Persistent ordinary volume was deleted
        Set Suite Variable   ${ORDINARY_VOL_ID}   ${EMPTY}
    END

Prepare Ns Config With Volume Id
    [Documentation]   Generate NS instantiation parameters to be used in next NS instance,
    ...               using the persistent volumes that were kept previously.
    Set Suite Variable   ${NS_CONFIG_WITH_VIM_ID}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ], vnf: [ {member-vnf-index: ${VNF_MEMBER_INDEX}, vdu: [ {id: ${VDU_ID}, volume: [{"name": ${ROOT_DISK}, vim-volume-id: ${ROOT_VOL_ID}}, {"name": ${ORDINARY_DISK}, vim-volume-id: ${ORDINARY_VOL_ID}}] } ] } ] }

Network Service Instance With Existing Volumes Test
    [Documentation]   Instantiate NS for the testsuite again. The persistent volumes passed as instantiation parameters should be used.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG_WITH_VIM_ID}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Verify the Attachment Of Existing Persistent Volumes
    [Documentation]   Get the identifiers of the persistent volumes attached to VM in the second NS instance and verify that they are the same
    ...               of the first NS instance.
    Set Suite Variable   ${PREV_ROOT_VOL_ID}   ${ROOT_VOL_ID}
    Set Suite Variable   ${PREV_ORDINARY_VOL_ID}   ${ORDINARY_VOL_ID}
    ${volume_match}=   Get Persistent Volumes Attached To Vm   ${NS_ID}
    Set Suite Variable   ${VOLUME_MATCH}
    Set Volume Id   ${VOLUME_MATCH}   0
    Set Volume Id   ${VOLUME_MATCH}   1
    Should Be Equal As Strings   ${ROOT_VOL_ID}   ${PREV_ROOT_VOL_ID}   msg=Existing root volume is not attached to server
    Should Be Equal As Strings   ${ORDINARY_VOL_ID}   ${PREV_ORDINARY_VOL_ID}   msg=Existing ordinary volume is not attached to server

Delete NS Instance With Existing Volumes Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Check Existence of Persistent Volumes
    [Documentation]   Check that the persistent volumes exist after deleting the NS.
    Variable Should Exist   ${ROOT_VOL_ID}   msg=Volume is not available
    Variable Should Exist   ${ORDINARY_VOL_ID}   msg=Volume is not available
    ${root_vol_exists}=   Check If Volume Exists   ${ROOT_VOL_ID}
    IF   not ${root_vol_exists}   Fail   msg=Existing root volume is deleted
    ${ordinary_vol_exists}=   Check If Volume Exists   ${ORDINARY_VOL_ID}
    IF   not ${ordinary_vol_exists}   Fail   msg=Existing ordinary volume is deleted

Delete Persistent Volumes
    [Documentation]   Delete persistent volumes.
    Clean Persistent Volumes

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Clean Persistent Volumes

Clean Persistent Volumes
    [Documentation]   Delete root and ordinary volumes.
    [Tags]   cleanup
    Delete Volume   ${ROOT_VOL_ID}
    Delete Volume   ${ORDINARY_VOL_ID}
