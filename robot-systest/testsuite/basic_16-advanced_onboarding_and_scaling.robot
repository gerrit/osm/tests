*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-16] Advanced onboarding with override and complex scaling (3 initial instances, scaled by two).

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_16   cluster_main   daily   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_basic_metrics_vnf
${VNFD_NAME}   hackfest_basic_metrics-vnf
${NSD_PKG}   hackfest_basic_metrics_ns
${NSD_NAME}   hackfest_basic-ns-metrics

# NS instance name and configuration
${NS_NAME}   basic_16
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH public keys file
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub

# Initial, delta, min and max number of VDU instances
${INIT_INSTANCES}   3
${MIN_INSTANCES}   3
${MAX_INSTANCES}   5
${DELTA_INSTANCES}   2
${SCALING_GROUP}   vdu_autoscale
${VNF_MEMBER_INDEX}   vnf


*** Test Cases ***
Create Scaling VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD Overriding Fields   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   df.0.instantiation-level.0.vdu-level.0.number-of-instances=${INIT_INSTANCES};df.0.vdu-profile.0.min-number-of-instances=${MIN_INSTANCES};df.0.vdu-profile.0.max-number-of-instances=${MAX_INSTANCES};df.0.scaling-aspect.0.aspect-delta-details.deltas.0.vdu-delta.0.number-of-instances=${DELTA_INSTANCES}

Create Scaling NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Scaling Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Retrieve VNF instance id to be used later on.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Check Vdus Before Scale Out
    [Documentation]   Check the number of VDUs instances before the manual scaling.
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    Set Suite Variable   ${INITIAL_VDUR_COUNT}   ${vdurs}
    IF   ${INIT_INSTANCES} != ${INITIAL_VDUR_COUNT}   Fail   msg=Instantiated VDUs (${INITIAL_VDUR_COUNT}) don't match specified number in descriptor (${INIT_INSTANCES})

Perform Manual Vdu Scale Out
    [Documentation]   Perform a manual scale-out operation of the VNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_OUT
    Log   ${ns_op_id}

Check Vdus After Scale Out
    [Documentation]   Check whether there is one extra VDU after scaling out.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    IF   ${vdurs} != ${INITIAL_VDUR_COUNT} + ${DELTA_INSTANCES}   Fail   msg=VDU instances in the VNF (${vdurs}) don't match after Scale Out (${INITIAL_VDUR_COUNT} + ${DELTA_INSTANCES})

Perform Manual Vdu Scale In
    [Documentation]   Perform a manual scale-in operation of the VNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_IN
    Log   ${ns_op_id}

Check Vdus After Scaling In
    [Documentation]   Check whether there is one less VDU after scaling in.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    IF   ${vdurs} != ${INITIAL_VDUR_COUNT}   Fail   msg=There is the same number of VDU records in the VNF after Scale In

Delete NS Instance
    [Documentation]   Delete NS intance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
