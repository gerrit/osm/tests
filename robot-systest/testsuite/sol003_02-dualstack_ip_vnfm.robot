*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License


*** Settings ***
Documentation   [Sol003-02] Sol003 Dual Stack IP in VNFM NB interface

Library   OperatingSystem
Library   String
Library   Collections
Library   RequestsLibrary
Library   yaml
Library   JsonValidator
Library   JSONLibrary

Resource   ../lib/rest_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/vim_lib.resource
Resource   ../lib/nsd_lib.resource

Test Tags   sol003_02   cluster_osm_rest   daily

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# SOL003 API URI paths
${VNF_INSTANCE_URI}   /osm/vnflcm/v1/vnf_instances
${VNF_INSTANCE_LCM_OPS}   /osm/vnflcm/v1/vnf_lcm_op_occs

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_basic_vnf
${VNFD_NAME}   hackfest_basic-vnf


*** Test Cases ***
Create VNFD Descriptor
    [Documentation]   Test case to create a VNF Identifier
    ${VNFD_ID}=   Create VNFD   %{PACKAGES_FOLDER}/${VNFD_PKG}
    Set Suite Variable   ${VNFD_ID}
    ${CREATED_VIM_ACCOUNT_ID}=   Get VIM Target ID   %{VIM_TARGET}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}
    Get Auth Token
    ${json_path}=   Read Directory
    ${json_obj}=   Load JSON From File   ${json_path}/sol003_02-dualstack_ip_vnfm.json
    ${json_obj}=   Get Variable Value   ${json_obj['data'][0]}
    ${NS_NAME}=   Get Variable Value   ${json_obj['vnfInstanceName']}
    Set Suite Variable   ${NS_NAME}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${json_obj}   $..vimAccountId   ${CREATED_VIM_ACCOUNT_ID}
    Log   ${UPDATED_JSON_OBJ}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..additionalParams.virtual-link-desc[0][id]   %{VIM_MGMT_NET}
    Log   ${UPDATED_JSON_OBJ}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..additionalParams.virtual-link-profile-id   %{VIM_MGMT_NET}
    Log   ${UPDATED_JSON_OBJ}
    Set Test Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}   ${UPDATED_JSON_OBJ}
    Pass Execution If   ${request_response.status_code} in ${success_status_code_list}   Query VNF Instance completed
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${CREATEID}   ${id[0]}
    Get VNFDs List

Instantiate VNF
    [Documentation]   Test case to Instantiate a VNF
    Get Ns List
    Get Vnf List
    Get Auth Token
    ${json_path}=   Read Directory
    ${json_obj}=   Load JSON From File   ${json_path}/sol003_02-dualstack_ip_vnfm.json
    ${json_obj}=   Get Variable Value   ${json_obj['data'][1]}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${json_obj}   $..vimAccountId   ${CREATED_VIM_ACCOUNT_ID}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..vnfId   ${CREATEID}
    Set Test Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}/${CREATEID}/instantiate   ${UPDATED_JSON_OBJ}
    Pass Execution If   ${request_response.status_code} in ${success_status_code_list}   Instantiate VNF Instance completed
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${VNF_INSTANCE_ID}   ${id[0]}
    Sleep   12s
    Check For NS Instance For Failure   ${ns_name}
    Get Ns List
    Get Vnf List

Get Ns Id
    [Documentation]   Get the NS instance id
    ${id}=   Get Ns Id   ${ns_name}
    Set Suite Variable   ${NS_ID}   ${id}

Get Ip From The NSD
    [Documentation]   Get the IP address from the NSD
    ${id}=   Get NSD ID From The NS   ${NS_ID}
    Set Suite Variable   ${NSD_ID}   ${id}
    ${Nsd_IP}=   Get NSD Ip   ${NSD_ID}
    Log   ${Nsd_IP}
    ${Nsd_IP}=   Split String   ${Nsd_IP}
    Set Suite Variable   ${IPV6NSD}   ${Nsd_IP}[0]
    Set Suite Variable   ${IPV4NSD}   ${Nsd_IP}[1]
    Log   ${IPV4NSD}
    Log   ${IPV6NSD}

Get Dual Ip
    [Documentation]   Get the IP address from the NS
    ${Ns_IP}=   Get Dual Ip   ${NS_ID}
    Log   ${Ns_IP}
    ${Ns_IP}=   Split String   ${Ns_IP}
    Set Suite Variable   ${IPV6NS}   ${Ns_IP}[0]
    Set Suite Variable   ${IPV4NS}   ${Ns_IP}[1]
    Log   ${IPV4NS}
    Log   ${IPV6NS}

Verify Dual Ip
    [Documentation]   Test case to validate the IP address.
    Should Be Equal   ${IPV4NS}   ${IPV4NSD}
    Should Be Equal   ${IPV6NS}   ${IPV6NSD}

Terminate VNF
    [Documentation]   Test case to terminate the VNF
    Get Auth Token
    ${json_path}=   Read Directory
    ${UPDATED_JSON_OBJ}=   Load JSON From File   ${json_path}/sol003_02-dualstack_ip_vnfm.json
    ${UPDATED_JSON_OBJ}=   Get Variable Value   ${UPDATED_JSON_OBJ['data'][3]}
    Set Suite Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}/${CREATEID}/terminate   ${UPDATED_JSON_OBJ}
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    ${terminateid}=   Set Variable   ${id[0]}
    Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${terminateid}
    ${LcmopsTerminate}=   Get Value From Json   ${request_response.json()}   $.._id
    Log   ${LcmopsTerminate[0]}
    FOR   ${index}   IN RANGE   0   15
        Log   ${index}
        Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${terminateid}
        ${value}=   Get ID   operationState
        ${status}=   Run Keyword And Return Status   Should Be Equal   ${value}   COMPLETED
        IF   ${status}
            BREAK
        ELSE
            Sleep   10s
        END
    END
    Pass Execution If   ${request_response.status_code} in ${success_status_code_list}   Terminate VNF instance completed

Delete VNF
    [Documentation]   Test case to delete VNF
    Get Auth Token
    Create Session   APISession   ${REST_API_HOST}
    &{Headers}=   Create Dictionary   Content-Type=application/json   Accept=application/json   Authorization=Bearer ${AccessToken}
    ${resp}=   DELETE On Session   APISession   ${VNF_INSTANCE_URI}/${CREATEID}   headers=${Headers}
    Pass Execution If   ${resp.status_code} in ${success_status_code_list}   Delete VNF Instance completed

Delete VNF Descriptor Test
    [Documentation]   Test case to delete the VNF descriptor.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
