*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [HEAL-01] Healing of a multi-volume VDU

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   heal_01   cluster_heal   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_VOLUMES_PKG}   several_volumes_vnf
${VNFD_VOLUMES_NAME}   several_volumes-vnf
${VDU_VOLUMES_NAME}   several_volumes-VM
${VNF_SEVERAL_INDEX}   several_volumes_vnf
${VNFD_MANUALSCALE_PKG}   manual_scale_vnf
${VNFD_MANUALSCALE_NAME}   manual_scale-vnf
${NSD_PKG}   volumes_healing_ns
${NSD_NAME}   volumes_healing-ns

# NS instance name and configuration
${NS_NAME}   heal_01
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   6min

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${SUCCESS_RETURN_CODE}   0

@{VIM_VDUS}   @{EMPTY}
@{VIM_VOLUMES}   @{EMPTY}


*** Test Cases ***
Create VNF Descriptors
    [Documentation]   Upload VNF packages for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_MANUALSCALE_PKG}'
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_VOLUMES_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}

Get NS Id
    [Documentation]   Get NS identifier.
    [Tags]   cleanup
    ${variables}=   Get Variables
    IF   not "\${ns_id}" in "${variables}"
        ${id}=   Get Ns Id   ${NS_NAME}
        Set Suite Variable   ${NS_ID}   ${id}
    END

Get VIM Objects
    [Documentation]   Retrieve all VMs and volumes from the NS and stores them in VIM_VDUS and VIM_VOLUMES lists.
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    @{vnf_id_list}=   Get Ns Vnf List   ${NS_ID}
    Log   ${vnf_id_list}
    FOR   ${vnf_id}   IN   @{vnf_id_list}
        Log   ${vnf_id}
        ${id}=   Get VNF VIM ID   ${vnf_id}
        @{vdu_ids}=   Split String   ${id}
        Append To List   ${VIM_VDUS}   @{vdu_ids}
    END
    FOR   ${vdu_id}   IN   @{VIM_VDUS}
        ${volumes_attached}=   Get Server Property   ${vdu_id}   volumes_attached
        ${match}=   Get Regexp Matches   ${volumes_attached}   '([0-9a-f\-]+)'   1
        IF   ${match} != @{EMPTY}
            IF   not "${match}[0]" in "@{VIM_VOLUMES}"
                Append To List   ${VIM_VOLUMES}   ${match}[0]
            END
        END
    END
    Log Many   @{VIM_VDUS}
    Log Many   @{VIM_VOLUMES}

Get Volume VNF Info
    [Documentation]   Get VDU ID, IP addresses and volumes of the VNF and stores them in suite variables to be used later on.
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_SEVERAL_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_VOLUMES_IP_ADDR}   ${ip_addr}
    ${vnf_id}=   Get Vnf Id   ${NS_ID}   ${VNF_SEVERAL_INDEX}
    Set Suite Variable   ${VNF_VOLUMES_ID}   ${vnf_id}
    ${id}=   Get VNF VIM ID   ${vnf_id}
    Set Suite Variable   ${VDU_VOLUMES_ID}   ${id}
    Log   ${VDU_VOLUMES_ID}
    @{VOLUMES_IP_LIST}=   Get Vnf Vdur IPs   ${VNF_VOLUMES_ID}
    Set Suite Variable   @{VOLUMES_IP_LIST}   @{VOLUMES_IP_LIST}
    Log   @{VOLUMES_IP_LIST}

Get Volumes Info
    [Documentation]   Get number of volumes from the VNF descriptor and get the attached volumes from the VDU instance.
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnfpkg-show ${VNFD_VOLUMES_NAME} --literal | yq '.vdu[0]."virtual-storage-desc" | length'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    ${num_virtual_storage}=   Convert To Integer   ${stdout}
    Set Suite Variable   ${VNF_NUM_VOLUMES}   ${num_virtual_storage}
    Log   ${VNF_NUM_VOLUMES}
    ${volumes_attached}=   Get Server Property   ${VDU_VOLUMES_ID}   volumes_attached
    ${match}=   Get Regexp Matches   ${volumes_attached}   '([0-9a-f\-]+)'   1
    Set Suite Variable   ${VOLUME_ID}   ${match}[0]

Check VDU Disks
    [Documentation]   Check that the number of disks in the VDU meets the expected one.
    Variable Should Exist   ${VNF_VOLUMES_IP_ADDR}   msg=VNF is not available
    Sleep   20 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF_VOLUMES_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   sudo lsblk -l
    Log   ${stdout}
    ${lines}=   Get Lines Containing String   ${stdout}   disk
    ${num_lines}=   Get Line Count   ${lines}
    IF   ${num_lines} < ${VNF_NUM_VOLUMES}   Fail   msg=Number of disks (${num_lines}) is less than specified in VDU (${VNF_NUM_VOLUMES})

Delete Persistent Volume VDU
    [Documentation]   Manually delete the VM in Openstack.
    Variable Should Exist   ${VDU_VOLUMES_ID}   msg=VDU is not available
    Delete Server   ${VDU_VOLUMES_ID}
    Sleep   20

Heal Persistent Volume VDU
    [Documentation]   Manually heal the VNF in order to re-create the deleted VM.
    Variable Should Exist   ${VNF_VOLUMES_ID}   msg=VNF is not available
    Heal Network Service   ${NS_ID}   --vnf ${VNF_VOLUMES_ID} --cause "Heal VM of volumes_vnf" --vdu ${VDU_VOLUMES_NAME}

Check VNF After Healing
    [Documentation]   Check that the IDs of the VM and volumes have not changed after healing.
    Variable Should Exist   ${VNF_VOLUMES_ID}   msg=VNF is not available
    @{ip_list}=   Get Vnf Vdur IPs   ${VNF_VOLUMES_ID}
    Log   @{ip_list}
    Should Be Equal   ${ip_list}   ${VOLUMES_IP_LIST}   IP addresses have changed after healing
    ${id}=   Get VNF VIM ID   ${VNF_VOLUMES_ID}
    Log   ${id}
    Should Not Be Equal   ${id}   ${VDU_VOLUMES_ID}   VDU id has not changed after healing
    ${volumes_attached}=   Get Server Property   ${id}   volumes_attached
    ${match}=   Get Regexp Matches   ${volumes_attached}   '([0-9a-f\-]+)'   1
    Should Be Equal   ${match}[0]   ${VOLUME_ID}   Volume id has changed after healing
    Sleep   30 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF_VOLUMES_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   sudo lsblk -l
    Log   ${stdout}
    ${lines}=   Get Lines Containing String   ${stdout}   disk
    ${num_lines}=   Get Line Count   ${lines}
    IF   ${num_lines} < ${VNF_NUM_VOLUMES}   Fail   msg=Number of disks (${num_lines}) is less than specified in VDU (${VNF_NUM_VOLUMES})

Update VIM Objects
    [Documentation]   Retrieve all VMs and volumes from the NS and stores them in VIM_VDUS and VIM_VOLUMES lists.
    ...               This is done again to guarantee that all objects are cleaned in the VIM in case the heal operation
    ...               added new objects.
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    @{vdu_updated}=   Create List
    @{vnf_id_list}=   Get Ns Vnf List   ${NS_ID}
    FOR   ${vnf_id}   IN   @{vnf_id_list}
        ${id}=   Get VNF VIM ID   ${vnf_id}
        @{vdu_ids}=   Split String   ${id}
        Append To List   ${vdu_updated}   @{vdu_ids}
        FOR   ${id}   IN   @{vdu_ids}
            IF   not "${id}" in "@{VIM_VDUS}"
                Append To List   ${VIM_VDUS}   ${id}
            END
        END
    END
    FOR   ${vdu_id}   IN   @{vdu_updated}
        ${volumes_attached}=   Get Server Property   ${vdu_id}   volumes_attached
        ${match}=   Get Regexp Matches   ${volumes_attached}   '([0-9a-f\-]+)'   1
        IF   ${match} != @{EMPTY}
            IF   not "${match}[0]" in "@{VIM_VOLUMES}"
                Append To List   ${VIM_VOLUMES}   ${match}[0]
            END
        END
    END
    Log Many   @{VIM_VDUS}
    Log Many   @{VIM_VOLUMES}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptors
    [Documentation]   Delete VNF packages from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_VOLUMES_NAME}
    Delete VNFD   ${VNFD_MANUALSCALE_NAME}

Delete Remaining Objects in VIM
    [Documentation]   Delete any remaining objects (volumes, VMs, etc.) in the VIM.
    [Tags]   cleanup
    Delete Objects In VIM


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_VOLUMES_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_MANUALSCALE_NAME}
    Run Keyword If Any Tests Failed   Delete Objects In VIM

Delete Objects In VIM
    [Documentation]   Clean up remaining VMs and volumes directly from the VIM.
    ${error}=   Set Variable   0
    FOR   ${vol_id}   IN   @{VIM_VOLUMES}
        Log   Checking if volume ${vol_id} is still in VIM
        ${exists}=   Check If Volume Exists   ${vol_id}
        IF   ${exists}
            ${error}=   Set Variable   1
            Log   Deleting volume ${vol_id}
            Run Keyword And Ignore Error   Delete Volume   ${vol_id}
        END
    END
    FOR   ${vdu_id}   IN   @{VIM_VDUS}
        Log   Checking if server ${vdu_id} is still in VIM
        ${status}=   Run Keyword And Ignore Error   Get Server Property   ${vdu_id}   id
        Log   ${status}[0]
        IF   '${status}[0]' == 'PASS'
            ${error}=   Set Variable   1
            Log   Deleting server ${vdu_id}
            Run Keyword And Ignore Error   Delete Server   ${vdu_id}
        END
    END
    IF   ${error}==1   Fail   Some objects created by test were not deleted in VIM
