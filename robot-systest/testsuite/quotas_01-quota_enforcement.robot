*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [QUOTAS-01] Quota enforcement.

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/project_lib.resource
Resource   ../lib/user_lib.resource

Test Tags   quotas_01   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# VNFD package to use during test
${VNFD_PKG}   hackfest_basic_vnf

# Project names and quotas to use
${PROJECT_1_NAME}   quotas_01_proj_1
${PROJECT_2_NAME}   quotas_01_proj_2
${PROJECT_3_NAME}   quotas_01_proj_3
${PROJECT_1_QUOTAS}   vnfds=1,nsds=2,ns_instances=3
${PROJECT_2_QUOTAS}   vnfds=1,nsds=1,ns_instances=1
${PROJECT_3_QUOTAS}   vnfds=5,nsds=,ns_instances=

# User name and password for project
${USERNAME}   quotas_01_user
${USER_PASSWORD}   Quotas_01_pass
${USER_NEW_PASSWORD}   Quotas_01_new_pass

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create First Project With Quotas
    [Documentation]   Create a first project with quotas, then get the project quotas and check that they are correct.
    Create Project With Quotas   ${PROJECT_1_NAME}   ${PROJECT_1_QUOTAS}
    ${project_1_vnfds}=   Get Project Quotas   ${PROJECT_1_NAME}   vnfds
    Should Be Equal As Integers   1   ${project_1_vnfds}
    ${project_1_nsds}=   Get Project Quotas   ${PROJECT_1_NAME}   nsds
    Should Be Equal As Integers   2   ${project_1_nsds}
    ${project_1_ns_instances}=   Get Project Quotas   ${PROJECT_1_NAME}   ns_instances
    Should Be Equal As Integers   3   ${project_1_ns_instances}

Create Second Project With Quotas
    [Documentation]   Create a second project with quotas, then get the project quotas and check that they are correct.
    Create Project With Quotas   ${PROJECT_2_NAME}   ${PROJECT_2_QUOTAS}
    ${project_2_vnfds}=   Get Project Quotas   ${PROJECT_2_NAME}   vnfds
    Should Be Equal As Integers   1   ${project_2_vnfds}
    ${project_2_nsds}=   Get Project Quotas   ${PROJECT_2_NAME}   nsds
    Should Be Equal As Integers   1   ${project_2_nsds}
    ${project_2_ns_instances}=   Get Project Quotas   ${PROJECT_2_NAME}   ns_instances
    Should Be Equal As Integers   1   ${project_2_ns_instances}

Create User In Projects
    [Documentation]   Create a user with role project_admin in both projects.
    ${rc}   ${stdout}=   Run And Return RC And Output   osm user-create ${USERNAME} --password ${USER_PASSWORD} --project-role-mappings ${PROJECT_1_NAME},project_admin --project-role-mappings ${PROJECT_2_NAME},project_admin
    Log   ${rc},${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Check If User Is Assigned To Project   ${USERNAME}   ${PROJECT_1_NAME}
    Check If User Is Assigned To Project   ${USERNAME}   ${PROJECT_2_NAME}
    Update User Password   ${USERNAME}   ${USER_PASSWORD}   ${USER_NEW_PASSWORD}

Change First Project Name to Third Project Name
    [Documentation]   Rename first project. From now on, the project will be considered for the tests a third project.
    Update Project Name   ${PROJECT_1_NAME}   ${PROJECT_3_NAME}
    ${project_3_vnfds}=   Get Project Quotas   ${PROJECT_3_NAME}   vnfds
    Should Be Equal As Integers   1   ${project_3_vnfds}
    ${project_3_nsds}=   Get Project Quotas   ${PROJECT_3_NAME}   nsds
    Should Be Equal As Integers   2   ${project_3_nsds}
    ${project_3_ns_instances}=   Get Project Quotas   ${PROJECT_3_NAME}   ns_instances
    Should Be Equal As Integers   3   ${project_3_ns_instances}
    Check If User Is Assigned To Project   ${USERNAME}   ${PROJECT_3_NAME}

Create VNFDs On Third Project Until Exceed Quota
    [Documentation]   Upload VNF packages in the third project.
    Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v1;product-name=v1'
    Run Keyword And Expect Error   *   Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v2;product-name=v2'

Increase Third Project Quotas
    [Documentation]   Update the quotas of the third project.
    Update Project Quotas   ${PROJECT_3_NAME}   ${PROJECT_3_QUOTAS}
    ${project_3_vnfds}=   Get Project Quotas   ${PROJECT_3_NAME}   vnfds
    Should Be Equal As Integers   5   ${project_3_vnfds}

Create More VNFDs On Third Project Until Exceed Quota
    [Documentation]   Upload VNF packages in the third project until the quota is reached, so that an error is obtained.
    Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v2;product-name=v2'
    Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v3;product-name=v3'
    Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v4;product-name=v4'
    Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v5;product-name=v5'
    Run Keyword And Expect Error   *   Create VNFD In Project   ${PROJECT_3_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=v6;product-name=v6'

Create VNFDs On Second Project Until Exceed Quota
    [Documentation]   Upload VNF packages in the second project until the quota is reached, so that an error is obtained.
    Create VNFD In Project   ${PROJECT_2_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=vp2_1;product-name=vp2_1'
    Run Keyword And Expect Error   *   Create VNFD In Project   ${PROJECT_2_NAME}   '%{PACKAGES_FOLDER}/${VNFD_PKG}'   ${USERNAME}   ${USER_NEW_PASSWORD}   override='id=vp2_2;product-name=vp2_2'

Check Project Scopes
    [Documentation]   Check that the user can see a VNF package in both projects.
    ${rc}   ${stdout}=   Run And Return RC And Output   osm --project ${PROJECT_3_NAME} --password ${USER_NEW_PASSWORD} --user ${USERNAME} vnfpkg-show vp2_1
    Log   ${stdout}
    Should Not Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm --project ${PROJECT_2_NAME} --password ${USER_NEW_PASSWORD} --user ${USERNAME} vnfpkg-show v1
    Log   ${stdout}
    Should Not Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}

Delete Second Project VNFD
    [Documentation]   Delete VNF packages from second project.
    [Tags]   cleanup
    Delete VNFD In Project   ${PROJECT_2_NAME}   vp2_1   ${USERNAME}   ${USER_NEW_PASSWORD}

Delete Third Project VNFDs
    [Documentation]   Delete VNF packages from third project.
    [Tags]   cleanup
    Delete VNFD In Project   ${PROJECT_3_NAME}   v1   ${USERNAME}   ${USER_NEW_PASSWORD}
    Delete VNFD In Project   ${PROJECT_3_NAME}   v2   ${USERNAME}   ${USER_NEW_PASSWORD}
    Delete VNFD In Project   ${PROJECT_3_NAME}   v3   ${USERNAME}   ${USER_NEW_PASSWORD}
    Delete VNFD In Project   ${PROJECT_3_NAME}   v4   ${USERNAME}   ${USER_NEW_PASSWORD}
    Delete VNFD In Project   ${PROJECT_3_NAME}   v5   ${USERNAME}   ${USER_NEW_PASSWORD}

Delete Second Project After Removing User From It
    [Documentation]   Delete user from second project, then delete the second project.
    [Tags]   cleanup
    Run Keyword And Expect Error   *   Delete Project   ${PROJECT_2_NAME}
    Remove User From Project   ${USERNAME}   ${PROJECT_2_NAME}
    Delete Project   ${PROJECT_2_NAME}

Delete Projects User
    [Documentation]   Delete user.
    [Tags]   cleanup
    ${rc}=   Run And Return RC   osm user-delete ${USERNAME}
    Log   ${rc}

Delete Third Project
    [Documentation]   Delete third project.
    [Tags]   cleanup
    Delete Project   ${PROJECT_3_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting VNF packages, projects and users.
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_2_NAME}   vp2_1   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_3_NAME}   v1   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_3_NAME}   v2   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_3_NAME}   v3   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_3_NAME}   v4   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete VNFD In Project   ${PROJECT_3_NAME}   v5   ${USERNAME}   ${USER_NEW_PASSWORD}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete Project   ${PROJECT_1_NAME}
    Run Keyword If Any Tests Failed   Delete Project   ${PROJECT_2_NAME}
    Run Keyword If Any Tests Failed   Run Keyword And Ignore Error   Delete Project   ${PROJECT_3_NAME}
    Run And Return RC   osm user-delete ${USERNAME}
