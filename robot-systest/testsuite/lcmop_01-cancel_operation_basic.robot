*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [LCMOP_01] Cancel an ongoing NS operation

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   lcmop_01   cluster_lcmop   daily

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   simple_ee_vnf
${VNFD_NAME}   simple_ee-vnf
${NSD_PKG}   simple_ee_ns
${NSD_NAME}   simple_ee-ns

# NS instance name and configuration
${NS_NAME}   lcmop_01_cancel_operation_basic
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Create the VNF descriptor
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Create the NS descriptor
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate the Network service
    ${id}=   Instantiate Network Service   ${NS_NAME}   ${NSD_NAME}   %{VIM_TARGET}   --config '${NS_CONFIG}'
    Set Suite Variable   ${NS_ID}   ${id}

Get Operation ID
    [Documentation]   Obtain the operation ID of the NS instantiation
    ${id}=   Get Operations By Type   ${NS_ID}   instantiate
    Set Suite Variable   ${OP_ID}   ${id}

Cancel Operation
    [Documentation]   Cancel the ongoing NS instantiation
    Sleep   5s   Waiting before cancelling the operation
    Cancel Operation By Id   ${OP_ID}

Check That Operation Is Cancelled
    [Documentation]   Check that the operation is succesfully cancelled
    Check For NS Operation Cancelled   ${OP_ID}

Delete NS Instance Test
    [Documentation]   Delete the cancelled NS instance
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete the NS descriptor
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete the VNF descriptor
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
