*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-32] Basic NS with a volume in different availability zone

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vim_lib.resource
Resource   ../lib/prometheus_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource

Variables   ../resources/basic_32-volume_with_different_az.py

Test Tags   basic_32

Suite Setup   Run Keyword And Ignore Error   Suite Preparation


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   several_volumes_vnf
${VNFD_NAME}   several_volumes-vnf
${NSD_PKG}   several_volumes_ns
${NSD_NAME}   several_volumes-ns

# NS instance name and configuration
${NS_NAME}   basic_32
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub


*** Test Cases ***
Create VIM Target Basic
    [Documentation]   Create a VIM Target only with the az parameters.
    ...               Checks the status of the VIM in Prometheus after it creation.

    Pass Execution If   '${VIM_ACCOUNT_TYPE}' != 'openstack'   Not applicable for ${VIM_ACCOUNT_TYPE} VIM
    ${rand}=   Generate Random String   6   [NUMBERS]
    ${VIM_NAME_AZ}=   Catenate   SEPARATOR=_   ${VIM_NAME_PREFIX}   ${rand}
    Set Suite Variable   ${VIM_NAME_AZ}
    ${CREATED_VIM_ACCOUNT_ID}=   Create VIM Target   ${VIM_NAME_AZ}   ${VIM_USER}   ${VIM_PASSWORD}   ${VIM_AUTH_URL}   ${VIM_TENANT}   ${VIM_ACCOUNT_TYPE}   config=${VIM_CONFIG}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}

Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   ${VIM_NAME_AZ}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup

    Delete VNFD   ${VNFD_NAME}

Delete VIM Target By ID
    [Documentation]   Delete the VIM Target created in previous test-case by its ID.
    ...               Checks whether the VIM Target was created or not before perform the deletion.
    [Tags]   cleanup

    ${vim_account_id}=   Get VIM Target ID   ${VIM_NAME_AZ}
    Should Be Equal As Strings   ${vim_account_id}   ${CREATED_VIM_ACCOUNT_ID}
    Delete VIM Target   ${vim_account_id}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables

Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
