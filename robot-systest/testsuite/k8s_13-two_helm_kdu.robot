*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-13] Two Helm-based KDU stored in public and private OCI repositories.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource
Resource   ../lib/repo_lib.resource

Test Tags   k8s_13   cluster_k8s   daily   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   two_helm_oci_knf
${VNFD_NAME}   two_helm_oci_knf
${NSD_PKG}   two_helm_oci_ns
${NSD_NAME}   two_helm_oci_ns

# NS instance name and configuration
${NS_NAME}   ldap
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}}], additionalParamsForVnf: [ {member-vnf-index: two_helm_oci, additionalParamsForKdu: [ {kdu_name: haproxy, additionalParams: {service: {type: LoadBalancer }, adminPassword: admin}}, {kdu_name: ldap, additionalParams: {service: {type: LoadBalancer }, adminPassword: admin}} ] } ] }

${NS_ID}   ${EMPTY}
${PUBLICKEY}   ${EMPTY}
${VNF_MEMBER_INDEX}   two_helm_oci
${KDU1_NAME}   haproxy
${KDU2_NAME}   ldap
${UPGRADE_ACTION}   upgrade
${ROLLBACK_ACTION}   rollback
${REPLICA_COUNT}   3

# OCI helm repo configuration
${REPO_NAME}   osm-gitlab
${REPO_URI}   %{OCI_REGISTRY_URL}
${REPO_USER}   %{OCI_REGISTRY_USER}
${REPO_PASSWORD}   %{OCI_REGISTRY_PASSWORD}


*** Test Cases ***
Create Package For CNF
    [Documentation]   Create Package For CNF
    [Tags]   prepare
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Package For NS
    [Documentation]   Create Package For NS
    [Tags]   prepare
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Create Helm OCI Repo
    [Documentation]   Create Helm OCI Repo for openldap kdu
    [Tags]   prepare
    Create Repo   ${REPO_NAME}   ${REPO_URI}   helm-chart   ${REPO_USER}   ${REPO_PASSWORD}   --oci

Create Network Service Instance
    [Documentation]   Create Network Service Instance
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Log   ${id}

Get Ns Id
    [Documentation]   Get ID of NS instance
    [Tags]   verify
    ${id}=   Get Ns Id   ${NS_NAME}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Get ID of VNF
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Execute Upgrade Operation over first KDU
    [Documentation]   Execute Upgrade Operation over first KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU1_NAME}   replicaCount=${REPLICA_COUNT}
    Log   ${ns_op_id}

Check Replicas After Upgrade Operation over first KDU
    [Documentation]   Check Replicas After Upgrade Operation over first KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU1_NAME}
    Log   ${count}
    Should Be Equal As Integers   ${count}   ${REPLICA_COUNT}

Execute Rollback Operation over first KDU
    [Documentation]   Execute Rollback Operation over first KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${ROLLBACK_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU1_NAME}
    Log   ${ns_op_id}

Check Replicas After Rollback Operation over first KDU
    [Documentation]   Check Replicas After Rollback Operation over first KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU1_NAME}
    Log   ${count}
    Should Be Empty   ${count}

Execute Upgrade Operation over second KDU
    [Documentation]   Execute Upgrade Operation over second KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU2_NAME}   replicaCount=${REPLICA_COUNT}
    Log   ${ns_op_id}

Check Replicas After Upgrade Operation over second KDU
    [Documentation]   Check Replicas After Upgrade Operation over second KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU2_NAME}
    Log   ${count}
    Should Be Equal As Integers   ${count}   ${REPLICA_COUNT}

Execute Rollback Operation over second KDU
    [Documentation]   Execute Rollback Operation over second KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${ROLLBACK_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU2_NAME}
    Log   ${ns_op_id}

Check Replicas After Rollback Operation over second KDU
    [Documentation]   Check Replicas After Rollback Operation over second KDU
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU2_NAME}
    Log   ${count}
    Should Be Empty   ${count}

Delete Network Service Instance
    [Documentation]   Delete Network Service Instance
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete Helm OCI Repo
    [Documentation]   Delete Helm OCI Repo
    [Tags]   prepare
    Delete Repo   ${REPO_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS Descriptor Test
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF Descriptor Test
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
