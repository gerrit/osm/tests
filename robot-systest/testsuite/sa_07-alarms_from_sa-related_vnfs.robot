*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [SA-07] Events or alarms coming from SA-related VNFs in the NS.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/prometheus_lib.resource

Test Tags   sa_07   cluster_sa   daily   regression

Suite Setup   Run Keyword And Ignore Error   Suite Preparation
Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS instantiation parameters
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   cirros_alarm_vnf
${VNFD_NAME}   cirros_alarm-vnf
${VNFD_FILE}   cirros_alarm_vnfd.yaml
${NEW_VNFD_PKG}   new_cirros_alarm_vnf
${NSD_PKG}   cirros_alarm_ns
${NSD_NAME}   cirros_alarm-ns

# NS instance name and id
${NS_ID}   ${EMPTY}
${NS_NAME}   sa_07-alarm_test

# Webhook NS and VNF descriptor package folder and ids
${WS_VNFD_PKG}   hackfest_basic_vnf
${WS_NSD_PKG}   hackfest_basic_ns
${WS_VNFD_NAME}   hackfest_basic-vnf
${WS_NSD_NAME}   hackfest_basic-ns

# Webhook NS instance name and id
${WS_NS_ID}   ${EMPTY}
${WS_NS_NAME}   sa_07-webhook_service_test

# Webhook NS Variables
${WS_VNF_MEMBER_INDEX}   vnf
${WS_VNF_IP_ADDR}   ${EMPTY}
${WS_LOG_FILE}   webhook.log
${WS_PORT}   5212
${WS_ALARM_MSG}   notify_alarm

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

# Prometheus polling interval and retries
${PROMETHEUS_POLL_RETRIES}   15 times
${PROMETHEUS_POLL_TIMEOUT}   1 minute

# Prometheus metrics to retrieve
${METRIC_NAME}   osm_cpu_utilization

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create Webhook Service VNF Descriptor
    [Documentation]   Upload VNF package with a webhook service for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${WS_VNFD_PKG}'

Create Webhook Service NS Descriptor
    [Documentation]   Upload NS package with a webhook service for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${WS_NSD_PKG}'

Instantiate Webhook Service Network Service
    [Documentation]   Instantiate NS with the webhook for the testsuite.
    ${id}=   Create Network Service   ${WS_NSD_NAME}   %{VIM_TARGET}   ${WS_NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${WS_NS_ID}   ${id}

Get Webhook Service VNF IP Address
    [Documentation]   Get the mgmt IP address of the VNF offering the webhook service.
    ${ip_addr}=   Get Vnf Management Ip Address   ${WS_NS_ID}   ${WS_VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${WS_VNF_IP_ADDR}   ${ip_addr}

Start Webhook Service
    [Documentation]   Launch simple command in webhook VNF to listen to notifications.
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    Sleep   40 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${WS_VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   while true; do { echo -e 'HTTP/1.0 200 OK\r\nContent-Length: 0\r\n\r\n'; test; } | nc -l '${WS_PORT}'; done > '${WS_LOG_FILE}' 2>&1 &
    Log   ${stdout}

Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    ${rc}   ${stdout}=   Run And Return RC And Output   mkdir '%{PACKAGES_FOLDER}/${NEW_VNFD_PKG}' && WEBHOOK_URL="http://${WS_VNF_IP_ADDR}:${WS_PORT}" envsubst < '%{PACKAGES_FOLDER}/${VNFD_PKG}'/'${VNFD_FILE}' > '%{PACKAGES_FOLDER}/${NEW_VNFD_PKG}'/'${VNFD_FILE}'
    Log   ${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    Create VNFD   '%{PACKAGES_FOLDER}/${NEW_VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate the main NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Alarm Metric
    [Documentation]   Check that the expected metric is present in Prometheus (pushed from SA modules).
    Variable Should Exist   ${PROMETHEUS_POLL_RETRIES}   msg=Metric polling retries is not available
    Variable Should Exist   ${PROMETHEUS_POLL_TIMEOUT}   msg=Metric polling timeout is not available
    Variable Should Exist   ${PROMETHEUS_URL}   msg=Prometheus URL is not available
    Variable Should Exist   ${METRIC_NAME}   msg=Prometheus metric name is not available
    ${metric_filter}=   Set Variable   ns_id=${NS_ID}
    ${metric_value}=   Wait Until Keyword Succeeds   ${PROMETHEUS_POLL_RETRIES}   ${PROMETHEUS_POLL_TIMEOUT}   Get Metric   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}   ${METRIC_NAME}   ${metric_filter}
    IF   ${metric_value} <= 0   Fail   msg=The metric '${METRIC_NAME}' value is '${metric_value}'

Check Alarms Were Received
    [Documentation]   Check in the webhook VNF that the notification is received.
    Wait Until Keyword Succeeds   6 times   40 seconds   Execute Remote Command Check Rc Return Output   ${WS_VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   cat '${WS_LOG_FILE}' | grep '${WS_ALARM_MSG}' | grep '${NS_NAME}'

Delete NS Instance
    [Documentation]   Delete main NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete main NS package.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete main VNF package.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}

Delete Webhook Service NS Instance
    [Documentation]   Delete NS instance for the webhook.
    [Tags]   cleanup
    Delete NS   ${WS_NS_NAME}

Delete Webhook Service NS Descriptor
    [Documentation]   Delete NS package for the webhook.
    [Tags]   cleanup
    Delete NSD   ${WS_NSD_NAME}

Delete Webhook Service VNF Descriptor
    [Documentation]   Delete VNF package for the webhook.
    [Tags]   cleanup
    Delete VNFD   ${WS_VNFD_NAME}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables

Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Delete NS   ${WS_NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${WS_NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${WS_VNFD_NAME}
    Delete Temporary Descriptor Folder   '%{PACKAGES_FOLDER}/${NEW_VNFD_PKG}'

Delete Temporary Descriptor Folder
    [Documentation]   Removes the temporary package folder created for the test
    [Arguments]   ${folder_name}
    ${rc}   ${stdout}=   Run And Return RC And Output   rm -rf '${folder_name}'
    Log   ${rc},${stdout}
