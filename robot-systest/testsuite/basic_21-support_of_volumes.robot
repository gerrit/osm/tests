*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-21] NS with only one VDU and several volumes

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   basic_21   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   several_volumes_vnf
${VNFD_NAME}   several_volumes-vnf
${NSD_PKG}   several_volumes_ns
${NSD_NAME}   several_volumes-ns

# NS instance name and configuration
${NS_NAME}   basic_21
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${VNF_MEMBER_INDEX}   vnf-several-volumes
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    [Tags]   prepare
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    [Tags]   prepare
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get NS Id
    [Documentation]   Get NS identifier and stores as suite variable to be used later on.
    [Tags]   verify   cleanup
    ${variables}=   Get Variables
    IF   not "\${ns_id}" in "${variables}"
        ${id}=   Get Ns Id   ${NS_NAME}
        Set Suite Variable   ${NS_ID}   ${id}
    END

Get Volumes From VNFD
    [Documentation]   Get from VNF descriptor the expected number of volumes in the VNF.
    [Tags]   verify
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnfpkg-show ${VNFD_NAME} --literal | yq '.vdu[0]."virtual-storage-desc" | length'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    ${num_virtual_storage}=   Convert To Integer   ${stdout}
    Set Suite Variable   ${VNF_NUM_VOLUMES}   ${num_virtual_storage}
    Log   ${VNF_NUM_VOLUMES}

Get VNF IP Address
    [Documentation]   Get the mgmt IP address of the VNF.
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=NS is not available
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

# Get Persistent Volume Id
#     [Documentation]   Get VIM volume identifier of the persistent volume of the VNF.
#     [Tags]   cleanup
#     Variable Should Exist   ${ns_id}   msg=NS is not available
#     ${vnfs_list}=   Get Ns Vnf List   ${ns_id}
#     ${vim_id}=   Get VNF VIM ID   ${vnfs_list}[0]
#     ${volumes_attached}=   Get Server Property   ${vim_id}   volumes_attached
#     ${match}=   Get Regexp Matches   ${volumes_attached}   '([0-9a-f\-]+)'   1
#     Set Suite Variable   ${PERSISTENT_VOLUME_ID}   ${match}[0]

Check VDU disks
    [Documentation]   Check that the number of volumes is the expected one.
    [Tags]   verify
    Variable Should Exist   ${VNF_IP_ADDR}   msg=VNF is not available
    Sleep   30 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   sudo lsblk -l
    Log   ${stdout}
    ${lines}=   Get Lines Containing String   ${stdout}   disk
    ${num_lines}=   Get Line Count   ${lines}
    IF   ${num_lines} < ${VNF_NUM_VOLUMES}   Fail   msg=Number of disks (${num_lines}) is less than specified in VDU (${VNF_NUM_VOLUMES})

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

# Check Persistent Volume Was Deleted
#     [Documentation]   Check that the persistent volume was deleted.
#     [Tags]   cleanup
#     Variable Should Exist   ${PERSISTENT_VOLUME_ID}   msg=Volume is not available
#     ${exists}=   Check If Volume Exists   ${PERSISTENT_VOLUME_ID}
#     Log   ${exists}
#     IF   ${exists}==0
#         Fail   msg=Persistent volume was deleted
#         Set Suite Variable   ${PERSISTENT_VOLUME_ID}   ${EMPTY}}
#     ELSE
#         Log   Persistent volume still exists, deleting...
#         Delete Volume   ${PERSISTENT_VOLUME_ID}
#         Set Suite Variable   ${PERSISTENT_VOLUME_ID}   ${EMPTY}}
#     END

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
#    Run Keyword If Any Tests Failed   Check Volume Was Deleted
