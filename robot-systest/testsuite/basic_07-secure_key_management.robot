*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-07] Secure key management.

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_07   cluster_ee_config   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   charm-packages/nopasswd_proxy_charm_vnf
${VNFD_NAME}   nopasswd_proxy_charm-vnf
${NSD_PKG}   charm-packages/nopasswd_proxy_charm_ns
${NSD_NAME}   nopasswd_proxy_charm-ns

# NS instance name and configuration
${NS_NAME}   basic_07_secure_key_management
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   15min

# SSH username and passwod
${USERNAME}   ubuntu
${PASSWORD}   osm4u

${ACTION_NAME}   touch
${VNF_MEMBER_INDEX}   vnf1
${DAY_1_FILE_NAME}   /home/ubuntu/first-touch
${DAY_2_FILE_NAME}   /home/ubuntu/mytouch1


*** Test Cases ***
Create Nopasswd Charm VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    [Tags]   prepare
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Nopasswd Charm NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    [Tags]   prepare
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Nopasswd Charm Network Service
    [Documentation]   Instantiate NS for the testsuite.
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${EMPTY}   ${NS_TIMEOUT}
    Log   ${id}

Get Ns Id
    [Documentation]   Get NS instance id.
    [Tags]   verify
    ${id}=   Get Ns Id   ${NS_NAME}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Addresses
    [Documentation]   Get the mgmt IP address of the VNF of the NS.
    [Tags]   verify
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test SSH Access
    [Documentation]   Check that the VNF is accessible via SSH in its mgmt IP address.
    [Tags]   verify
    Variable Should Exist   ${VNF_IP_ADDR}   msg=IP address of the management VNF is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${EMPTY}

Check Remote Files Created Via Day 1 Operations
    [Documentation]   The Nopasswd VNF has a Day 1 operation that creates a file named ${day_1_file_name} and performs it without password.
    ...               This test checks whether that files have been created or not.
    [Tags]   verify
    Check If Remote File Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${EMPTY}   ${DAY_1_FILE_NAME}

Execute Day 2 Operations
    [Documentation]   Performs one Day 2 operation that creates a new file, this action is executed without password too.
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX}   filename=${DAY_2_FILE_NAME}
    Log   ${ns_op_id}

Check Remote Files Created Via Day 2 Operations
    [Documentation]   Check whether the file created in the previous test via Day 2 operation exists or not.
    [Tags]   verify
    Check If Remote File Exists   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${EMPTY}   ${DAY_2_FILE_NAME}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
