*** Comments ***
#   Copyright 2020 Atos
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [SLICE-01] Network Slicing.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary
Library   yaml

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/nst_lib.resource
Resource   ../lib/nsi_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   slice_01   cluster_slices   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package files
${VNFD1_PKG}   slice_basic_vnf
${VNFD2_PKG}   slice_basic_middle_vnf
${NSD1_PKG}   slice_basic_ns
${NSD2_PKG}   slice_basic_middle_ns
${NST}   slice_basic_nst/slice_basic_nst.yaml

# Descriptor names
${NST_NAME}   slice_basic_nst
${VNFD1_NAME}   slice_basic_vnf
${VNFD2_NAME}   slice_basic_middle_vnf
${NSD1_NAME}   slice_basic_ns
${NSD2_NAME}   slice_basic_middle_ns

# Instance names
${SLICE_NAME}   slicebasic
${MIDDLE_NS_NAME}   slicebasic.slice_basic_nsd_2

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${VNF_MEMBER_INDEX}   middle
${VNF_IP_ADDR}   ${EMPTY}
${MGMT_VNF_IP}   ${EMPTY}
${NST_CONFIG}   {netslice-vld: [ {name: slice_vld_mgmt, vim-network-name: %{VIM_MGMT_NET}} ] }


*** Test Cases ***
Create Slice VNF Descriptors
    [Documentation]   Onboards all the VNFDs required for the test: vnfd1_pkg and vnfd2_pkg (in the variables file)
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD1_PKG}'
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD2_PKG}'

Create Slice NS Descriptors
    [Documentation]   Onboards all the NSDs required for the test: nsd1_pkg and nsd2_pkg (in the variables file)
    Create NSD   '%{PACKAGES_FOLDER}/${NSD1_PKG}'
    Create NSD   '%{PACKAGES_FOLDER}/${NSD2_PKG}'

Create Slice Template
    [Documentation]   Onboards the Network Slice Template: nst (in the variables file)
    Create NST   '%{PACKAGES_FOLDER}/${NST}'

Network Slice Instance Test
    [Documentation]   Instantiates the NST recently onboarded and sets the instantiation id as a suite variable (nsi_id)
    ${id}=   Create Network Slice   ${NST_NAME}   %{VIM_TARGET}   ${SLICE_NAME}   ${NST_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NSI_ID}   ${id}

Get Middle Vnf Management Ip
    [Documentation]   Obtains the management IP of the slice middle VNF (name in the reources file) and sets the ip as a suite variable (mgmt_vnf_ip)
    ${middle_ns_id}=   Run And Return RC And Output   osm ns-list | grep ${MIDDLE_NS_NAME} | awk '{print $4}' 2>&1
    ${vnf_ip}=   Get Vnf Management Ip Address   ${middle_ns_id}[1]   ${VNF_MEMBER_INDEX}
    IF   '${vnf_ip}' == '${EMPTY}'   Fatal Error   Variable \$\{ vnf_ip\} Empty
    Set Suite Variable   ${MGMT_VNF_IP}   ${vnf_ip}

Get Slice Vnf Ip Addresses
    [Documentation]   Obtains the list of IPs addresses in the slice and sets the list as a suite variable (slice_vnfs_ips)
    # Get all the ns_id in the slice except the middle one to avoid self ping
    @{slice_ns_list}=   Get Slice Ns List Except One   ${SLICE_NAME}   ${MIDDLE_NS_NAME}
    Log Many   @{slice_ns_list}
    @{temp_list}=   Create List
    # For each ns_id in the list, get all the vnf_id and their IP addresses
    FOR   ${ns_id}   IN   @{slice_ns_list}
        Log   ${ns_id}
        @{vnf_id_list}=   Get Ns Vnf List   ${ns_id}
        # For each vnf_id in the list, get all its IP addresses
        @{ns_ip_list}=   Get Ns Ip List   @{vnf_id_list}
        @{temp_list}=   Combine Lists   ${temp_list}   ${ns_ip_list}
    END
    Log List   ${temp_list}
    Set Suite Variable   ${SLICE_VNFS_IPS}   ${temp_list}

Test Middle Ns Ping
    [Documentation]   Pings the slice middle vnf (mgmt_vnf_ip)
    Sleep   60s   Waiting for the network to be up
    # Ping to the middle VNF
    Log   ${MGMT_VNF_IP}
    Test Connectivity   ${MGMT_VNF_IP}

Test Middle Vnf SSH Access
    [Documentation]   SSH access to the slice middle vnf (mgmt_vnf_ip) with the credentials provided in the variables file
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${MGMT_VNF_IP}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Test Slice Connectivity
    [Documentation]   SSH access to the slice middle vnf (mgmt_vnf_ip) with the credentials provided in the variables file
    ...               and pings all the IP addresses in the list (slice_vnfs_ips)
    Ping Many   ${MGMT_VNF_IP}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   @{SLICE_VNFS_IPS}

Delete Slice Instance
    [Documentation]   Stops the slice instance (slice_name)
    [Tags]   cleanup
    Delete NSI   ${SLICE_NAME}

Delete Slice Template
    [Documentation]   Deletes the NST (nst_name) from OSM
    [Tags]   cleanup
    Delete NST   ${NST_NAME}

Delete NS Descriptors
    [Documentation]   Deletes all the NSDs created for the test: nsd1_name, nsd2_name
    [Tags]   cleanup
    Delete NSD   ${NSD1_NAME}
    Delete NSD   ${NSD2_NAME}

Delete VNF Descriptors
    [Documentation]   Deletes all the VNFDs created for the test: vnfd1_name, vnfd2_name
    [Tags]   cleanup
    Delete VNFD   ${VNFD1_NAME}
    Delete VNFD   ${VNFD2_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptors, instance and template
    Run Keyword If Any Tests Failed   Delete NSI   ${SLICE_NAME}
    Run Keyword If Any Tests Failed   Delete NST   ${NST_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD1_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD2_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD1_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD2_NAME}
