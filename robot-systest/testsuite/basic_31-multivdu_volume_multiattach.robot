*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-31] Basic NS with two multi-VDU VNF

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_31   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   multivdu_multiattach_vnf
${VNFD_NAME}   multivdu_multiattach-vnf
${NSD_PKG}   multivdu_multiattach_ns
${NSD_NAME}   multivdu_multiattach-ns

# NS instance name and configuration
${NS_NAME}   multivdu_multiattach
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${VDU0_USERNAME}   ubuntu
${VDU0_PASSWORD}   ${EMPTY}
${VDU0_PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${VDU0_PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${VDU1_USERNAME}   ubuntu
${VDU1_PASSWORD}   osmpass
${VDU1_PUBLICKEY}   ${EMPTY}
${VDU1_PRIVATEKEY}   ${EMPTY}

${NS_ID}   ${EMPTY}
${VNF_MEMBER_INDEX}   vnf
${VNF_IP_ADDR}   ${EMPTY}
${WAIT_GUARD_FOR_VM_BOOT}   50s


*** Test Cases ***
Create Multiattach VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Multiattach NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${status}   ${message}=   Run Keyword And Ignore Error   Variable Should Exist   ${VDU0_PUBLICKEY}
    Log   ${status}
    Log   ${message}
    IF   "${status}" == "FAIL"   Set Global Variable   ${VDU0_PUBLICKEY}   ${EMPTY}
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${VDU0_PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}
    Sleep   ${WAIT_GUARD_FOR_VM_BOOT}   Waiting for VM's daemons to be up and running

Get Vnf ID
    [Documentation]   Get the VNF instance id from OSM.
    ${vnf_id}=   Get Vnf Id   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Set Suite Variable   ${VNF_ID}   ${vnf_id}

Get Vnf Management Ip Addresses
    [Documentation]   Get the IP addresses of the two VDU of the VNF.
    ${rc}   ${vdu0_ip}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq -r '.vdur[0].interfaces[]."ip-address"'
    Log   ${rc},${vdu0_ip}
    ${rc}   ${vdu1_ip}=   Run And Return RC And Output   osm vnf-show ${VNF_ID} --literal | yq -r '.vdur[1].interfaces[]."ip-address"'
    Log   ${rc},${vdu1_ip}
    Set Suite Variable   ${MGMT_IP_ADDR_0}   ${vdu0_ip}
    Set Suite Variable   ${MGMT_IP_ADDR_1}   ${vdu1_ip}

Ensure that VDUs can share data
    [Documentation]   Connect via SSH to the two VDUs of the VNF and check that the volume is shared as expected.
    Execute Remote Command Check Rc Return Output   ${MGMT_IP_ADDR_0}   ${VDU0_USERNAME}   ${VDU0_PASSWORD}   ${VDU0_PRIVATEKEY}   sudo mkfs.ext3 -F /dev/vdb && sudo mkdir /shared && sudo mount /dev/vdb /shared && sudo chown ubuntu:ubuntu /shared && echo "osmtest" > /shared/test_file
    Sleep   10s
    Execute Remote Command Check Rc Return Output   ${MGMT_IP_ADDR_1}   ${VDU1_USERNAME}   ${VDU1_PASSWORD}   ${VDU1_PRIVATEKEY}   sudo mkdir /shared && sudo mount /dev/vdb /shared && ls /shared
    Execute Remote Command Check Rc Return Output   ${MGMT_IP_ADDR_1}   ${VDU1_USERNAME}   ${VDU1_PASSWORD}   ${VDU1_PRIVATEKEY}   cat /shared/test_file
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${MGMT_IP_ADDR_0}   ${VDU0_USERNAME}   ${VDU0_PASSWORD}   ${VDU0_PRIVATEKEY}   cat /shared/test_file
    Log   ${stdout}
    Should Be Equal   ${stdout}   osmtest   Shared data can't be read

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
