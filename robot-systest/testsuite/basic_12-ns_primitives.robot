*** Comments ***
#   Copyright 2020 Canonical Ltd.
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-12] NS Primitives

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/package_lib.resource
Resource   ../lib/juju_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_12   cluster_ee_config   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG1}   nscharm_policy_vnf
${VNFD_PKG2}   nscharm_user_vnf
${VNFD_NAME1}   nscharm-policy-vnf
${VNFD_NAME2}   nscharm-user-vnf
${NSD_PKG}   nscharm_ns
${NEW_NSD_PKG}   new_nscharm_ns
${NSD_NAME}   nscharm-ns
${NSD_FILE}   nscharm_nsd.yaml

# NS instance name and configuration
${NS_NAME}   test_nscharm
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   15min

${OLD_JUJU_PASSWORD}   a5611fc6452349cc6e45705d34c501d4
${PUBLICKEY}   ${EMPTY}
${SUCCESS_RETURN_CODE}   0

# VDU profile id, execution environment name to check vdu level charm naming structure
${VDU_PROFILE_ID}   PolicyVM
${EE_NAME}   vnf-policy

# # Username and SSH private key for accessing OSM host
${PRIVATEKEY}   %{OSM_RSA_FILE}
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

# Charm name to check ns level charm naming structure
${CHARM_NAME}   ns


*** Test Cases ***
Change Juju Password
    [Documentation]   NS package needs to be updated with the Juju credentials for your OSM installation
    ${rc}   ${stdout}=   Run And Return RC And Output   cp -r '%{PACKAGES_FOLDER}/${NSD_PKG}' '%{PACKAGES_FOLDER}/${NEW_NSD_PKG}'
    Log   ${rc},${stdout}
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    ${nsd_yaml}=   OperatingSystem.Get File   %{PACKAGES_FOLDER}/${NEW_NSD_PKG}/${NSD_FILE}
    ${changed_nsd_yaml}=   Replace String   ${nsd_yaml}   ${OLD_JUJU_PASSWORD}   %{JUJU_PASSWORD}
    Create File   %{PACKAGES_FOLDER}/${NEW_NSD_PKG}/${NSD_FILE}   ${changed_nsd_yaml}

Upload Vnfds
    [Documentation]   Upload VNF packages for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG1}'
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG2}'

Upload Nsd
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   %{PACKAGES_FOLDER}/${NEW_NSD_PKG}

Instantiate NS
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ns_launch_max_wait_time=${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}
    Set Suite Variable   ${MODEL_NAME}   ${id}

Check NS Charm Application Name
    [Documentation]   Check that the NS charm has the right length.
    ${ns_charm_app_name}=   Get Application Name NS Level Charm   %{OSM_HOSTNAME}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${MODEL_NAME}   ${CHARM_NAME}
    ${length}=   Get Length   ${ns_charm_app_name}
    Should Be True   ${length} <50

Check VDU Charm Application Name
    [Documentation]   Check that the VDU charm has the right length.
    ${vdu_charm_app_name}=   Get Application Name VDU Level Charm   %{OSM_HOSTNAME}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${MODEL_NAME}   ${VDU_PROFILE_ID}   ${EE_NAME}
    ${length}=   Get Length   ${vdu_charm_app_name}
    Should Be True   ${length} <50

# TODO: Check Initial Config Primitives Status

Delete NS
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}
    Delete Temporary Descriptor Folder   '%{PACKAGES_FOLDER}/${NEW_NSD_PKG}'

Delete VNF Descriptors
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME1}
    Delete VNFD   ${VNFD_NAME2}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor and instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME1}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME2}
    Delete Temporary Descriptor Folder   '%{PACKAGES_FOLDER}/${NEW_NSD_PKG}'

Delete Temporary Descriptor Folder
    [Documentation]   Removes the temporary package folder created for the test
    [Arguments]   ${folder_name}
    ${rc}   ${stdout}=   Run And Return RC And Output   rm -rf '${folder_name}'
    Log   ${rc},${stdout}
