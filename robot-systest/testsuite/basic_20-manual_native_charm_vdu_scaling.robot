*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-20] Manual VDU Scaling to test juju applications are scaling for native charms (testing Bug-1609).

Library   OperatingSystem
Library   String
Library   Collections

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_20   cluster_main   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   charm-packages/native_manual_scale_charm_vnf
${VNFD_NAME}   native_manual_scale_charm-vnf
${NSD_PKG}   charm-packages/native_manual_scale_charm_ns
${NSD_NAME}   native_manual_scale_charm-ns

# NS instance name and configuration
${NS_NAME}   basic_20_manual_vdu_scaling_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${VNF_MEMBER_INDEX}   vnf

# SSH public key file
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub

# Scaling group name of VDU instances
${SCALING_GROUP}   manual-scaling_mgmtVM

${SCALE_WAIT_TIME}   4min


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Retrieve VNF instance id to be used later on
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Get Vdus Before Scale Out
    [Documentation]   Get the number of VDU records before the manual scaling.
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    Set Suite Variable   ${INITIAL_VDUR_COUNT}   ${vdurs}

Get Application Name Before Scale Out
    [Documentation]   Get the application names.
    @{name_list}=   Get Application Names   ${NS_NAME}
    Log List   ${name_list}
    Set Suite Variable   ${APPLICATION_LIST}   ${name_list}

Check Number of Duplicated Application Name Before Scale Out
    [Documentation]   Check the application names if there are 2 application name in the application list.
    ${count_duplicated}=   Get Length   ${APPLICATION_LIST}
    IF   ${count_duplicated} != 2   Fail   msg=There are not 2 application name in the application list before Scale Out

Perform Manual Vdu Scale Out
    [Documentation]   Perform a manual scale-out operation of the VNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_OUT   ${SCALE_WAIT_TIME}
    Log   ${ns_op_id}

Check Vdus After Scale Out
    [Documentation]   Check whether there is one more VDU after scaling out.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    IF   ${vdurs} != ${INITIAL_VDUR_COUNT} + 1   Fail   msg=There is no new VDU records in the VNF after Scale Out

Get Application Name After Scale Out
    [Documentation]   Get the application names.
    @{name_list}=   Get Application Names   ${NS_NAME}
    Log List   ${name_list}
    Set Suite Variable   ${APPLICATION_LIST}   ${name_list}

Check Number of Duplicated Application Name After Scale Out
    [Documentation]   Check the application names if there are 3 application name in the application list.
    ${count_duplicated}=   Get Length   ${APPLICATION_LIST}
    IF   ${count_duplicated} != 3   Fail   msg=There are not 2 application name in the application list after Scale Out

Perform Manual Vdu Scale In
    [Documentation]   Perform a manual scale-in operation of the VNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_IN
    Log   ${ns_op_id}

Check Vdus After Scaling In
    [Documentation]   Check whether there is one less VDU after scaling in.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    IF   ${vdurs} != ${INITIAL_VDUR_COUNT}   Fail   msg=There is the same number of VDU records in the VNF after Scale In

Get Application Name After Scale In
    [Documentation]   Get the application names.
    @{name_list}=   Get Application Names   ${NS_NAME}
    Log List   ${name_list}
    Set Suite Variable   ${APPLICATION_LIST}   ${name_list}

Check Number of Duplicated Application Name After Scale In
    [Documentation]   Check the application names if there are 2 application name in the application list.
    ${count_duplicated}=   Get Length   ${APPLICATION_LIST}
    IF   ${count_duplicated} != 2   Fail   msg=There are not 2 application name in the application list after Scale In

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
