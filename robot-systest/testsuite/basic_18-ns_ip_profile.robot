*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-18] NS with a VLD with a virtual link profile.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_18   cluster_main   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   ubuntu_4ifaces_vnf
${VNFD_NAME}   ubuntu_4ifaces-vnf
${NSD_PKG}   ubuntu_4ifaces_ns
${NSD_NAME}   ubuntu_4ifaces-ns

# NS instance name and configuration
${NS_NAME}   basic_18
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu

# VNFs data
${VNF_MEMBER_INDEX_1}   vnf1
${VNF_MEMBER_INDEX_2}   vnf2
${IFACE1_NAME}   eth1
${IFACE2_NAME}   eth2
${IFACE3_NAME}   eth3
${DATANET1_PREFIX}   ^192.168.10.*
${DATANET2_PREFIX}   ^192.168.20.*
${DATANET3_PREFIX}   ^192.168.30.*

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnfs Info
    [Documentation]   Get information from the two VNF of the Ns, specifically the VNF instance id and the mgmt IP address.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID1}   ${vnfr_list}[0]
    Set Suite Variable   ${VNF_ID2}   ${vnfr_list}[1]
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Set Suite Variable   ${VNF1_IPMGMT}   ${ip}
    Log   ${VNF1_IPMGMT}
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_2}
    Set Suite Variable   ${VNF2_IPMGMT}   ${ip}
    Log   ${VNF2_IPMGMT}

Check Vnf1 IPs
    [Documentation]   Check whether IP addresses of the first VNF in the VNF record meet the expected CIDR.
    Variable Should Exist   ${VNF_ID1}   msg=VNF1 is not available
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID1} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE1_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET1_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET1_PREFIX}
    Set Suite Variable   ${VNF1_IP1}   ${stdout}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID1} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE2_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET2_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET2_PREFIX}
    Set Suite Variable   ${VNF1_IP2}   ${stdout}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID1} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE3_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET3_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET3_PREFIX}
    Set Suite Variable   ${VNF1_IP3}   ${stdout}

Check Vnf2 IPs
    [Documentation]   Check whether IP addresses of the second VNF in the VNF record meet the expected CIDR.
    Variable Should Exist   ${VNF_ID1}   msg=VNF2 is not available
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID2} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE1_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET1_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET1_PREFIX}
    Set Suite Variable   ${VNF2_IP1}   ${stdout}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID2} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE2_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET2_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET2_PREFIX}
    Set Suite Variable   ${VNF2_IP2}   ${stdout}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnf-show ${VNF_ID2} --literal | yq '.vdur[0].interfaces[] | select(.name == "${IFACE3_NAME}")' | yq -r '."ip-address"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    Should Match Regexp   ${stdout}   ${DATANET3_PREFIX}   msg=${stdout} doesn't match subnet's regexp ${DATANET3_PREFIX}
    Set Suite Variable   ${VNF2_IP3}   ${stdout}

Verify Vnf1 Interfaces
    [Documentation]   Check whether IP addresses of the first VNF configured inside the VM meet the expected CIDR.
    Variable Should Exist   ${VNF1_IPMGMT}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_1}' is not available
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    Sleep   10 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF1_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip --brief addr show up | grep -v "^lo" | awk '{print $3}'
    Log   ${stdout}
    @{ip}=   Split String   ${stdout}
    Should Match Regexp   ${ip}[1]   ${DATANET1_PREFIX}   msg=${ip}[1] doesn't match subnet's regexp ${DATANET1_PREFIX}
    Should Match Regexp   ${ip}[2]   ${DATANET2_PREFIX}   msg=${ip}[2] doesn't match subnet's regexp ${DATANET2_PREFIX}
    Should Match Regexp   ${ip}[3]   ${DATANET3_PREFIX}   msg=${ip}[3] doesn't match subnet's regexp ${DATANET3_PREFIX}

Verify Vnf2 Interfaces
    [Documentation]   Check whether IP addresses of the second VNF configured inside the VM meet the expected CIDR.
    Variable Should Exist   ${VNF2_IPMGMT}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_2}' is not available
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF2_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip --brief addr show up | grep -v "^lo" | awk '{print $3}'
    Log   ${stdout}
    @{ip}=   Split String   ${stdout}
    Should Match Regexp   ${ip}[1]   ${DATANET1_PREFIX}   msg=${ip}[1] doesn't match subnet's regexp ${DATANET1_PREFIX}
    Should Match Regexp   ${ip}[2]   ${DATANET2_PREFIX}   msg=${ip}[2] doesn't match subnet's regexp ${DATANET2_PREFIX}
    Should Match Regexp   ${ip}[3]   ${DATANET3_PREFIX}   msg=${ip}[3] doesn't match subnet's regexp ${DATANET3_PREFIX}

Ping from Vnf1 to Vnf2
    [Documentation]   Check connectivity from the first VNF to the second VNF using ping.
    Variable Should Exist   ${VNF1_IPMGMT}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_1}' is not available
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF1_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip addr ; ping -c 5 ${VNF2_IPMGMT}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF1_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ping -c 5 ${VNF2_IP1}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF1_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ping -c 5 ${VNF2_IP2}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF1_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ping -c 5 ${VNF2_IP3}
    Log   ${stdout}

Ping from Vnf2 to Vnf1
    [Documentation]   Check connectivity from the second VNF to the first VNF using ping.
    Variable Should Exist   ${VNF1_IPMGMT}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_2}' is not available
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF2_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip addr ; ping -c 5 ${VNF1_IPMGMT}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF2_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip addr ; ping -c 5 ${VNF1_IP1}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF2_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip addr ; ping -c 5 ${VNF1_IP2}
    Log   ${stdout}
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF2_IPMGMT}   ${USERNAME}   ${EMPTY}   ${PRIVATEKEY}   ip addr ; ping -c 5 ${VNF1_IP3}
    Log   ${stdout}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
