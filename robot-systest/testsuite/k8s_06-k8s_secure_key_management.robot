*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-06] K8s Secure Key Management.

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   k8s_06   cluster_ee_config   cluster_k8s_charms   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   charm-packages/nopasswd_k8s_proxy_charm_vnf
${VNFD_NAME}   nopasswd_k8s_proxy_charm-vnf
${NSD_PKG}   charm-packages/nopasswd_k8s_proxy_charm_ns
${NSD_NAME}   nopasswd_k8s_proxy_charm-ns

# NS instance name and configuration
${NS_NAME}   k8s_06-nopasswd_k8s_proxy_charm
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   15min

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${ACTION_NAME}   touch
${VNF_MEMBER_INDEX_1}   vnf1
${VNF_MEMBER_INDEX_2}   vnf2
${DAY_1_FILE_NAME}   /home/ubuntu/first-touch
${DAY_2_FILE_NAME_1}   /home/ubuntu/mytouch1
${DAY_2_FILE_NAME_2}   /home/ubuntu/mytouch2


*** Test Cases ***
Create Charm VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    [Tags]   prepare
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Charm NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    [Tags]   prepare
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Charm Network Service
    [Documentation]   Instantiate NS for the testsuite.
    [Tags]   prepare
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Log   ${id}

Get Ns Id
    [Documentation]   Get the NS id and save it NS_ID suite variable to use it later on.
    [Tags]   verify
    ${id}=   Get Ns Id   ${NS_NAME}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Addresses
    [Documentation]   Get the mgmt IP address of the two VNF of the NS.
    [Tags]   verify
    ${ip_addr_1}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Log   ${ip_addr_1}
    Set Suite Variable   ${VNF_1_IP_ADDR}   ${ip_addr_1}
    ${ip_addr_2}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_2}
    Log   ${ip_addr_2}
    Set Suite Variable   ${VNF_2_IP_ADDR}   ${ip_addr_2}

Test SSH Access
    [Documentation]   Check that both VNF are accessible via SSH in their respective mgmt IP address.
    [Tags]   verify
    Variable Should Exist   ${VNF_1_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_1}' is not available
    Variable Should Exist   ${VNF_2_IP_ADDR}   msg=IP address of the management VNF '${VNF_MEMBER_INDEX_2}' is not available
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_1_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}
    Test SSH Connection   ${VNF_2_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Check Remote Files Created Via Day 1 Operations
    [Documentation]   The Charm VNF has a Day 1 operation that creates a file named ${DAY_1_FILE_NAME}.
    ...               This test checks whether that files have been created or not.
    [Tags]   verify
    Check If Remote File Exists   ${VNF_1_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_1_FILE_NAME}
    Check If Remote File Exists   ${VNF_2_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_1_FILE_NAME}

Execute Day 2 Operations
    [Documentation]   Performs one Day 2 operation per VNF that creates a new file.
    [Tags]   verify
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id_1}=   Execute NS Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX_1}   filename=${DAY_2_FILE_NAME_1}
    Log   ${ns_op_id_1}
    ${ns_op_id_2}=   Execute NS Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX_2}   filename=${DAY_2_FILE_NAME_2}
    Log   ${ns_op_id_2}

Check Remote Files Created Via Day 2 Operations
    [Documentation]   Check whether the files created in the previous test via Day 2 operations exist or not.
    [Tags]   verify
    Check If Remote File Exists   ${VNF_1_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_2_FILE_NAME_1}
    Check If Remote File Exists   ${VNF_2_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   ${DAY_2_FILE_NAME_2}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete NF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
