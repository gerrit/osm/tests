*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [BASIC-13] NS Relations

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   basic_13   cluster_ee_config   cluster_relations   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG1}   charm-packages/ns_relations_provides_vnf
${VNFD_PKG2}   charm-packages/ns_relations_requires_vnf
${VNFD_NAME1}   ns_relations_provides-vnf
${VNFD_NAME2}   ns_relations_requires-vnf
${NSD_PKG}   charm-packages/ns_relations_ns
${NSD_NAME}   ns_relations-ns

# NS instance name and configuration
${NS_NAME}   basic_13_ns_relations_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   15min

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${ACTION_NAME}   touch
${VNF_MEMBER_INDEX_1}   vnf1
${VNF_MEMBER_INDEX_2}   vnf2
${DAY_1_FILE_NAME}   /home/ubuntu/first-touch
${DAY_2_FILE_NAME_1}   /home/ubuntu/mytouch1
${DAY_2_FILE_NAME_2}   /home/ubuntu/mytouch2


*** Test Cases ***
Create Charm VNF Descriptor Provides
    [Documentation]   Upload VNF package for the testsuite with a charm providing a relation.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG1}'

Create Charm VNF Descriptor Requires
    [Documentation]   Upload VNF package for the testsuite with a charm requiring a relation.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG2}'

Create Charm NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Charm Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}

# TODO: Check juju status for relations

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Provides
    [Documentation]   Delete first VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME1}

Delete VNF Descriptor Requires
    [Documentation]   Delete second VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME2}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME1}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME2}
