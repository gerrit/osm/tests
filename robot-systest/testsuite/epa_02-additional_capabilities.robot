*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [EPA-02] Support for additional EPA capabilities.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   epa_02   cluster_epa   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   epa_quota_vnf
${VNFD_NAME}   epa_quota-vnf
${NSD_PKG}   epa_quota_ns
${NSD_NAME}   epa_quota-ns

# NS instance name and configuration
${NS_NAME}   epa_02-epa_quota_test
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH public keys file
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub

${NS_ID}   ${EMPTY}
${VNF_SERVER_ID}   ${EMPTY}
${FLAVOR_PROPERTIES}   ${EMPTY}
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Remove Environment Variable   OVERRIDES
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate the NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get VNF Server ID
    [Documentation]   Get the id of the VM at the VIM and store in VNF_SERVER_ID suite variable to be used later on.
    ${vnfs_list}=   Get Ns Vnf List   ${NS_ID}
    ${vim_id}=   Get VNF VIM ID   ${vnfs_list}[0]
    Log   ${vim_id}
    Set Suite Variable   ${VNF_SERVER_ID}   ${vim_id}

Get Server Flavor Properties
    [Documentation]   Get from the VIM the flavor properties of the VM with id VNF_SERVER_ID.
    ${flavor_id}=   Get Server Flavor ID   ${VNF_SERVER_ID}
    ${properties}=   Get Flavor Properties   ${flavor_id}
    Log   ${properties}
    Set Suite Variable   ${FLAVOR_PROPERTIES}   ${properties}

Check Flavor Quota Properties
    [Documentation]   Assert that the flavor properties include the expected EPA parameters included in the descriptor.
    ${rc}=   Run And Return Rc   echo ${FLAVOR_PROPERTIES} | grep "cpu_shares_level=custom"
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    ${rc}=   Run And Return Rc   echo ${FLAVOR_PROPERTIES} | grep "disk_io_shares_level=custom"
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}
    ${rc}=   Run And Return Rc   echo ${FLAVOR_PROPERTIES} | grep "memory_shares_level=custom"
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
