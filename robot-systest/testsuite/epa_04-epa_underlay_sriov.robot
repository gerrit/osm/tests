*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [EPA-04] EPA underlay SR-IOV.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/openstack_lib.resource

Test Tags   epa_04   cluster_epa   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   epa_1vm_sriov_vnf
${VNFD_NAME}   epa_1vm_sriov-vnf
${NSD_PKG}   epa_1vm_sriov_ns
${NSD_NAME}   epa_1vm_sriov-ns
${NS_NAME}   epa_04

# Fixed IPs and subnet for datanet VL
${DATANET_SUBNET}   192.168.100.0/24
${DATANET_IP1}   192.168.100.11
${DATANET_IP2}   192.168.100.22

${VNF_MEMBER_INDEX_1}   vnf1
${VNF_MEMBER_INDEX_2}   vnf2
${USERNAME}   ubuntu
${PASSWORD}   osm4u
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} , {name: datanet, ip-profile: {ip-version: ipv4, subnet-address: "${DATANET_SUBNET}"}, vnfd-connection-point-ref: [ {member-vnf-index-ref: "${VNF_MEMBER_INDEX_1}", vnfd-connection-point-ref: vnf-data-ext, ip-address: "${DATANET_IP1}"}, {member-vnf-index-ref: "${VNF_MEMBER_INDEX_2}", vnfd-connection-point-ref: vnf-data-ext, ip-address: "${DATANET_IP2}"}]} ] }
${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Remove Environment Variable   OVERRIDES
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${EMPTY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Management Ip Addresses
    [Documentation]   Get the mgmt IP addresses of both VNF of the NS.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_1}
    Set Suite Variable   ${IP_MGMT_1}   ${ip}
    Log   ${IP_MGMT_1}
    ${ip}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX_2}
    Set Suite Variable   ${IP_MGMT_2}   ${ip}
    Log   ${IP_MGMT_2}

Ping from Vnf1 to Vnf2
    [Documentation]   Check connectivity from the first VNF to the second VNF using ping.
    Variable Should Exist   ${IP_MGMT_1}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_1}' is not available
    Sleep   30 seconds   Wait for SSH daemon to be up
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${IP_MGMT_1}   ${USERNAME}   ${PASSWORD}   ${EMPTY}   ip addr ; ping -c 5 ${DATANET_IP2}
    Log   ${stdout}

Ping from Vnf2 to Vnf1
    [Documentation]   Check connectivity from the second VNF to the first VNF using ping.
    Variable Should Exist   ${IP_MGMT_2}   msg=IP address of the data VNF '${VNF_MEMBER_INDEX_2}' is not available
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${IP_MGMT_2}   ${USERNAME}   ${PASSWORD}   ${EMPTY}   ip addr ; ping -c 5 ${DATANET_IP1}
    Log   ${stdout}

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
