*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License


*** Settings ***
Documentation   [SOL003-01] Sol003 Api testing

Library   OperatingSystem
Library   String
Library   Collections
Library   RequestsLibrary
Library   yaml
Library   JsonValidator
Library   JSONLibrary

Resource   ../lib/rest_lib.resource
Resource   ../lib/vnfd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/vnf_lib.resource
Resource   ../lib/vim_lib.resource

Test Tags   sol003_01   cluster_osm_rest   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# SOL003 API URI paths
${VNF_INSTANCE_URI}   /osm/vnflcm/v1/vnf_instances
${VNF_INSTANCE_LCM_OPS}   /osm/vnflcm/v1/vnf_lcm_op_occs

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_basic_metrics_vnf
${VNFD_NAME}   hackfest_basic_metrics-vnf


*** Test Cases ***
Create VNF Package From JSON File
    [Documentation]   Create a VNF package and update it via SOL003 interface using a JSON file
    Get VNFDs List
    ${id}=   Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'
    Log   ${id}
    ${CREATED_VIM_ACCOUNT_ID}=   Get VIM Target ID   %{VIM_TARGET}
    Set Suite Variable   ${CREATED_VIM_ACCOUNT_ID}
    Get Auth Token
    ${json_path}=   Read Directory
    ${json_obj}=   Load JSON From File   ${json_path}/sol003_01-vnf-lifecycle-management.json
    ${json_obj}=   Get Variable Value   ${json_obj['data'][0]}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${json_obj}   $..vimAccountId   ${CREATED_VIM_ACCOUNT_ID}
    Log   ${UPDATED_JSON_OBJ}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..additionalParams.virtual-link-desc[0][id]   %{VIM_MGMT_NET}
    Log   ${UPDATED_JSON_OBJ}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..additionalParams.virtual-link-profile-id   %{VIM_MGMT_NET}
    Log   ${UPDATED_JSON_OBJ}
    Set Test Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}   ${UPDATED_JSON_OBJ}
    Pass Execution If   ${request_response.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Query VNF Instance completed
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${CREATEID}   ${id[0]}
    Get VNFDs List

Instantiate VNF
    [Documentation]   Test case to Instantiate a VNF
    Get Ns List
    Get Vnf List
    Get Auth Token
    ${json_path}=   Read Directory
    ${json_obj}=   Load JSON From File   ${json_path}/sol003_01-vnf-lifecycle-management.json
    ${json_obj}=   Get Variable Value   ${json_obj['data'][1]}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${json_obj}   $..vimAccountId   ${CREATED_VIM_ACCOUNT_ID}
    ${UPDATED_JSON_OBJ}=   JSONLibrary.Update Value To Json   ${UPDATED_JSON_OBJ}   $..vnfId   ${CREATEID}
    Set Test Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}/${CREATEID}/instantiate   ${UPDATED_JSON_OBJ}
    Pass Execution If   ${request_response.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Instantiate VNF Instance completed
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${INSTANTIATEID}   ${id[0]}
    Sleep   12s
    Get Ns List
    Get Vnf List

Query VNF Instances
    [Documentation]   Test case to query VNF Instance
    Get Auth Token
    Get Api Request   ${VNF_INSTANCE_URI}
    ${value}=   Get ID   nsState
    Should Be Equal   ${value}   INSTANTIATED
    Pass Execution If   ${request_response.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Query VNF Instance completed
    ${id}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${QUERYID}   ${id[0]}

Query VNF Instance ID
    [Documentation]   Test case to query Vnf instance ID
    Get Auth Token
    Get Api Request   ${VNF_INSTANCE_URI}/${QUERYID}
    ${value}=   Get ID   nsState
    Should Be Equal   ${value}   INSTANTIATED
    Should Be Equal As Strings   ${RequestResponse.status_code}   200
    ${id}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${INSTANCEID}   ${id[0]}

Query VNF LCM Ops
    [Documentation]   Test case to Query VNF LCM operation
    Get Auth Token
    Get Api Request   ${VNF_INSTANCE_LCM_OPS}
    ${id1}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${LCMOPS}   ${id1[0]}
    FOR   ${index}   IN RANGE   0   15
        Log   ${index}
        Get Api Request   ${VNF_INSTANCE_LCM_OPS}
        ${value}=   Get ID   operationState
        ${status}=   Run Keyword And Return Status   Should Be Equal   ${value}   COMPLETED
        IF   ${status}
            BREAK
        ELSE
            Sleep   10s
        END
    END

Query VNF LCM Ops ID
    [Documentation]   Check if the operation has completed.
    Get Auth Token
    Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${LCMOPS}
    ${id1}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${LCMOPSID}   ${id1[0]}
    FOR   ${index}   IN RANGE   0   15
        Log   ${index}
        Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${LCMOPS}
        ${value}=   Get ID   operationState
        ${status}=   Run Keyword And Return Status   Should Be Equal   ${value}   COMPLETED
        IF   ${status}
            BREAK
        ELSE
            Sleep   10s
        END
    END

Scale VNF
    [Documentation]   Scale out VNF
    Get Auth Token
    ${json_path}=   Read Directory
    ${updated_json_obj}=   Load JSON From File   ${json_path}/sol003_01-vnf-lifecycle-management.json
    ${updated_json_obj}=   Get Variable Value   ${UPDATED_JSON_OBJ['data'][2]}
    Set Test Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}/${CREATEID}/scale   ${UPDATED_JSON_OBJ}
    ${value}=   Get ID   id
    ${id1}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${SCALEOUTID}   ${id1[0]}
    Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${SCALEOUTID}
    ${LcmopsScale}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${LCMOPSSCALEID}   ${LcmopsScale[0]}
    FOR   ${index}   IN RANGE   0   15
        Log   ${index}
        Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${SCALEOUTID}
        ${value}=   Get ID   operationState
        ${status}=   Run Keyword And Return Status   Should Be Equal   ${value}   COMPLETED
        IF   ${status}
            BREAK
        ELSE
            Sleep   10s
        END
    END
    Pass Execution If   ${request_response.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Scale VNF instance completed

Terminate VNF
    [Documentation]   Test case to terminate the VNF
    Get Auth Token
    ${json_path}=   Read Directory
    ${updated_json_obj}=   Load JSON From File   ${json_path}/sol003_01-vnf-lifecycle-management.json
    ${updated_json_obj}=   Get Variable Value   ${UPDATED_JSON_OBJ['data'][3]}
    Set Suite Variable   ${UPDATED_JSON_OBJ}
    Post Api Request   ${VNF_INSTANCE_URI}/${CREATEID}/terminate   ${UPDATED_JSON_OBJ}
    ${id}=   Get Value From Json   ${request_response.json()}   $..id
    Set Suite Variable   ${TERMINATEID}   ${id[0]}
    Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${TERMINATEID}
    ${LcmopsTerminate}=   Get Value From Json   ${request_response.json()}   $.._id
    Set Suite Variable   ${LCMOPSSCALEID}   ${LcmopsTerminate[0]}
    FOR   ${index}   IN RANGE   0   15
        Log   ${index}
        Get Api Request   ${VNF_INSTANCE_LCM_OPS}/${TERMINATEID}
        ${value}=   Get ID   operationState
        ${status}=   Run Keyword And Return Status   Should Be Equal   ${value}   COMPLETED
        IF   ${status}
            BREAK
        ELSE
            Sleep   10s
        END
    END
    Pass Execution If   ${request_response.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Terminate VNF instance completed

Delete VNF
    [Documentation]   Delete VNF instance.
    Get Auth Token
    Create Session   APISession   ${REST_API_HOST}
    &{Headers}=   Create Dictionary   Content-Type=application/json   Accept=application/json   Authorization=Bearer ${ACCESS_TOKEN}
    ${resp}=   DELETE On Session   APISession   ${VNF_INSTANCE_URI}/${CREATEID}   headers=${Headers}
    Pass Execution If   ${resp.status_code} in ${SUCCESS_STATUS_CODE_LIST}   Delete VNF Instance completed

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
