*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [EPA-03] CRUD operations on SDNC accounts.

Resource   ../lib/prometheus_lib.resource
Resource   ../lib/sdnc_lib.resource

Test Tags   epa_03   cluster_epa   daily   regression

Suite Setup   Run Keyword And Ignore Error   Suite Preparation
Suite Teardown   Run Keyword And Ignore Error   Clean SDNC


*** Variables ***
${SDNC_ENABLED_MAX_WAIT_TIME}   1min
${SDNC_ENABLED_POL_TIME}   10
${SDNC_NAME}   epa_03
${SDNC_USER}   %{SDNC_USER=karaf}
${SDNC_PASSWORD}   %{SDNC_PASSWORD=karaf}
${SDNC_URL}   %{SDNC_URL=http://localhost:8181}
${SDNC_TYPE}   %{SDNC_TYPE=onos_vpls}


*** Test Cases ***
Create Basic SDNC
    [Documentation]   Register SDN controller in OSM.
    ${created_sdnc_id}=   Create SDNC   ${SDNC_NAME}   ${SDNC_USER}   ${SDNC_PASSWORD}   ${SDNC_URL}   ${SDNC_TYPE}
    Set Suite Variable   ${SDNC_ID}   ${created_sdnc_id}
    Wait Until Keyword Succeeds   ${SDNC_ENABLED_MAX_WAIT_TIME}   ${SDNC_ENABLED_POL_TIME}   Check For SDNC   ${SDNC_ID}

Check SDNC Status Is Healthy
    [Documentation]   Check in Prometheus that the SDN controller is in healthy state.
    Check For SDNC Status   ${SDNC_ID}   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}

Delete Basic SDNC
    [Documentation]   Delete SDN controller from OSM.
    [Tags]   cleanup
    Delete SDNC   ${SDNC_NAME}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables.
    Set Testsuite Prometheus Variables

Clean SDNC
    [Documentation]   Delete SDN controller from OSM.
    Delete SDNC   ${sdnc_name}
