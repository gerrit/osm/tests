*** Comments ***
#   Copyright 2020 Canonical Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-09] Pebble Charm.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/package_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   k8s_09   cluster_k8s   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   charm-packages/pebble_charm_vnf
${VNFD_NAME}   pebble_charm-vnf
${NSD_PKG}   charm-packages/pebble_charm_ns
${NSD_NAME}   pebble_charm-ns

# NS instance name and configuration
${NS_NAME}   pebble-charm-k8s
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

${NS_ID}   ${EMPTY}
${PUBLICKEY}   ${EMPTY}
${VNF_MEMBER_INDEX}   pebble_charm-vnf
${ACTION_NAME}   list-available-apps
${KDU_NAME}   onos-kdu
${APPLICATION_NAME}   onos


*** Test Cases ***
Create Simple K8s VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Simple K8s Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service K8s Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Execute Day 2 Operations
    [Documentation]   Performs one Day 2 operation per VNF that creates a new file.

    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   application-name=${APPLICATION_NAME}
    Log   ${ns_op_id}

Delete NS K8s Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete the NS package.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete the VNF package.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}

Delete VNF NS Packages
    [Documentation]   Delete the tar.gz files associated to the VNF and NS packages.
    [Tags]   cleanup
    Delete Package   '%{PACKAGES_FOLDER}/${VNFD_PKG}'
    Delete Package   '%{PACKAGES_FOLDER}/${NSD_PKG}'


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
