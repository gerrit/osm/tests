*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-12] Openldap Helm chart.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   k8s_12   cluster_k8s   regression   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# K8s cluster name and version
${K8SCLUSTER_NAME}   k8s12
${K8SCLUSTER_VERSION}   v1

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   openldap_primitives_knf
${VNFD_NAME}   openldap_knf
${NSD_PKG}   openldap_primitives_ns
${NSD_NAME}   openldap_ns

# NS instance name and configuration
${NS_NAME}   ldap
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}}], additionalParamsForVnf: [ {member-vnf-index: openldap, additionalParamsForKdu: [ {kdu_name: ldap, additionalParams: {service: {type: LoadBalancer }, adminPassword: admin}} ] } ] }

${NS_ID}   ${EMPTY}
${PUBLICKEY}   ${EMPTY}
${VNF_MEMBER_INDEX}   openldap
${KDU_NAME}   ldap
${UPGRADE_ACTION}   upgrade
${ROLLBACK_ACTION}   rollback
${REPLICA_COUNT}   3
${ACTION_NAME}   get-user-info
${ACTION_PARAM_CN}   admin
${ACTION_PARAM_DC}   example.org


*** Test Cases ***
Create Package For OpenLDAP CNF
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Package For OpenLDAP NS
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Add K8s Cluster To OSM
    [Documentation]   Register K8s cluster in OSM.
    Create K8s Cluster   %{K8S_CREDENTIALS}   ${K8SCLUSTER_VERSION}   %{VIM_TARGET}   %{VIM_MGMT_NET}   ${K8SCLUSTER_NAME}
    Check For K8s Cluster To Be Enabled   ${K8SCLUSTER_NAME}

Create Network Service Instance
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Log   ${id}

Get Ns Id
    [Documentation]   Get NS instance id from OSM.
    ${id}=   Get Ns Id   ${NS_NAME}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Get VNF instance id from OSM.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Execute Day 2 Operations
    [Documentation]   Performs one Day 2 operation.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${ACTION_NAME}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   cn=${ACTION_PARAM_CN}   dc=${ACTION_PARAM_DC}
    Log   ${ns_op_id}

Execute Upgrade Operation
    [Documentation]   Perform OSM action to upgrade the number of replicas of a deployment in the CNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${UPGRADE_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}   replicaCount=${REPLICA_COUNT}
    Log   ${ns_op_id}

Check Replicas After Upgrade Operation
    [Documentation]   Check that the number of replicas after the upgrade is the expected one.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU_NAME}
    Log   ${count}
    Should Be Equal As Integers   ${count}   ${REPLICA_COUNT}

Execute Rollback Operation
    [Documentation]   Perform OSM action to rollback the previous upgrade of the CNF.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${ns_op_id}=   Execute NS K8s Action   ${NS_NAME}   ${ROLLBACK_ACTION}   ${VNF_MEMBER_INDEX}   ${KDU_NAME}
    Log   ${ns_op_id}

Check Replicas After Rollback Operation
    [Documentation]   Check that the number of replcias after the rollback is the expected one.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU_NAME}
    Log   ${count}
    Should Be Empty   ${count}

Delete Network Service Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Remove K8s Cluster from OSM
    [Documentation]   Delete K8s cluster from OSM.
    [Tags]   cleanup
    Delete K8s Cluster   ${K8SCLUSTER_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete NF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Delete K8s Cluster   ${K8SCLUSTER_NAME}
