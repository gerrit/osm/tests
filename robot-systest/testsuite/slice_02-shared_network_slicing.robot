*** Comments ***
#   Copyright 2020 Atos
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [SLICE-02] Shared Network Slicing.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/nst_lib.resource
Resource   ../lib/nsi_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   slice_02   cluster_slices   daily   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package files
${VNFD1_PKG}   slice_basic_vnf
${VNFD2_PKG}   slice_basic_middle_vnf
${NSD1_PKG}   slice_basic_ns
${NSD2_PKG}   slice_basic_middle_ns
${NST}   slice_basic_nst/slice_basic_nst.yaml
${NST2}   slice_basic_nst/slice_basic_2nd_nst.yaml

# Instance names
${SLICE_NAME}   slicebasic
${SLICE2_NAME}   sliceshared
${MIDDLE_NS_NAME}   slicebasic.slice_basic_nsd_2

# Descriptor names
${NST_NAME}   slice_basic_nst
${NST2_NAME}   slice_basic_nst2
${VNFD1_NAME}   slice_basic_vnf
${VNFD2_NAME}   slice_basic_middle_vnf
${NSD1_NAME}   slice_basic_ns
${NSD2_NAME}   slice_basic_middle_ns

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}

${VNF_MEMBER_INDEX}   middle
${VNF_IP_ADDR}   ${EMPTY}
${NST_CONFIG}   {netslice-vld: [ {name: slice_vld_mgmt, vim-network-name: %{VIM_MGMT_NET}} ] }


*** Test Cases ***
Create Slice VNF Descriptors
    [Documentation]   Onboards all the VNFDs required for the test: vnfd1_pkg and vnfd2_pkg (in the variables file)
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD1_PKG}'
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD2_PKG}'

Create Slice NS Descriptors
    [Documentation]   Onboards all the NSDs required for the test: nsd1_pkg and nsd2_pkg (in the variables file)
    Create NSD   '%{PACKAGES_FOLDER}/${NSD1_PKG}'
    Create NSD   '%{PACKAGES_FOLDER}/${NSD2_PKG}'

Create Slice Templates
    [Documentation]   Onboards the Network Slice Templates: nst, nst2 (in the variables file)
    Create NST   '%{PACKAGES_FOLDER}/${NST}'
    Create NST   '%{PACKAGES_FOLDER}/${NST2}'

Network Slice First Instance
    [Documentation]   Instantiates the First NST recently onboarded (nst_name) and sets the instantiation id as a suite variable (nsi_id)
    ...               The slice contains 3 NS (1 shared)
    ${id}=   Create Network Slice   ${NST_NAME}   %{VIM_TARGET}   ${SLICE_NAME}   ${NST_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NSI_ID}   ${id}

Network Slice Second Instance
    [Documentation]   Instantiates the Second NST recently onboarded (nst2_name) and sets the instantiation id as a suite variable (nsi2_id)
    ...               The slice contains 2 NS (1 shared)
    ${id}=   Create Network Slice   ${NST2_NAME}   %{VIM_TARGET}   ${SLICE2_NAME}   ${NST_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NSI2_ID}   ${id}

First Network Slice Ns Count
    [Documentation]   Counts the NS in both slice instances and shoul be equal to 4
    ${slice1_count}=   Get Slice Ns Count   ${SLICE_NAME}
    ${slice2_count}=   Get Slice Ns Count   ${SLICE2_NAME}
    ${together}=   Evaluate   ${slice1_count} + ${slice2_count}
    Should Be Equal As Integers   ${together}   4

Get Middle Vnf Management Ip
    [Documentation]   Obtains the management IP of the shared NS main (only) VNF and sets it as a suite variable (mgmt_vnf_ip)
    ${middle_ns_id}=   Run And Return RC And Output   osm ns-list | grep ${MIDDLE_NS_NAME} | awk '{print $4}' 2>&1
    ${vnf_ip}=   Get Vnf Management Ip Address   ${middle_ns_id}[1]   ${VNF_MEMBER_INDEX}
    IF   '${vnf_ip}' == '${EMPTY}'   Fatal Error   Variable \$\{ vnf_ip\} Empty
    Set Suite Variable   ${MGMT_VNF_IP}   ${vnf_ip}

Get First Slice Vnf IPs
    [Documentation]   Obtains the list of IPs addresses in the first slice and sets the list as a suite variable (slice1_vnfs_ips)
    # Get all the ns_id in the slice except the middle one
    @{ip_list}=   Get Slice Vnf Ip Addresses   ${SLICE_NAME}
    Should Be True   ${ip_list} is not None
    Set Suite Variable   ${SLICE1_VNFS_IPS}   ${ip_list}

Test Middle Ns Ping
    [Documentation]   Pings the slice middle vnf (mgmt_vnf_ip)
    Sleep   60s   Waiting for the network to be up
    # Ping to the middle VNF
    Test Connectivity   ${MGMT_VNF_IP}

Test Middle Vnf SSH Access
    [Documentation]   SSH access to the slice middle vnf (mgmt_vnf_ip) with the credentials provided in the variables file
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${MGMT_VNF_IP}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}

Test First Slice Connectivity
    [Documentation]   SSH access to the slice middle vnf (mgmt_vnf_ip) with the credentials provided in the variables file
    ...               and pings all the IP addresses in the list (slice1_vnfs_ips)
    Ping Many   ${MGMT_VNF_IP}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   @{SLICE1_VNFS_IPS}

Delete Slice One Instance
    [Documentation]   Stops the slice instance (slice_name)
    [Tags]   cleanup
    Delete NSI   ${SLICE_NAME}

Second Network Slice Ns Count
    [Documentation]   Counts the NS in both slice instances and should be equal to 2
    ${slice1_count}=   Get Slice Ns Count   ${SLICE_NAME}
    ${slice2_count}=   Get Slice Ns Count   ${SLICE2_NAME}
    ${together}=   Evaluate   ${slice1_count} + ${slice2_count}
    Should Be Equal As Integers   ${together}   2

Get Second Slice Vnf IPs
    [Documentation]   Obtains the list of IPs addresses in the second slice and sets the list as a suite variable (slice2_vnfs_ips)
    # Get all the ns_id in the slice
    @{ip_list}=   Get Slice Vnf Ip Addresses   ${SLICE2_NAME}
    Should Be True   ${ip_list} is not None
    Set Suite Variable   ${SLICE2_VNFS_IPS}   ${ip_list}

Test Second Slice Connectivity
    [Documentation]   SSH access to the slice middle vnf (mgmt_vnf_ip) with the credentials provided in the variables file
    ...               and pings all the IP addresses in the list (slice2_vnfs_ips)
    Ping Many   ${MGMT_VNF_IP}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   @{SLICE2_VNFS_IPS}

Delete Slice Two Instance
    [Documentation]   Stops the slice instance (slice2_name)
    [Tags]   cleanup
    Delete NSI   ${SLICE2_NAME}

Delete Slice One Template
    [Documentation]   Deletes the NST (nst_name) from OSM
    [Tags]   cleanup
    Delete NST   ${NST_NAME}

Delete Slice Two Template
    [Documentation]   Deletes the NST (nst2_name) from OSM
    [Tags]   cleanup
    Delete NST   ${NST2_NAME}

Delete NS Descriptors
    [Documentation]   Deletes all the NSDs created for the test: nsd1_name, nsd2_name
    [Tags]   cleanup
    Delete NSD   ${NSD1_NAME}
    Delete NSD   ${NSD2_NAME}

Delete VNF Descriptors
    [Documentation]   Deletes all the VNFDs created for the test: vnfd1_name, vnfd2_name
    [Tags]   cleanup
    Delete VNFD   ${VNFD1_NAME}
    Delete VNFD   ${VNFD2_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptors, instance and templates
    Run Keyword If Any Tests Failed   Delete NSI   ${SLICE_NAME}
    Run Keyword If Any Tests Failed   Delete NSI   ${SLICE2_NAME}

    Run Keyword If Any Tests Failed   Delete NST   ${NST_NAME}
    Run Keyword If Any Tests Failed   Delete NST   ${NST2_NAME}

    Run Keyword If Any Tests Failed   Delete NSD   ${NSD1_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD2_NAME}

    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD1_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD2_NAME}
