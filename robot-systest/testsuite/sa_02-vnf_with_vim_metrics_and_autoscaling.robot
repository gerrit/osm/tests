*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [SA-02] VNF with VIM-based metrics and auto-scaling.

Library   OperatingSystem
Library   String
Library   Collections
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/prometheus_lib.resource

Test Tags   sa_02   cluster_sa   daily   regression   azure

Suite Setup   Run Keyword And Ignore Error   Suite Preparation
Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS instantiation parameters
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_basic_metrics_vnf
${VNFD_NAME}   hackfest_basic_metrics-vnf
${NSD_PKG}   hackfest_basic_metrics_ns
${NSD_NAME}   hackfest_basic-ns-metrics

# NS instance name and id
${NS_ID}   ${EMPTY}
${NS_NAME}   sa_02

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   osm4u

# Prometheus polling interval and retries
${PROMETHEUS_POLL_RETRIES}   15 times
${PROMETHEUS_POLL_TIMEOUT}   1 minute

# Prometheus metrics to retrieve
${METRIC_NAME}   osm_cpu_utilization

# VNF Variables
${VNF_MEMBER_INDEX}   vnf
${VNF_IP_ADDR}   ${EMPTY}
${VNF_ID}   ${EMPTY}

${SUCCESS_RETURN_CODE}   0


*** Test Cases ***
Create VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Get Thresholds From VNF
    [Documentation]   Read metric threshold and threshold time from VNF descriptor.
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnfpkg-show ${VNFD_NAME} --literal | yq -r '.df[0]."scaling-aspect"[0]."scaling-policy"[0]."scaling-criteria"[0]."scale-out-threshold"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    ${scaleout_threshold}=   Convert To Number   ${stdout}
    Set Suite Variable   ${METRIC_THRESHOLD}   ${scaleout_threshold}
    Log   ${METRIC_THRESHOLD}
    ${rc}   ${stdout}=   Run And Return RC And Output   osm vnfpkg-show ${VNFD_NAME} --literal | yq -r '.df[0]."scaling-aspect"[0]."scaling-policy"[0]."threshold-time"'
    Should Be Equal As Integers   ${rc}   ${SUCCESS_RETURN_CODE}   msg=${stdout}   values=False
    ${threshold_time_value}=   Convert To Number   ${stdout}
    Set Suite Variable   ${THRESHOLD_TIME}   ${threshold_time_value}
    Log   ${THRESHOLD_TIME}

Create NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Instantiate Network Service
    [Documentation]   Instantiate the NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get VNF Id
    [Documentation]   Retrieve VNF instance id to be used later on.
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Get VNF IP Address
    [Documentation]   Get the mgmt IP address of the VNF to be used in later tests.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Get VNF VIM-based Metric Before Auto-scaling
    [Documentation]   Get from Prometheus the VIM metric associated to the VNF auto-scaling group.
    Variable Should Exist   ${PROMETHEUS_POLL_RETRIES}   msg=Metric polling retries is not available
    Variable Should Exist   ${PROMETHEUS_POLL_TIMEOUT}   msg=Metric polling timeout is not available
    Variable Should Exist   ${PROMETHEUS_URL}   msg=Prometheus URL is not available
    Variable Should Exist   ${METRIC_NAME}   msg=Prometheus metric name is not available
    ${metric_filter}=   Set Variable   ns_id=${NS_ID}
    ${metric_value}=   Wait Until Keyword Succeeds   ${PROMETHEUS_POLL_RETRIES}   ${PROMETHEUS_POLL_TIMEOUT}   Get Metric   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}   ${METRIC_NAME}   ${metric_filter}
    IF   ${metric_value} <= 0
        Fail   msg=The metric '${METRIC_NAME}' value is '${metric_value}'
    END
    IF   ${metric_value} >= ${METRIC_THRESHOLD}
        Fail   msg=The metric '${METRIC_NAME}' value is higher than '${METRIC_THRESHOLD}' before scaling
    END

Increase VIM-based Metric To Force Auto-scaling
    [Documentation]   Connect to the VNF via SSH and force VIM metric to increase.
    Variable Should Exist   ${PRIVATEKEY}   msg=SSH private key not available
    Execute Remote Command Check Rc Return Output   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${PRIVATEKEY}   for i in {1..9}; do yes &> /dev/null & done

Wait VIM-based Metric To Exceed Threshold
    [Documentation]   Wait until the VIM metric exceeds threshold, which should lead to a scale up.
    Variable Should Exist   ${PROMETHEUS_POLL_RETRIES}   msg=Metric polling retries is not available
    Variable Should Exist   ${PROMETHEUS_POLL_TIMEOUT}   msg=Metric polling timeout is not available
    Wait Until Keyword Succeeds   ${PROMETHEUS_POLL_RETRIES}   ${PROMETHEUS_POLL_TIMEOUT}   Check VIM-based Metric Exceeds Threshold

Wait Threshold Time
    [Documentation]   Wait until the VIM metric has exceeded threshold for the defined period in the descriptor.
    ${threshold_time_sleep}=   Evaluate   ${THRESHOLD_TIME} + 1
    Sleep   ${threshold_time_sleep} minutes   Wait scale-out threshold time plus 1 minute

Check VIM-based Metric Exceeds Threshold After Threshold-time
    [Documentation]   Check that the VIM metric has exceeded the threshold.
    Check VIM-based Metric Exceeds Threshold

Get VDUs After Auto-scaling
    [Documentation]   Check that the VNF has scaled up and the humber of VDUs has increased.
    Sleep   2 minutes   Wait for auto-scale to take place
    @{vdur_list}=   Get Vnf Vdur Names   ${VNF_ID}
    Log List   ${vdur_list}
    ${vdurs}=   Get Length   ${vdur_list}
    IF   ${vdurs} <= 1   Fail   msg=There is no new VDU after auto-scaling

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Preparation
    [Documentation]   Test Suite Preparation: Setting Prometheus Testsuite Variables
    Set Testsuite Prometheus Variables

Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}

Check VIM-based Metric Exceeds Threshold
    [Documentation]   Auxiliar keyword to check if metric exceeds threshold
    Variable Should Exist   ${PROMETHEUS_URL}   msg=Prometheus URL is not available
    Variable Should Exist   ${METRIC_NAME}   msg=Prometheus metric name is not available
    ${metric_filter}=   Set Variable   ns_id=${NS_ID}
    ${metric_value}=   Get Metric   ${PROMETHEUS_URL}   ${PROMETHEUS_USER}   ${PROMETHEUS_PASSWORD}   ${METRIC_NAME}   ${metric_filter}
    IF   ${metric_value} <= ${METRIC_THRESHOLD}
        Fail   msg=The metric '${METRIC_NAME}' value is '${metric_value}' which is lower than '${METRIC_THRESHOLD}'
    END
