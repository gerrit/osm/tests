*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [HACKFEST-CLOUDINIT] Basic NS with two multi-VDU VNF with cloudinit

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource

Test Tags   hackfest_cloudinit   cluster_main   daily   regression   sanity   azure

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# NS and VNF descriptor package folder and ids
${VNFD_PKG}   hackfest_cloudinit_vnf
${VNFD_NAME}   hackfest_cloudinit-vnf
${NSD_PKG}   hackfest_cloudinit_ns
${NSD_NAME}   hackfest_cloudinit-ns

# NS instance name and configuration
${NS_NAME}   hfcloudinit
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# SSH user and password
${USERNAME}   ubuntu
${PASSWORD}   osm4u

${NS_ID}   ${EMPTY}
${VNF_MEMBER_INDEX}   vnf1
${VNF_IP_ADDR}   ${EMPTY}


*** Test Cases ***
Create Hackfest Cloudinit VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Hackfest Cloudinit NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Network Service Instance Test
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${EMPTY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Ip Address
    [Documentation]   Get the mgmt IP address of the VNF of the NS.
    ${ip_addr}=   Get Vnf Management Ip Address   ${NS_ID}   ${VNF_MEMBER_INDEX}
    Log   ${ip_addr}
    Set Suite Variable   ${VNF_IP_ADDR}   ${ip_addr}

Test SSH Access
    [Documentation]   Check that the VNF is accessible via SSH in its mgmt IP address.
    Sleep   30s   Waiting ssh daemon to be up
    Test SSH Connection   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${EMPTY}

Check Remote File Injected Via Cloud-init
    [Documentation]   Check that the day-0 configuration via cloudinit worked as expected in the VNF. A file should exist in the VM.
    ${stdout}=   Execute Remote Command Check Rc Return Output   ${VNF_IP_ADDR}   ${USERNAME}   ${PASSWORD}   ${EMPTY}   sudo cat /root/helloworld.txt
    Log   ${stdout}

Delete NS Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
