*** Comments ***
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.


*** Settings ***
Documentation   [BASIC-22] Cross-model relations

Library   OperatingSystem
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   basic_22   cluster_ee_config   cluster_relations   regression

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# K8s cluster name and version
${K8SCLUSTER_NAME}   k8sbasic_22
${K8SCLUSTER_VERSION}   v1

# NS and VNF descriptor package folder and ids
${VNFD_PKG1}   charm-packages/cmr_relation_vnf
${VNFD_PKG2}   charm-packages/cmr_no_relation_vnf
${NSD_PKG}   charm-packages/cmr_relation_ns
${VNFD_NAME1}   cross_model_relation-vnf
${VNFD_NAME2}   cross_model_no_relation-vnf
${NSD_NAME}   cross_model_relation-ns

# NS instance name and configuration
${NS_NAME}   basic_22
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }
${NS_TIMEOUT}   15min

# SSH keys and username to be used
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${PRIVATEKEY}   %{HOME}/.ssh/id_rsa
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}


*** Test Cases ***
Create Charm VNF Descriptor 1
    [Documentation]   Upload first VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG1}'

Create Charm VNF Descriptor 2
    [Documentation]   Upload second VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG2}'

Create Charm NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Add K8s Cluster To OSM
    [Documentation]   Register K8s cluster in OSM.
    Create K8s Cluster   %{K8S_CREDENTIALS}   ${K8SCLUSTER_VERSION}   %{VIM_TARGET}   %{VIM_MGMT_NET}   ${K8SCLUSTER_NAME}
    Check For K8s Cluster To Be Enabled   ${K8SCLUSTER_NAME}

Instantiate Charm Network Service
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}   ${NS_TIMEOUT}
    Set Suite Variable   ${NS_ID}   ${id}

# TODO: Check juju status for relations

Delete NS Instance
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Remove K8s Cluster from OSM
    [Documentation]   Unregister K8s cluster from OSM.
    [Tags]   cleanup
    Delete K8s Cluster   ${K8SCLUSTER_NAME}

Delete NS Descriptor
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor 1
    [Documentation]   Delete first VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME1}

Delete VNF Descriptor 2
    [Documentation]   Delete second VNF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME2}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suite Cleanup: Deleting descriptors and NS instance
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD 1   ${VNFD_NAME1}
    Run Keyword If Any Tests Failed   Delete VNFD 2   ${VNFD_NAME2}
