*** Comments ***
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


*** Settings ***
Documentation   [K8s-11] Simple Helm K8s Scale.

Library   OperatingSystem
Library   String
Library   Collections
Library   Process
Library   SSHLibrary

Resource   ../lib/vnfd_lib.resource
Resource   ../lib/nsd_lib.resource
Resource   ../lib/ns_lib.resource
Resource   ../lib/ns_operation_lib.resource
Resource   ../lib/connectivity_lib.resource
Resource   ../lib/ssh_lib.resource
Resource   ../lib/k8scluster_lib.resource

Test Tags   k8s_11   cluster_k8s

Suite Teardown   Run Keyword And Ignore Error   Suite Cleanup


*** Variables ***
# K8s cluster name and version
${K8SCLUSTER_NAME}   k8s11
${K8SCLUSTER_VERSION}   v1

# NS and VNF descriptor package folder and ids
${VNFD_PKG}   openldap_scale_knf
${VNFD_NAME}   openldap_scale_knf
${NSD_PKG}   openldap_scale_ns
${NSD_NAME}   openldap_scale_ns

# NS instance name and configuration
${NS_NAME}   openldap-scale
${NS_CONFIG}   {vld: [ {name: mgmtnet, vim-network-name: %{VIM_MGMT_NET}} ] }

# Username and SSH private key for accessing OSM host
${USERNAME}   ubuntu
${PASSWORD}   ${EMPTY}
${PRIVATEKEY}   %{OSM_RSA_FILE}

${NS_ID}   ${EMPTY}
${PUBLICKEY}   %{HOME}/.ssh/id_rsa.pub
${VNF_MEMBER_INDEX}   openldap
${KDU_NAME}   native-kdu
${SCALING_GROUP}   scale-kdu


*** Test Cases ***
Create Simple K8s Scale VNF Descriptor
    [Documentation]   Upload VNF package for the testsuite.
    Create VNFD   '%{PACKAGES_FOLDER}/${VNFD_PKG}'

Create Simple K8s Scale NS Descriptor
    [Documentation]   Upload NS package for the testsuite.
    Create NSD   '%{PACKAGES_FOLDER}/${NSD_PKG}'

Add K8s Cluster To OSM
    [Documentation]   Register K8s cluster in OSM.
    Create K8s Cluster   %{K8S_CREDENTIALS}   ${K8SCLUSTER_VERSION}   %{VIM_TARGET}   %{VIM_MGMT_NET}   ${K8SCLUSTER_NAME}
    Check For K8s Cluster To Be Ready   ${K8SCLUSTER_NAME}

Create Network Service Instance
    [Documentation]   Instantiate NS for the testsuite.
    ${id}=   Create Network Service   ${NSD_NAME}   %{VIM_TARGET}   ${NS_NAME}   ${NS_CONFIG}   ${PUBLICKEY}
    Set Suite Variable   ${NS_ID}   ${id}

Get Vnf Id
    [Documentation]   Get VNF identifier from OSM.
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    @{vnfr_list}=   Get Ns Vnfr Ids   ${NS_ID}
    Log List   ${vnfr_list}
    Set Suite Variable   ${VNF_ID}   ${vnfr_list}[0]

Get Scale Count Before Scale Out
    [Documentation]   Get the scale count of the application of network service k8s instance
    Variable Should Exist   ${NS_ID}   msg=Network service instance is not available
    ${kdu_count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU_NAME}
    Log   ${kdu_count}
    Set Suite Variable   ${INITIAL_KDU_COUNT}   ${kdu_count}

Perform Manual KDU Scale Out
    [Documentation]   Scale out the application of network service k8s instance.
    ${ns_op_id_1}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_OUT
    Log   ${ns_op_id_1}

Check Scale Count After Scale Out
    [Documentation]   Check whether the scale count is more than one.

    Variable Should Exist   ${INITIAL_KDU_COUNT}   msg=Initial KDU count is not available
    ${kdu_count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU_NAME}
    Log   ${kdu_count}
    IF   ${kdu_count} != ${INITIAL_KDU_COUNT} + 2   Fail   msg=There is no new KDU in the model after Scale Out

Perform Manual KDU Scale In
    [Documentation]   Scale in the application of network service k8s instance.
    ${ns_op_id_2}=   Execute Manual VNF Scale   ${NS_NAME}   ${VNF_MEMBER_INDEX}   ${SCALING_GROUP}   SCALE_IN
    Log   ${ns_op_id_2}

Check Scale Count After Scale In
    [Documentation]   Check whether the scale count is one less.
    ${kdu_count}=   Get Vnf Kdu Replica Count   ${VNF_ID}   ${KDU_NAME}
    IF   ${kdu_count} != ${INITIAL_KDU_COUNT}   Fail   msg=There is the same number of KDU in the model after Scale In

Delete NS K8s Instance Test
    [Documentation]   Delete NS instance.
    [Tags]   cleanup
    Delete NS   ${NS_NAME}

Remove K8s Cluster from OSM
    [Documentation]   Unregister K8s cluster from OSM.
    [Tags]   cleanup
    Delete K8s Cluster   ${K8SCLUSTER_NAME}

Delete NS Descriptor Test
    [Documentation]   Delete NS package from OSM.
    [Tags]   cleanup
    Delete NSD   ${NSD_NAME}

Delete VNF Descriptor Test
    [Documentation]   Delete NF package from OSM.
    [Tags]   cleanup
    Delete VNFD   ${VNFD_NAME}


*** Keywords ***
Suite Cleanup
    [Documentation]   Test Suit Cleanup: Deleting Descriptor, instance and vim
    Run Keyword If Any Tests Failed   Delete NS   ${NS_NAME}
    Run Keyword If Any Tests Failed   Delete NSD   ${NSD_NAME}
    Run Keyword If Any Tests Failed   Delete VNFD   ${VNFD_NAME}
    Run Keyword If Any Tests Failed   Delete K8s Cluster   ${K8SCLUSTER_NAME}
