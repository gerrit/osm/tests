## Copyright 2020 ETSI OSM
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from shutil import copy
from subprocess import run
from osm_client import *

packages_folder = os.environ["PACKAGES_FOLDER"]
conformance_folder = "{}/{}".format(os.environ["ROBOT_DEVOPS_FOLDER"], "conformance-tests")
test_lists_folder = "{}/{}".format(conformance_folder, "test-lists")
sol005_folder = "{}/repo/SOL005".format(conformance_folder)
osm_hostname = os.environ["OSM_HOSTNAME"]


def get_package_path(package_name):
    return "{}/{}".format(packages_folder, package_name)


def get_suite_path(suite_name):
    return "{}/repo/SOL005/{}".format(conformance_folder, suite_name)


def run_test_suite(suite_dir, suite_name, arg_file):
    print("robot -d {}/repo/{}/reports --argumentfile {} .".format(conformance_folder, suite_name, arg_file))
    run(["robot --loglevel=TRACE -d {}/reports/{} --argumentfile {} .".format(conformance_folder, suite_name, arg_file)], cwd=suite_dir, shell=True)


def sub_env_vars(file_dir, file_name):
    run(["cat {} | envsubst | cat > {}".format(file_name, get_suite_path(file_name.split(".")[0]) + "/" + file_name)], cwd=file_dir, shell=True)


# RESOURCE CREATION HAPPENS BELOW

nsd_id, vnfd_id, ns_id = "", "", ""

if ':' in osm_hostname:
    port = osm_hostname.split(':')[-1]
else:
    port = "9999"
osm_hostname = osm_hostname.split(':')[0]

osm = OSM(osm_hostname, port)

os.environ["AUTH_TOKEN"] = osm.get_auth_token()

try:
    vnfd_id = osm.create_vnfd(get_package_path("hackfest_basic_vnf/hackfest_basic_vnfd.yaml"))
    nsd_id = osm.create_nsd(get_package_path("hackfest_basic_ns/hackfest_basic_nsd.yaml"))
    ns_id = osm.create_ns("Test_NS", nsd_id, "osm")
except ResourceException as e:
    print(e.message)

print("VNFD: {}\nNSD: {}\nNS: {}".format(vnfd_id, nsd_id, ns_id))

# Apply relevant env variables (required for test vars)
os.environ["NSD_INFO_ID"] = nsd_id
os.environ["VNFD_INFO_ID"] = vnfd_id
os.environ["NS_INFO_ID"] = ns_id
os.environ["NFVO_HOST"] = osm_hostname
os.environ["NFVO_PORT"] = port

# RESOURCE CREATION HAPPENS ABOVE


# Copy test selection files over to relevant directories
(_, _, filenames) = next(os.walk(test_lists_folder))
for f in filenames:
    if f.endswith(".txt"):
        # Apply ENV Variables
        sub_env_vars(test_lists_folder, f)


# Run the robot tests
(_, directories, _) = next(os.walk(sol005_folder))
for d in directories:
    run_test_suite("{}/{}".format(sol005_folder, d), d, d + ".txt")


# We then need to clear the created resources
try:
    osm.delete_ns(ns_id)
    osm.delete_nsd(nsd_id)
    osm.delete_vnfd(vnfd_id)
except ResourceException as e:
    print("Deletion failed: {}".format(e.message))

print("Cleared resources")
