## Copyright 2020 ETSI OSM
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License

-t GET Individual Network Service Descriptor Information with invalid resource identifier
-t Disable Individual Network Service Descriptor
-t Enable Individual Network Service Descriptor
-t Enable Individual Network Service Descriptor with conflict due to operational state ENABLED
# -t DELETE Individual Network Service Descriptor in operational state ENABLED
-t POST Individual Network Service Descriptor - Method not implemented
-t PUT Individual Network Service Descriptor - Method not implemented
-t Get single file NSD Content in Plain Format
-t Get NSD Content in Zip Format
-t Get single file NSD Content in Plain or Zip Format
-t Get multi file NSD Content in Plain or Zip Format
-t Get multi file NSD Content in Plain Format
-t Get NSD Content with invalid resource identifier
-t Get NSD Content with conflict due to onboarding state
-t GET NSD Content with Range Request and NFVO not supporting Range Requests
-t Upload NSD Content as Zip file in synchronous mode
-t Upload NSD Content as plain text file in synchronous mode
-t POST NSD Content - Method not implemented
-t PATCH NSD Content - Method not implemented
-t DELETE NSD Content - Method not implemented

--variable nsdInfoId:$NSD_INFO_ID
--variable nsdInfoIdPlain:$NSD_INFO_ID
--variable nsdInfoIdZip:$NSD_INFO_ID
--variable notOnboardedNsInfoId:$NSD_INFO_ID

--variable NFVO_HOST:$NFVO_HOST
--variable NFVO_PORT:$NFVO_PORT
--variable apiRoot:/osm
--variable AUTHORIZATION_TOKEN:Bearer $AUTH_TOKEN
