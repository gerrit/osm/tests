<!--
Copyright 2020 ETSI OSM

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions and
limitations under the License
-->

# Guide to SOL005 integration into OSM's existing CI/CD pipeline
## Add tests to test selection files
### Locate the correct test selection file

The test selection files are located in `/conformance-tests/test-lists/`. Each API has its own test selection file of the same name (i.e. NSDManagement-API.txt for NSDManagement-API).

### Test selection files
Each test selection file has the following format
```git l
-t [Test name]
--variable [varName:value]
```
For example
```
-t PATCH NSD Content - Method not implemented
-t DELETE NSD Content - Method not implemented
--variable nsdInfoId:$NSD_INFO_ID
--variable nsdInfoIdPlain:$NSD_INFO_ID
```
For values which need to be created dynamically (like resource IDs), use environment variables (see below).
## Add resource management code
### Resource creation
The test management code resides in `/conformance-tests/run_conformance_tests.py`.
Using the osm client, create the relevant resources and assign the required values to environment variables (using the `os.environ` dictionary).

### Env variable subbing
The code then replaces the environment variables referred to in the test selection files using the `envsubst` unix command).

### Resource clearing
Don't forget to clear all created resources at the end of the testing!

## Reports
The reports are located in `/conformance-tests/reports/{API_NAME}/*`.