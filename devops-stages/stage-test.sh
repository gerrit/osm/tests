#!/bin/sh
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

check_tabs () {
    folder="$1"
    tabs="y"
    ! grep -r "	" ${folder} && tabs=""
    [ -n "${tabs}" ] && \
    echo "There are tabs in ${folder}" && \
    echo "Please replace by spaces" && \
    exit 1
}

check_two_spaces () {
    folder="$1"
    two_spaces="y"
    ! grep -r "[^ ]  [^ ]" ${folder} && two_spaces=""
    [ -n "${two_spaces}" ] && \
    echo "Some files in ${folder} are using two spaces to separate params" && \
    echo "Use this command to change it:" && \
    echo "   sed -i 's/\([^ ]\)  \([^ ]\)/\\\1   \\\2/g' <FILENAME>" && \
    exit 1
}

check_four_spaces () {
    folder="$1"
    four_spaces="y"
    ! grep -r "[^# \.]    " ${folder} && four_spaces=""
    [ -n "${four_spaces}" ] && \
    echo "Some files in ${folder} are using four spaces or more to separate params" && \
    echo "You can try this command to change it:" && \
    echo "   sed -i 's/\([^# \.]\)    */\\\1   /g' <FILENAME>" && \
    exit 1
}

check_crlf_terminators () {
    folder="$1"
    crlf_terminators="y"
    ! (find ${folder} -not -type d -exec file "{}" ";" | grep CRLF) && crlf_terminators=""
    [ -n "${crlf_terminators}" ] && \
    echo "Some files in ${folder} have CRLF at the end of some lines" && \
    echo "Use this command to change it:" && \
    echo "   dos2unix <FILENAME>" && \
    exit 1
}

check_spaces_eol () {
    folder="$1"
    spaces_eol="y"
    ! grep -ri " $" ${folder} && spaces_eol=""
    [ -n "${spaces_eol}" ] && \
    echo "Some files in ${folder} have spaces at the end of some lines" && \
    echo "Use this command to change it:" && \
    echo "   sed -i 's/ *$//g' <FILENAME>" && \
    exit 1
}

echo "Checking syntax of Robot tests"

echo "Checking tabs in robot files. No tabs should be present"
check_tabs robot-systest/testsuite
check_tabs robot-systest/lib
echo "No tabs are present in robot files. Correct!"

echo "Checking param separation in robot files. Three spaces is the recommendation, instead of two"
check_two_spaces robot-systest/testsuite
check_two_spaces robot-systest/lib
echo "No presence of two spaces to separate params in robot files. Correct!"

echo "Checking param separation in robot files. Three spaces is the recommendation, instead of four or more"
check_four_spaces robot-systest/testsuite
check_four_spaces robot-systest/lib
echo "Only three spaces must be used between params in robot files. Correct!"

echo "Checking CRLF terminators in robot files. No CRLF should be found"
check_crlf_terminators robot-systest/testsuite
check_crlf_terminators robot-systest/lib
echo "No presence of CRLF terminators in robot files. Correct!"

echo "Checking spaces at the end of lines in robot files. No spaces at EOL should be found"
check_spaces_eol robot-systest/testsuite
check_spaces_eol robot-systest/lib
echo "No presence of spaces at EOL in robot files. Correct!"

# Other policies to be added here:
# - Blank lines used to separate main sections (Settings, Variables, Test Cases, Keywords)
# - Blank lines used to separate parts of eah section:
#   - Variables: max 1 blank line to separate each one
#   - Settings: 1 blank line to separate each kind of setting (all LIbrary together, all Resource together, etc.)
#   - Test cases: 2 blank lines between test cases, max 1 blank line inside Test case, no blank line after Test Case Keyword

echo "Launching tox"
TOX_PARALLEL_NO_SPINNER=1 tox --parallel=auto

